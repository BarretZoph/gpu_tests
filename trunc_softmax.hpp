
template<typename dType>
void trunc_softmax<dType>::init_loss_layer(int output_vocab_size,int LSTM_size,int minibatch_size,int shortlist_size,int sampled_size,int longest_sent) {

	this->output_vocab_size = output_vocab_size;
	this->LSTM_size = LSTM_size;
	this->minibatch_size = minibatch_size;
	this->shortlist_size = shortlist_size;
	this->sampled_size = sampled_size;

	dType *h_temp;
	full_matrix_setup(&h_temp,&d_D,LSTM_size,output_vocab_size);
	full_matrix_setup(&h_temp,&d_b_d,1,output_vocab_size);
	full_matrix_setup(&h_temp,&d_D_small,LSTM_size,shortlist_size + sampled_size);
	full_matrix_setup(&h_temp,&d_D_grad_small,LSTM_size,shortlist_size + sampled_size);
	full_matrix_setup(&h_temp,&d_b_d_small,1,shortlist_size + sampled_size);
	full_matrix_setup(&h_temp,&d_b_d_grad_small,1,shortlist_size + sampled_size);
	full_matrix_setup(&h_temp,&d_outputdist,output_vocab_size,minibatch_size);
	full_matrix_setup(&h_temp,&d_outputdist_small,shortlist_size+sampled_size,minibatch_size);

	h_sampled_indicies = (int *)malloc( sampled_size*sizeof(int));
	bitmap = (bool *)malloc( output_vocab_size*sizeof(bool));

	h_vocab_indicies = (int *)malloc( longest_sent*minibatch_size*sizeof(int));
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_vocab_indicies,longest_sent*minibatch_size*sizeof(int)),"GPU memory allocation failed\n");

	h_vocab_indicies_01 = (int *)malloc( longest_sent*minibatch_size*sizeof(int));
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_vocab_indicies_01,longest_sent*minibatch_size*sizeof(int)),"GPU memory allocation failed\n");

	h_vocab_indicies_01_float = (dType *)malloc( longest_sent*minibatch_size*sizeof(dType));
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_vocab_indicies_01_float,longest_sent*minibatch_size*sizeof(dType)),"GPU memory allocation failed\n");


	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_result, 1*sizeof(dType)),"GPU memory allocation failed\n");
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_temp_result, NORM_THREADS*sizeof(dType)),"GPU memory allocation failed\n");

	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_train_perplexity, 1*sizeof(double)),"GPU memory allocation failed\n");
	cudaMemset(d_train_perplexity,0,1*sizeof(double));


	for(int i=0; i<longest_sent; i++) {
		nodes.push_back( softmax_node<dType>(LSTM_size,minibatch_size,shortlist_size+sampled_size,i,dropout) );
	}

	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");

	clear_gradients();
}

template<typename dType>
void trunc_softmax<dType>::zero_bitmap() {
	memset(bitmap,0,output_vocab_size*sizeof(bool));
}

template<typename dType>
void trunc_softmax<dType>::clear_gradients() {
	cudaMemset(d_D_grad_small,0,output_vocab_size*LSTM_size*sizeof(dType));
	cudaMemset(d_b_d_grad_small,0,output_vocab_size*1*sizeof(dType));
}

template<typename dType>
void trunc_softmax<dType>::prep_GPU_vocab_indices(int *h_output_vocab_indicies_target,int current_target_length) {

	//get the unique samples
	zero_bitmap();
	resevoir_mapping.clear();

	int curr_index = 0;
	for(int i=0; i<minibatch_size*current_target_length; i++) {

		if(bitmap[h_output_vocab_indicies_target[i]]==false && h_output_vocab_indicies_target[i] >= shortlist_size) {
			bitmap[h_output_vocab_indicies_target[i]] = true;
			h_sampled_indicies[curr_index] = h_output_vocab_indicies_target[i];
			curr_index++;
			if(curr_index>=sampled_size) {
				std::cout << "ERROR: the sample size of the truncated softmax is too small\n";
				std::cout << "More unique words in the minibatch that there are sampled slots\n";
				exit (EXIT_FAILURE);
			}
		}
	}
	int len_unique_words_trunc_softmax = curr_index;

	if(curr_index > sampled_size) {
		std::cout << "ERROR: the sample size of the truncated softmax is too small\n";
		std::cout << "More unique words in the minibatch that there are sampled slots\n";
		exit (EXIT_FAILURE);
	}

	curr_index = 0;
	int num_to_sample = sampled_size - len_unique_words_trunc_softmax;
	boost::uniform_real<> distribution(0,1);
	for(int i=shortlist_size; i<output_vocab_size; i++) {
		if(bitmap[i]==false) {
			//fill the resevoir initially
			if(curr_index < num_to_sample) {
				h_sampled_indicies[len_unique_words_trunc_softmax+curr_index] = i;
				curr_index++;
			}
			else {
				int rand_num = (int)(curr_index*distribution(gen));
				
				if (rand_num <num_to_sample) {
					h_sampled_indicies[len_unique_words_trunc_softmax+rand_num] = i;
				}
				curr_index++;
			}
		}
	}

	//get the mappings
	for(int i=0; i<shortlist_size; i++) {
		resevoir_mapping[i] = i;
	}
	for(int i=0; i<sampled_size; i++) {
		resevoir_mapping[h_sampled_indicies[i]] = i+shortlist_size;
	}

	//now modify the mappings
	for(int i=0; i<current_target_length*minibatch_size; i++) {
		if(h_output_vocab_indicies_target[i]!=-1) {
			h_vocab_indicies[i] = resevoir_mapping[h_output_vocab_indicies_target[i]];
			h_vocab_indicies_01[i] = 1;
		}
		else {
			h_vocab_indicies[i] = 0;
			h_vocab_indicies_01[i] = 0;
		}
	}

	//get the sample correction
	sample_correction = ((dType)(output_vocab_size-shortlist_size-len_unique_words_trunc_softmax))/(sampled_size-len_unique_words_trunc_softmax);

	//get how many words are in the shortlist
	shortlist_size_plus = shortlist_size + len_unique_words_trunc_softmax;


	//copy over the samples
	cudaMemcpy(d_sampled_indicies, h_sampled_indicies, sampled_size*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_vocab_indicies, h_vocab_indicies, current_target_length*minibatch_size*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_vocab_indicies_01, h_vocab_indicies_01, current_target_length*minibatch_size*sizeof(int), cudaMemcpyHostToDevice);


	//load in the correct embeddings
	load_in_embeddings_trunc<<<256,256>>>(d_D_small,d_D,d_sampled_indicies,shortlist_size,sampled_size,LSTM_size);
	

	//load in the correct bias
	load_in_embeddings_trunc<<<256,256>>>(d_b_d_small,d_b_d,d_sampled_indicies,shortlist_size,sampled_size,1);

}

template<typename dType>
void trunc_softmax<dType>::backprop_prep_GPU(dType *d_h_t,int step) 
{
	this->d_h_t = d_h_t;
	this->d_vocab_indicies_single = d_vocab_indicies + step;
	this->d_vocab_indicies_01_single = d_vocab_indicies_01 + step;
	this->d_vocab_indicies_01_float_single = d_vocab_indicies_01_float + step;
}

template<typename dType>
void trunc_softmax<dType>::backprop_prep_GPU_mgpu(int step) 
{
	this->d_vocab_indicies_single = d_vocab_indicies + step;
	this->d_vocab_indicies_01_single = d_vocab_indicies_01 + step;
	this->d_vocab_indicies_01_float_single = d_vocab_indicies_01_float + step;
}



template<typename dType>
void trunc_softmax<dType>::forward_prop(int index) {
	cudaDeviceSynchronize();
	dType alpha = 1;
	dType beta = 0;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle, CUBLAS_OP_T, CUBLAS_OP_N,
	 shortlist_size+sampled_size, minibatch_size, LSTM_size, &alpha, d_D_small, LSTM_size,
	  nodes[index].d_h_t, LSTM_size, &beta, nodes[index].d_outputdist, shortlist_size+sampled_size),"get_distribution cuBLAS call failed\n");


	//add the bias vector to the matrix
	int threads_per_block = 128;
	int num_block = (shortlist_size+sampled_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(minibatch_size,num_block,1);
	matrix_bias_kernel<<< kernel_dim,threads_per_block>>>(shortlist_size+sampled_size,nodes[index].d_outputdist,d_b_d_small,nodes[index].d_outputdist);
	CUDA_GET_LAST_ERROR();

	outputdist_truncated_kernel<<<minibatch_size,SOFTMAX_THREADS>>>(nodes[index].d_outputdist, nodes[index].d_outputdist,shortlist_size+sampled_size,
		sample_correction,shortlist_size_plus);

	train_perplexity_kernel<<<1,1>>>(d_vocab_indicies_single,d_vocab_indicies_01_single,nodes[index].d_outputdist,
		d_train_perplexity,minibatch_size,shortlist_size_plus); 
	cudaDeviceSynchronize();
}

template<typename dType>
void trunc_softmax<dType>::back_prop1(int index) {

	cudaDeviceSynchronize();
	dType alpha = -1;
	dType beta = 0;
	//multiply outputdist by D
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,LSTM_size,minibatch_size,output_vocab_size,
		&alpha,d_D_small,LSTM_size,nodes[index].d_outputdist,shortlist_size+sampled_size,&beta,nodes[index].d_d_ERRt_ht,LSTM_size),"cuBLAS h_t gradient failed");

	//add in the D rows
	int threads_per_block = 128;
	int num_block = (output_vocab_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(minibatch_size,num_block,1);
	matrix_column_to_matrix_column_kernel<<<kernel_dim,threads_per_block>>>(nodes[index].d_d_ERRt_ht,d_D_small,d_vocab_indicies_single,LSTM_size);
	CUDA_GET_LAST_ERROR();

	//zero out columns
	int num_block_2 = (LSTM_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim_2(minibatch_size,num_block_2,1);
	zero_columns_kernel_128<<<kernel_dim_2,threads_per_block>>>(LSTM_size,nodes[index].d_d_ERRt_ht,d_vocab_indicies_01_single,nodes[index].d_d_ERRt_ht);	

	cudaDeviceSynchronize();
}

template<typename dType>
void trunc_softmax<dType>::back_prop2(int index) {

	cudaDeviceSynchronize();

	/////////////////////// for D grad ///////////////////////////////
	//zero out h_t
	int threads_per_block = 128;
	int num_block = (LSTM_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(minibatch_size,num_block,1);
	zero_columns_kernel_128<<<kernel_dim,threads_per_block>>>(LSTM_size,nodes[index].d_h_t,d_vocab_indicies_01_single,nodes[index].d_h_t);
	CUDA_GET_LAST_ERROR();

	//multiply output dist and h_t
	dType alpha = -1;
	dType beta = 1;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,LSTM_size,shortlist_size+sampled_size,minibatch_size,&alpha,nodes[index].d_h_t,LSTM_size,
		nodes[index].d_outputdist,shortlist_size+sampled_size,&beta,d_D_grad_small,LSTM_size),"computing softmax D gradient failed in cuBLAS\n");

	//add columns of h_t to D_grad
	matrix_column_to_matrix_column_kernel<<< kernel_dim,threads_per_block>>>(d_D_grad_small,nodes[index].d_h_t,d_vocab_indicies_single,LSTM_size);
	CUDA_GET_LAST_ERROR();

	////////////////////// for b_d grad ////////////////////////////
	//multiply
	alpha = -1;
	beta = 1;
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,shortlist_size+sampled_size,minibatch_size,&alpha,nodes[index].d_outputdist,shortlist_size+sampled_size,
		d_vocab_indicies_01_float_single,1,&beta,d_b_d_grad_small,1),"cuBLAS compute b_d_gradient failed");

	//add ones
	threads_per_block = 128;
	num_block = (minibatch_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim_2(1,num_block,1);
	add_ones_b_d_grad<<< kernel_dim_2,threads_per_block>>>(d_b_d_grad_small,d_vocab_indicies_01_single,d_vocab_indicies_single,minibatch_size);

	cudaDeviceSynchronize();
}




template<typename dType>
void trunc_softmax<dType>::update_learning_rate(dType learning_rate) {
	this->learning_rate = learning_rate;
}




