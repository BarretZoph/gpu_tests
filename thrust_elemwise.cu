#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <time.h>

struct ElementWiseProductBasic : public thrust::binary_function<float,float,float>
{
    __host__ __device__
    float operator()(const float& v1, const float& v2) const
    {
        float res;
        res = v1 * v2;
        return res;
    }
};

int get_random_int(int min, int max)
{
    return min + (rand() % (int)(max - min + 1));
}

thrust::host_vector<float> init_vector(const size_t N)
{
    thrust::host_vector<float> temp(N);
    for(size_t i = 0; i < N; i++)
    {
        temp[i] = get_random_int(0, 10);
    }
    return temp;
}

int main(void)
{
    const size_t N = 4;
    const bool compute_basic_product  = true;

    srand(time(NULL));

    thrust::host_vector<float>   h_A = init_vector(N);
    thrust::host_vector<float>   h_B = init_vector(N);
    thrust::device_vector<float> d_A = h_A;
    thrust::device_vector<float> d_B = h_B;

    thrust::host_vector<float> h_result(N);
    thrust::host_vector<float> h_result_modified(N);

    thrust::device_vector<float> d_result(N);

    for(int i = 0; i < num_trials; i++)
    {

        thrust::transform(d_A.begin(), d_A.end(),
                          d_B.begin(), d_result.begin(),
                          ElementWiseProductBasic());
        h_result = d_result;
    }

    // std::cout << std::fixed;
    // for (size_t i = 0; i < 4; i++)
    // {
    //     float a = h_A[i];
    //     float b = h_B[i];

    //     std::cout << "(" << a << ")";
    //     std::cout << " * ";
    //     std::cout << "(" << b << ")";

    //     if (compute_basic_product)
    //     {
    //         float prod = h_result[i];
    //         std::cout << " = ";
    //         std::cout << "(" << prod << ")";
    //     }

    //     // if (compute_modified_product)
    //     // {
    //     //     float2 prod_modified = h_result_modified[i];
    //     //     std::cout << " = ";
    //     //     std::cout << "(" << prod_modified.x << "," << prod_modified.y << ")";
    //     // }
    //     std::cout << std::endl;
    // }   

    return 0;
}