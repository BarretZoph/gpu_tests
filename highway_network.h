#ifndef HIGHWAY_NETWORK_H
#define HIGHWAY_NETWORK_H

#include "highway_node.h"

//highway network layer
template<typename dType>
class highway_network_layer {
public:

	cublasHandle_t handle;
	cudaStream_t s0;
	int device_number;
	int state_size;
	int minibatch_size;
	int longest_sent;

	//params
	dType *d_W_h; //for ReLU gate
	dType *d_W_t; //for sigmoid gate
	dType *d_b_h;
	dType *d_b_t; //initialize to -2

	//gradients
	dType *d_W_h_grad; //for ReLU gate
	dType *d_W_t_grad; //for sigmoid gate
	dType *d_b_h_grad;
	dType *d_b_t_grad;

	//forward prop values
	//these are stored in nodes

	dType *d_temp;
	dType *d_ones_minibatch;

	//back prop values
	dType *d_Err_t; //gate value
	dType *d_Err_y; //input
	dType *d_Err_g; //
	dType *d_Err_z;//output error being passed in


	dType *h_gold_vals;

	std::vector<highway_node<dType>*> nodes;

	void init(int state_size,int minibatch_size,int longest_sent);
	dType forward(int index,dType *d_y_temp); 
	void backward(int index,dType *d_Err_z_temp);
	void clear_gradients();
	void scale_gradients();
	void update_params();

};


//cuda kernels
template<typename dType>
__global__
void sigmoid_bias_kernel(dType *d_final,dType *d_bias,int state_size,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_final[i] = 1.0/(1.0 + cuda_exp_wrapper(-1*(d_final[i] + d_bias[i%state_size])));
	}
}


//cuda kernels
template<typename dType>
__global__
void ReLU_bias_kernel(dType *d_final,dType *d_bias,int state_size,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		dType temp = d_final[i] + d_bias[i%state_size];
		d_final[i] = temp * (temp > 0);
	}
}

//cuda kernels
template<typename dType>
__global__
void highway_compute_z_kernel(dType *d_z,dType *d_g,dType *d_t,dType *d_y,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_z[i] = d_t[i]*d_g[i] + (1-d_t[i])*d_y[i];
	}
}

template<typename dType>
__global__
void error_g_kernel(dType *d_Err_g,dType *d_Err_z,dType *d_t,dType *d_g,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_Err_g[i] = d_Err_z[i] * d_t[i] * (d_g[i] > 0);
	}
}

template<typename dType>
__global__
void error_t_kernel(dType *d_Err_t,dType *d_Err_z,dType *d_t,dType *d_g,dType *d_y,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_Err_t[i] = d_Err_z[i] * (d_g[i] - d_y[i]) * d_t[i] * (1 - d_t[i]);
	}
}

template<typename dType>
__global__
void error_y_final(dType *d_Err_z,dType *d_t,dType *d_Err_y,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_Err_y[i] += d_Err_z[i] * (1 - d_t[i]);
	}
}


template<typename dType>
void highway_network_layer<dType>::init(int state_size,int minibatch_size,int longest_sent,int device_number) {
	
	this->state_size = state_size;
	this->minibatch_size = minibatch_size;
	this->device_number = device_number;

	cudaSetDevice(device_number);

	dType *h_temp;
	full_matrix_setup(&h_temp,&d_W_h,state_size,state_size);
	full_matrix_setup(&h_temp,&d_W_t,state_size,state_size);
	full_matrix_setup(&h_temp,&d_b_h,state_size,1);
	full_matrix_setup(&h_temp,&d_b_t,state_size,1);

	thrust::device_ptr<dType> bias_ptr = thrust::device_pointer_cast(d_b_t);
	for(int i=0; i<state_size; i++) {
		bias_ptr[i] = -2;
	}

	full_matrix_setup(&h_temp,&d_W_h_grad,state_size,state_size);
	full_matrix_setup(&h_temp,&d_W_t_grad,state_size,state_size);
	full_matrix_setup(&h_temp,&d_b_h_grad,state_size,1);
	full_matrix_setup(&h_temp,&d_b_t_grad,state_size,1);

	full_matrix_setup(&h_temp,&d_temp,state_size,minibatch_size);
	full_vector_setup_ones(&h_temp,&d_ones_minibatch,minibatch_size);

	full_matrix_setup(&h_temp,&d_Err_t,state_size,minibatch_size);
	full_matrix_setup(&h_temp,&d_Err_y,state_size,minibatch_size);
	full_matrix_setup(&h_temp,&d_Err_g,state_size,minibatch_size);
	full_matrix_setup(&h_temp,&d_Err_z,state_size,minibatch_size);


	h_gold_vals = (dType *)malloc(state_size*minibatch_size*sizeof(dType));
	for(int i=0; i<state_size*minibatch_size;i++) {
		h_gold_vals[i] = 0.01*i;
	}

	for(int i=0; i<longest_sent; i++) {
		nodes.push_back( new highway_node<dType>(state_size,minibatch_size,i) );
	}

	clear_gradients();
}

template<typename dType>
dType highway_network_layer<dType>::forward(int index,dType *d_y_temp) {

	cudaSetDevice(device_number);

	dType *d_t = nodes[index]->d_t; //gate value
	dType *d_y = nodes[index]->d_y; //input
	dType *d_g = nodes[index]->d_g; //new value from ReLU
	dType *d_z = nodes[index]->d_z;//output

	cudaMemcpy(d_y, d_y_temp, state_size*minibatch_size*sizeof(dType), cudaMemcpyDefault);

	//calculate t
	dType alpha = 1;
	dType beta = 0;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,state_size,minibatch_size,state_size,&alpha,d_W_t,state_size,
		d_y,state_size,&beta,d_t,state_size),"Forward prop o_t temp1 failed\n");

	sigmoid_bias_kernel<<<256,256>>>(d_t,d_b_t,state_size,state_size*minibatch_size);


	//calculate g
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,state_size,minibatch_size,state_size,&alpha,d_W_h,state_size,
		d_y,state_size,&beta,d_g,state_size),"Forward prop o_t temp1 failed\n");

	ReLU_bias_kernel<<<256,256>>>(d_g,d_b_h,state_size,state_size*minibatch_size);

	//calculate z
	highway_compute_z_kernel<<<256,256>>>(d_z,d_g,d_t,d_y,state_size*minibatch_size);

	cudaDeviceSynchronize();

	thrust::device_ptr<dType> d_thrust_mat = thrust::device_pointer_cast(d_z);
	dType loss = 0;
	for(int i=0; i<state_size*minibatch_size; i++) {
		loss+= (d_thrust_mat[i] - h_gold_vals[i])*(d_thrust_mat[i] - h_gold_vals[i]);
	}
	return loss;
}



template<typename dType>
void highway_network_layer<dType>::backward(int index,dType *d_Err_z_temp) {


	cudaSetDevice(device_number);

	dType *d_t = nodes[index]->d_t; //gate value
	dType *d_y = nodes[index]->d_y; //input
	dType *d_g = nodes[index]->d_g; //new value from ReLU
	dType *d_z = nodes[index]->d_z;//output	

	if(d_Err_z_temp==NULL) {
		thrust::device_ptr<dType> d_thrust_mat = thrust::device_pointer_cast(d_Err_z);
		thrust::device_ptr<dType> d_thrust_mat_2 = thrust::device_pointer_cast(d_z);
		for(int i=0; i<state_size*minibatch_size; i++) {
			d_thrust_mat[i] = 2*(d_thrust_mat_2[i] - h_gold_vals[i]);
		}
	}
	else {
		cudaMemcpy(d_Err_z, d_Err_z_temp, state_size*minibatch_size*sizeof(dType), cudaMemcpyDefault);
	}

	//compute error with respect to g
	error_g_kernel<<<256,256>>>(d_Err_g,d_Err_z,d_t,d_g,state_size*minibatch_size);

	//compute error for W_h
	dType alpha = 1;
	dType beta = 1;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,state_size,state_size,minibatch_size,&alpha,
		d_Err_g,state_size,d_y,state_size,&beta,d_W_h_grad,state_size),"HIGHWAY backprop W_h failed\n");

	//error for b_h
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,state_size,minibatch_size,&alpha,d_Err_g,state_size,
		d_ones_minibatch,1,&beta,d_b_h_grad,1),"HIGHWAY backprop b_h failed\n");

	
	alpha = 1;
	beta = 0;
	//for partial y
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,state_size,minibatch_size,state_size,
		&alpha,d_W_h,state_size,d_Err_g,state_size,&beta,d_Err_y,state_size),"HIGHWAY BACKPROP y 1\n");

	
	//compute error with respect to t
	error_t_kernel<<<256,256>>>(d_Err_t,d_Err_z,d_t,d_g,d_y,state_size*minibatch_size);

	
	//compute error for W_h
	alpha = 1;
	beta = 1;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,state_size,state_size,minibatch_size,&alpha,
		d_Err_t,state_size,d_y,state_size,&beta,d_W_t_grad,state_size),"HIGHWAY backprop W_h failed\n");


	//error for b_h
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,state_size,minibatch_size,&alpha,d_Err_t,state_size,
		d_ones_minibatch,1,&beta,d_b_t_grad,1),"HIGHWAY backprop b_h failed\n");

	//for partial y
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,state_size,minibatch_size,state_size,
		&alpha,d_W_t,state_size,d_Err_t,state_size,&beta,d_Err_y,state_size),"HIGHWAY BACKPROP y 1\n");

	error_y_final<<<256,256>>>(d_Err_z,d_t,d_Err_y,state_size*minibatch_size);
}



template<typename dType>
void highway_network_layer<dType>::clear_gradients() {

	cudaMemset(d_W_h_grad,0,state_size*state_size*sizeof(dType));
	cudaMemset(d_W_t_grad,0,state_size*state_size*sizeof(dType));
	cudaMemset(d_b_h_grad,0,state_size*1*sizeof(dType));
	cudaMemset(d_b_t_grad,0,state_size*1*sizeof(dType));
}

template<typename dType>
void highway_network_layer<dType>::scale_gradients() {

}







#endif
