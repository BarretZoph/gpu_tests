#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"
#include <curand.h>

//thrust streams
//#include <thrust/execution_policy.h>
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/extrema.h>

int main() {

	float *d_mask;
	int LSTM_size = 1000;
	int minibatch_size = 128;
	int num_trials = 100;
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_mask, LSTM_size*minibatch_size*sizeof(float)),"GPU memory allocation failed zeros\n");

	curandGenerator_t gen;
	curandCreateGenerator(&gen,CURAND_RNG_PSEUDO_DEFAULT);

	curandSetPseudoRandomGeneratorSeed(gen,1234);

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	std::chrono::duration<double> elapsed_seconds;
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		curandGenerateUniform(gen,d_mask,LSTM_size*minibatch_size);
		cudaDeviceSynchronize();
	}
	thrust::device_ptr<float> debug_ptr = thrust::device_pointer_cast(d_mask);
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;

    std::cout << "\n\n\n";
    std::cout << "Average time: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	for(int i=0; i<15; i++) {
		std::cout << debug_ptr[i] << "\n";
	}
}