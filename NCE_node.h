
template<typename dType>
struct NCE_node {

	//each node stores the unnormalized probabilities, plus the h_t
	dType *d_h_t;
	dType *d_p_true;
	int index;

	NCE_node(int LSTM_size,int minibatch_size,int output_vocab_size,int index,bool dropout) {
		CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_outputdist, output_vocab_size*minibatch_size*sizeof(dType)),"GPU memory allocation failed\n");
		CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_h_t, LSTM_size*minibatch_size*sizeof(dType)),"GPU memory allocation failed\n");
		CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_d_ERRt_ht, LSTM_size*minibatch_size*sizeof(dType)),"GPU memory allocation failed\n");
		this->index = index;
		if(dropout) {
			CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_dropout_mask, LSTM_size*minibatch_size*sizeof(dType)),"GPU memory allocation failed\n");
		}
	}
};