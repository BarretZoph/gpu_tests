#include <stdio.h>
#include <assert.h>

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", 
            cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}

void profileCopies(float        *h_a, 
                   float        *h_b, 
                   float        *d, 
                   unsigned int  n,
                   char         *desc)
{

  printf("\n%s transfers\n", desc);

  unsigned int bytes = n * sizeof(float);

  // events for timing
  cudaEvent_t startEvent, stopEvent; 

  checkCuda( cudaEventCreate(&startEvent) );
  checkCuda( cudaEventCreate(&stopEvent) );

  checkCuda( cudaEventRecord(startEvent, 0) );
  checkCuda( cudaMemcpy(d, h_a, bytes, cudaMemcpyHostToDevice) );
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );

  float time;
  checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
  printf("  Host to Device bandwidth (GB/s): %f\n", bytes * 1e-6 / time);

  checkCuda( cudaEventRecord(startEvent, 0) );
  checkCuda( cudaMemcpy(h_b, d, bytes, cudaMemcpyDeviceToHost) );
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );

  checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
  printf("  Device to Host bandwidth (GB/s): %f\n", bytes * 1e-6 / time);

  for (int i = 0; i < n; ++i) {
    if (h_a[i] != h_b[i]) {
      printf("*** %s transfers failed ***", desc);
      break;
    }
  }

  // clean up events
  checkCuda( cudaEventDestroy(startEvent) );
  checkCuda( cudaEventDestroy(stopEvent) );
}

void profileCopies_dma(float        *h_a, 
                    float *h_b,
                   float        *d_a, 
                   float        *d_b, 
                   unsigned int  n,
                   char         *desc)
{
  printf("\n%s transfers\n", desc);

  unsigned int bytes = n * sizeof(float);

  cudaSetDevice(0);

  // events for timing
  cudaEvent_t startEvent, stopEvent; 

  checkCuda( cudaMemcpy(d_a, h_a, bytes, cudaMemcpyHostToDevice) );

  checkCuda( cudaEventCreate(&startEvent) );
  checkCuda( cudaEventCreate(&stopEvent) );

  checkCuda( cudaEventRecord(startEvent, 0) );
  checkCuda( cudaMemcpy(d_b, d_a, bytes, cudaMemcpyDefault) );
  checkCuda( cudaEventRecord(stopEvent, 0) );
  cudaSetDevice(1);
  checkCuda( cudaEventSynchronize(stopEvent) );

  float time;
  checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
  printf("  Device to Device bandwidth (GB/s): %f\n", bytes * 1e-6 / time);
  checkCuda( cudaMemcpy(h_b, d_b, bytes, cudaMemcpyDeviceToHost) );

  // checkCuda( cudaEventRecord(startEvent, 0) );
  // checkCuda( cudaMemcpy(h_b, d, bytes, cudaMemcpyDeviceToHost) );
  // checkCuda( cudaEventRecord(stopEvent, 0) );
  // checkCuda( cudaEventSynchronize(stopEvent) );

  // checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
  // printf("  Device to Host bandwidth (GB/s): %f\n", bytes * 1e-6 / time);

  for (int i = 0; i < n; ++i) {
    if (h_a[i] != h_b[i]) {
      printf("*** %s transfers failed ***", desc);
      break;
    }
  }

  // clean up events
  checkCuda( cudaEventDestroy(startEvent) );
  checkCuda( cudaEventDestroy(stopEvent) );
}


int main()
{
  unsigned int nElements = 4*1024*1024;
  const unsigned int bytes = nElements * sizeof(float);

  // host arrays
  float *h_aPageable, *h_bPageable;   
  float *h_aPinned, *h_bPinned;

  // device array
  float *d_a;
  float *d_b;

  // allocate and initialize
  h_aPageable = (float*)malloc(bytes);                    // host pageable
  h_bPageable = (float*)malloc(bytes);                    // host pageable
  checkCuda( cudaMallocHost((void**)&h_aPinned, bytes) ); // host pinned
  checkCuda( cudaMallocHost((void**)&h_bPinned, bytes) ); // host pinned
  checkCuda( cudaMalloc((void**)&d_a, bytes) );           // device a


  cudaSetDevice(0);
  checkCuda( cudaMalloc((void**)&d_a, bytes) );           // device b
  cudaSetDevice(1);
  checkCuda( cudaMalloc((void**)&d_b, bytes) );           // device b
  cudaSetDevice(0);

  cudaError_t status =  cudaDeviceEnablePeerAccess( 1, 0 );
  if (status == cudaSuccess)
      printf("cudaSuccess: 1 to 0");
  else if(status == cudaErrorInvalidDevice)
    printf("cudaErrorInvalidDevice: 1 to 0");
  else if(status == cudaErrorInvalidDevice)
    printf("cudaErrorPeerAccessAlreadyEnabled: 1 to 0");
  else if(status == cudaErrorInvalidDevice)
    printf("cudaErrorInvalidValue: 1 to 0");

    status =  cudaDeviceEnablePeerAccess( 0, 1 );
  if (status == cudaSuccess)
      printf("cudaSuccess: 0 to 1");
  else if(status == cudaErrorInvalidDevice)
    printf("cudaErrorInvalidDevice: 0 to 1");
  else if(status == cudaErrorInvalidDevice)
    printf("cudaErrorPeerAccessAlreadyEnabled: 0 to 1");
  else if(status == cudaErrorInvalidDevice)
    printf("cudaErrorInvalidValue: 0 to 1");


  for (int i = 0; i < nElements; ++i) h_aPageable[i] = i;      
  memcpy(h_aPinned, h_aPageable, bytes);
  memset(h_bPageable, 0, bytes);
  memset(h_bPinned, 0, bytes);

  // output device info and transfer size
  cudaDeviceProp prop;
  checkCuda( cudaGetDeviceProperties(&prop, 0) );

  printf("\nDevice: %s\n", prop.name);
  printf("Transfer size (MB): %d\n", bytes / (1024 * 1024));

  // perform copies and report bandwidth
  profileCopies(h_aPageable, h_bPageable, d_a, nElements, "Pageable");
  profileCopies(h_aPinned, h_bPinned, d_a, nElements, "Pinned");
  profileCopies_dma(h_aPinned, h_bPinned, d_a, d_b, nElements, "DMA");

  printf("\n");



  // cleanup
  cudaFree(d_a);
  cudaFreeHost(h_aPinned);
  cudaFreeHost(h_bPinned);
  free(h_aPageable);
  free(h_bPageable);

  return 0;
}