#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

//thrust streams
//#include <thrust/execution_policy.h>
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/extrema.h>


#define SOFTMAX_THREADS 1024


//This is called on the un-normalized distribution
//Note this is only called for float to deal with overflow issues with floats
/*
	-Each thread in a block gets a location in the buffer. Initially the max element is stored in this location
	-For buffer one extra slot is allocated to store the true max of the buffer
	-Each block does one outputdist column, so for a minibatch of 128, simply call this with dim = 20000 and blocks = 128
	-column major storage is necessary for this!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	-adapted from torch
	-This does summing and exping all in one go, so no thrust or column of 1's needed
	-This needs to be adapted to do tree based reductions and not have thread zero naively do everything
	- 
*/
__global__
void outputdist_overflow_prevention_kernel(float *output, float *input, int dim) {
	__shared__ float buffer[SOFTMAX_THREADS+1]; //shared memory for the block, this must be the number of threads per block in size
	int k = blockIdx.x; //get the block index
	float *input_k = input + k*dim; //all threads in block start from same index
	float *output_k = output + k*dim; //again all threads in block start from same index

	int i_start = threadIdx.x; //start at the thread index
	int i_end = dim; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step

	//get the max element for each thread's assigned locations and put them in the buffer
	//dim elements are covered in this reduction
	buffer[threadIdx.x] = -FLT_MAX;
	for(int i=i_start; i<i_end; i+=i_step) {
		float z = input_k[i];
		if(buffer[threadIdx.x] < z) {
			buffer[threadIdx.x] = z;
		}
	}

	 __syncthreads();

	 // reduce
	 //first thread goes through and finds the max element in the buffer
	 //after this stage the max element for dim items is found
	if (threadIdx.x == 0) {
		float max_k = -FLT_MAX;
		for (int i=0; i<blockDim.x; i++) {
	  		if(max_k < buffer[i])
	   			max_k = buffer[i];
		}
		buffer[SOFTMAX_THREADS] = max_k;
	}

	__syncthreads();

	// sum
	//Now go through all the dim elements and subtract the max from the element, keep a running sum for the normalization constant
	float max_k = buffer[SOFTMAX_THREADS];
	buffer[threadIdx.x] = 0;
	for (int i=i_start; i<i_end; i+=i_step) {
		float z = __expf(input_k[i]-max_k); //subtract the max from the input, then exp it for the softmax
		buffer[threadIdx.x] += z; //keep a running sum of these values for the normalization constant
		output_k[i] = z; //set the output as this value, then get ready to divide by the sum again
	}

 	__syncthreads();

 	// reduce
 	//Now sum all the elements in the buffer, for the normalization constant
	if (threadIdx.x == 0) {
		float sum_k = 0;
		for (int i=0; i<blockDim.x; i++) {
	  		sum_k += buffer[i];
	  	}
		buffer[SOFTMAX_THREADS] = sum_k;
	}

  	__syncthreads();

  	// normalize the softmax
	float sum_k = buffer[SOFTMAX_THREADS];
	for (int i=i_start; i<i_end; i+=i_step) {
		output_k[i] = output_k[i] / sum_k;
	}
}


//for optimizing warps
//volatile must be used as register optimization will lead to wrong answers
__device__ 
void warpReduceSum(volatile float* sdata, int tid) {
	sdata[tid] += sdata[tid + 32];
	sdata[tid] += sdata[tid + 16];
	sdata[tid] += sdata[tid + 8];
	sdata[tid] += sdata[tid + 4];
	sdata[tid] += sdata[tid + 2];
	sdata[tid] += sdata[tid + 1];
}

__device__ 
void warpReduceMax(volatile float* sdata, int tid) {
	sdata[tid] = (sdata[tid] > sdata[32 + tid]) ? sdata[tid] : sdata[32 + tid];
	sdata[tid] = (sdata[tid] > sdata[16 + tid]) ? sdata[tid] : sdata[16 + tid];
	sdata[tid] = (sdata[tid] > sdata[8 + tid]) ? sdata[tid] : sdata[8 + tid];
	sdata[tid] = (sdata[tid] > sdata[4 + tid]) ? sdata[tid] : sdata[4 + tid];
	sdata[tid] = (sdata[tid] > sdata[2 + tid]) ? sdata[tid] : sdata[2 + tid];
	sdata[tid] = (sdata[tid] > sdata[1 + tid]) ? sdata[tid] : sdata[1 + tid];
}



//This is version 2
/*
	-Adds tree based reduction within blocks, so thread zero doesnt sum on its own
	-has reduction #3 for sequential addressing
*/
__global__
void outputdist_overflow_prevention_kernel_v2(float *output, float *input, int dim) {
	__shared__ float buffer[SOFTMAX_THREADS]; //shared memory for the block, this must be the number of threads per block in size
	int k = blockIdx.x; //get the block index
	float *input_k = input + k*dim; //all threads in block start from same index
	float *output_k = output + k*dim; //again all threads in block start from same index

	int i_start = threadIdx.x; //start at the thread index
	int i_end = dim; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step
	const int tid = threadIdx.x;

	//get the max element for each thread's assigned locations and put them in the buffer
	//dim elements are covered in this reduction
	buffer[threadIdx.x] = -FLT_MAX;
	for(int i=i_start; i<i_end; i+=i_step) {
		float z = input_k[i];
		if(buffer[threadIdx.x] < z) {
			buffer[threadIdx.x] = z;
		}
	}

	 __syncthreads();

	 // reduce
	 //first thread goes through and finds the max element in the buffer
	 //after this stage the max element for dim items is found
	for(int stride=SOFTMAX_THREADS/2; stride>0; stride>>=1) {
		if(tid < stride) {
			buffer[tid] = (buffer[tid] > buffer[stride + tid]) ? buffer[tid] : buffer[stride + tid];
		}
		__syncthreads();
	}

	__syncthreads();

	// sum
	//Now go through all the dim elements and subtract the max from the element, keep a running sum for the normalization constant
	float max_k = buffer[0];
	buffer[threadIdx.x] = 0;
	for (int i=i_start; i<i_end; i+=i_step) {
		float z = __expf(input_k[i]-max_k); //subtract the max from the input, then exp it for the softmax
		buffer[threadIdx.x] += z; //keep a running sum of these values for the normalization constant
		output_k[i] = z; //set the output as this value, then get ready to divide by the sum again
	}

 	__syncthreads();

 	// reduce
 	//Now sum all the elements in the buffer, for the normalization constant
 	for(int stride=SOFTMAX_THREADS/2; stride>0; stride>>=1) {
		if(tid < stride) {
			buffer[tid] += buffer[stride + tid];
		}
		__syncthreads();
	}

  	__syncthreads();

  	// normalize the softmax
	float sum_k = buffer[0];
	for (int i=i_start; i<i_end; i+=i_step) {
		output_k[i] = output_k[i] / sum_k;
	}
}


//This is version 3
/*
	-Does reduce on last warp
	-
*/
__global__
void outputdist_overflow_prevention_kernel_v3(float *output, float *input, int dim) {
	__shared__ float buffer[SOFTMAX_THREADS]; //shared memory for the block, this must be the number of threads per block in size
	int k = blockIdx.x; //get the block index
	float *input_k = input + k*dim; //all threads in block start from same index
	float *output_k = output + k*dim; //again all threads in block start from same index

	int i_start = threadIdx.x; //start at the thread index
	int i_end = dim; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step
	const int tid = threadIdx.x;

	//get the max element for each thread's assigned locations and put them in the buffer
	//dim elements are covered in this reduction
	buffer[threadIdx.x] = -FLT_MAX;
	for(int i=i_start; i<i_end; i+=i_step) {
		float z = input_k[i];
		if(buffer[threadIdx.x] < z) {
			buffer[threadIdx.x] = z;
		}
	}

	 __syncthreads();

	 // reduce
	 //first thread goes through and finds the max element in the buffer
	 //after this stage the max element for dim items is found
	for(int stride=SOFTMAX_THREADS/2; stride>32; stride>>=1) {
		if(tid < stride) {
			buffer[tid] = (buffer[tid] > buffer[stride + tid]) ? buffer[tid] : buffer[stride + tid];
		}
		__syncthreads();
	}

	if(tid<32) {
		warpReduceMax(buffer,tid);
	}

	__syncthreads();

	// sum
	//Now go through all the dim elements and subtract the max from the element, keep a running sum for the normalization constant
	float max_k = buffer[0];
	buffer[threadIdx.x] = 0;
	for (int i=i_start; i<i_end; i+=i_step) {
		float z = __expf(input_k[i]-max_k); //subtract the max from the input, then exp it for the softmax
		buffer[threadIdx.x] += z; //keep a running sum of these values for the normalization constant
		output_k[i] = z; //set the output as this value, then get ready to divide by the sum again
	}

 	__syncthreads();

 	// reduce
 	//Now sum all the elements in the buffer, for the normalization constant
 	for(int stride=SOFTMAX_THREADS/2; stride>32; stride>>=1) {
		if(tid < stride) {
			buffer[tid] += buffer[stride + tid];
		}
		__syncthreads();
	}

	if(tid<32) {
		warpReduceSum(buffer,tid);
	}

  	__syncthreads();

  	// normalize the softmax
	float sum_k = buffer[0];
	for (int i=i_start; i<i_end; i+=i_step) {
		output_k[i] = output_k[i] / sum_k;
	}
}


int main() { 

	const int rows = 20000;
	const int cols = 128;
	const int num_trials = 100;
	const int minibatch_size = 128;

	cudaStream_t streams[minibatch_size];
	for(int i=0; i<minibatch_size; i++) {
		cudaStreamCreate(&streams[i]);
	}


	thrust::device_vector<float> thrust_d_1(rows * cols,5);
	
	thrust::device_ptr<float> d_1_ptr = &thrust_d_1[0];

	thrust::device_vector<float> result(minibatch_size, -1);

	float *d_result = thrust::raw_pointer_cast(&result[0]);

	float *d_outputdist = thrust::raw_pointer_cast(&thrust_d_1[0]);

	thrust_d_1[1001] = 101.123456789f;

	std::cout << thrust_d_1[1001] << "\n";

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	std::chrono::duration<double> elapsed_seconds;
	float max_val=-1.01;
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		max_val = thrust::reduce(thrust_d_1.begin(), thrust_d_1.end(),
                              -1.0f,
                              thrust::maximum<float>());
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test1: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;
    std::cout << max_val << "\n";


	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		outputdist_overflow_prevention_kernel<<<cols,SOFTMAX_THREADS>>>(d_outputdist, d_outputdist, rows);
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test2: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

    start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		outputdist_overflow_prevention_kernel_v2<<<cols,SOFTMAX_THREADS>>>(d_outputdist, d_outputdist, rows);
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test3: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

    start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		outputdist_overflow_prevention_kernel_v3<<<cols,SOFTMAX_THREADS>>>(d_outputdist, d_outputdist, rows);
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test4: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

}

