//Kernel for the sparse lookups

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <time.h>

//W is the embedding matrix
//d_lookup is the (hidden state size)x(minibatch size) matrix
//Note that W is stored in column major
//Row size and col size



// __global__ 
// void sparse_lookup(float *d_lookup, float *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size,int vocab_size)
// {
// 	//Each block is responsible for copying one column of d_W to d_lookup
// 	d_lookup[IDX2C(threadIdx.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
// }


//n is the number of data elements in single list

struct ElementWiseProductBasic : public thrust::binary_function<float,float,float>
{
    __host__ __device__
    float operator()(const float& v1, const float& v2) const
    {
        float res;
        res = v1 * v2;
        return res;
    }
};


__global__ 
void elementwise_mult_kernel(const int n, float *d_mat1,float *d_mat2,float *d_final) 
{
  CUDA_KERNEL_LOOP(index, n) {
    d_final[index] = d_mat1[index] * d_mat2[index];
  }
}


__global__ 
void elementwise_mult_kernel_1(int hiddenstate_size, float *d_mat1,float *d_mat2,float *d_final) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size)
		d_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_mat1[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_mat2[IDX2C(idx,blockIdx.x,hiddenstate_size)];
}

__global__ 
void elementwise_mult_kernel_2(int hiddenstate_size, float *d_mat1,float *d_mat2,float *d_final) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_mat1[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_mat2[IDX2C(idx,blockIdx.x,hiddenstate_size)];

	}
}


__global__ 
void elementwise_mult_kernel_3(int hiddenstate_size, float *d_mat1,float *d_mat2,float *d_final) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_mat1[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_mat2[IDX2C(idx,blockIdx.x,hiddenstate_size)];
	}
}



template<typename Derived>
void eigen_test(const Eigen::MatrixBase<Derived> &h_mat1,const Eigen::MatrixBase<Derived> &h_mat2,
	const Eigen::MatrixBase<Derived> &h_mat_final_const)
{
	UNCONST(Derived,h_mat_final_const,h_mat_final);
	h_mat_final = h_mat1.array()*h_mat2.array();
}


int main() {
	const int minibatch_size = 128;
	const int hiddenstate_size = 1000;
	const int num_trials =100;

	float *h_mat1;
	float *d_mat1;
	float *h_mat2;
	float *d_mat2;
	float *h_mat_final;
	float *d_mat_final;


	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_mat1(hiddenstate_size,minibatch_size);
	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_mat2(hiddenstate_size,minibatch_size);
	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_mat_final(hiddenstate_size,minibatch_size);

	//initialize cuBLAS
	std::cout << "\n";
	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");
	
	//Init matrices on CUDA
	full_matrix_setup(&h_mat1,&d_mat1,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_mat2,&d_mat2,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_mat_final,&d_mat_final,hiddenstate_size,minibatch_size);

	//Copy the matrices to eigen
	copy_to_eigen(h_eigen_mat1,h_mat1);
	copy_to_eigen(h_eigen_mat2,h_mat2);
	copy_to_eigen(h_eigen_mat_final,h_mat_final);

	//Run the CAFFE Kernel
	for(int i=0; i<num_trials; i++) {
		elementwise_mult_kernel<<<GET_BLOCKS(minibatch_size*hiddenstate_size),CUDA_NUM_THREADS>>>(minibatch_size*hiddenstate_size ,d_mat1,d_mat2,d_mat_final);
		CUDA_GET_LAST_ERROR();
	}


	// for(int i=0; i<num_trials; i++) {
	// 	int threads_per_block = 512;
	// 	int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
	// 	dim3 kernel1(minibatch_size,num_block,1);
	// 	elementwise_mult_kernel_1<<<kernel1,threads_per_block>>>(hiddenstate_size
	// 		,d_mat1,d_mat2,d_mat_final);
	// 	CUDA_GET_LAST_ERROR();
	// }

	for(int i=0; i<num_trials; i++) {
		int threads_per_block = 256;
		int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
		dim3 kernel2(minibatch_size,num_block,1);
		elementwise_mult_kernel_2<<< kernel2,threads_per_block >>>(hiddenstate_size , d_mat1,d_mat2,d_mat_final);
		CUDA_GET_LAST_ERROR();
	}

	for(int i=0; i<num_trials; i++) {
		int threads_per_block = 128;
		int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
		dim3 kernel3(minibatch_size,num_block,1);
		elementwise_mult_kernel_3<<< kernel3,threads_per_block >>>(hiddenstate_size	,d_mat1,d_mat2,d_mat_final);
		CUDA_GET_LAST_ERROR();
	}

	cudaDeviceSynchronize();

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	//Eigen code timing
	for(int i=0; i<num_trials; i++) {
		eigen_test(h_eigen_mat1,h_eigen_mat2,h_eigen_mat_final);
	}
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of Eigen: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	get_matrix_cuBLAS(h_mat_final,d_mat_final,hiddenstate_size,minibatch_size);

	if(eigen_check_thres(h_eigen_mat_final,h_mat_final,0.001f)) {
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}


	// const size_t N = minibatch_size * hiddenstate_size;

 //    srand(time(NULL));

 //    thrust::host_vector<float>   h_A(N);
 //    thrust::host_vector<float>   h_B(N);

 //    initialize_thrust_vector(h_A, N);
 //    initialize_thrust_vector(h_B, N);


 //    thrust::device_vector<float> d_A = h_A;
 //    thrust::device_vector<float> d_B = h_B;


 //    thrust::host_vector<float> h_result(N);
 //    thrust::host_vector<float> h_result_modified(N);

 //    thrust::device_vector<float> d_result(N);

 //    // for(int i = 0; i < num_trials; i++)
 //    // {
 //    //     thrust::transform(d_A.begin(), d_A.end(),
 //    //                       d_B.begin(), d_result.begin(),
 //    //                       ElementWiseProductBasic());
 //    // }
 //    // h_result = d_result;
}

