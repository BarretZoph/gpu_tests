

template<typename dType>
NCE_layer<dType>::NCE_layer(int LSTM_size,int minibatch_size,int output_vocab_size,int num_negative_samples,int longest_sent,std::string file_name) {


	this->LSTM_size = LSTM_size;
	this->minibatch_size = minibatch_size;
	this->output_vocab_size = output_vocab_size;
	this->num_negative_samples = num_negative_samples;
	this->longest_sent = longest_sent;


	full_matrix_setup(&h_temp,&d_D,LSTM_size,output_vocab_size);
	full_matrix_setup(&h_temp,&d_b_d,output_vocab_size,1);
	full_matrix_setup(&h_temp,&d_h_t,LSTM_size,minibatch_size);
	full_matrix_setup(&h_temp,&d_temp_embeddings,LSTM_size,num_negative_samples + minibatch_size);
	full_matrix_setup(&h_temp,&d_dot_products,num_negative_samples + minibatch_size,minibatch_size);
	full_matrix_setup(&h_temp,&d_p_true,minibatch_size,num_negative_samples + minibatch_size);

	full_matrix_setup(&h_temp,&d_D_grad,LSTM_size,output_vocab_size);
	full_matrix_setup(&h_temp,&d_temp_D_grad,LSTM_size,num_negative_samples);
	full_vector_setup(&h_temp,&d_b_d_grad,output_vocab_size);

	full_matrix_setup(&h_temp,&d_d_ERRt_ht,LSTM_size,minibatch_size);

	full_vector_setup_ones(&h_temp,&d_ones,minibatch_size);

	h_vocab_indicies = (int *)malloc( (longest_sent * (num_negative_samples + minibatch_size))*sizeof(int));
	h_sampling_probs = (dType *)malloc( (longest_sent * (num_negative_samples + minibatch_size))*sizeof(dType));

	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_vocab_indicies, (longest_sent * (num_negative_samples + minibatch_size))*sizeof(int)),"GPU memory allocation failed\n");
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_sampling_probs, (longest_sent * (num_negative_samples + minibatch_size))*sizeof(dType)),"GPU memory allocation failed\n");
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_OBJ_val_temp, NUM_NCE_THREADS*sizeof(double)),"GPU memory allocation failed\n");
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_final_NCE_OBJ, 1*sizeof(double)),"GPU memory allocation failed\n");
	cudaMemset(d_final_NCE_OBJ,0,1*sizeof(double));

	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_temp_b_d_grad,(num_negative_samples)*sizeof(dType)),"GPU memory allocation failed\n");

	h_unique_indicies = (int *)malloc( (longest_sent * (num_negative_samples + minibatch_size))*sizeof(int));
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_unique_indicies,longest_sent*(num_negative_samples+minibatch_size)*sizeof(int)),"GPU memory allocation failed\n");

	std::vector<long long int> unigram_counts(output_vocab_size);
	std::fill(unigram_counts.begin(),unigram_counts.end(),0);
	get_unigram_counts(unigram_counts,file_name); //fill the unigram counts for sampling using the alias method
	unigram = multinomial<long long int,double> (unigram_counts);

	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");

	cudaMemset(d_b_d_grad,0,output_vocab_size*sizeof(dType));
	cudaMemset(d_D_grad,0,output_vocab_size*LSTM_size*sizeof(dType));

	thrust_d_b_d_grad = thrust::device_pointer_cast(d_b_d_grad);


	//for sparse stuff
	h_vocab_indicies_SPARSE = (int *)malloc( (num_negative_samples+1)*minibatch_size*longest_sent*sizeof(int));
	h_sampling_probs_SPARSE = (dType *)malloc( (num_negative_samples+1)*minibatch_size*longest_sent*sizeof(dType));
	full_matrix_setup(&h_temp,&d_dot_products_SPARSE,num_negative_samples+1,minibatch_size);
	full_matrix_setup(&h_temp,&d_p_true,minibatch_size,num_negative_samples+1);
}




//this will compute all of the negative samples for a minibatch
template<typename dType>
void NCE_layer<dType>::get_unigram_counts(std::vector<long long int> &unigram_counts,std::string file_name) {

	// std::ifstream data_file;
	// data_file.open(file_name.c_str(),std::ifstream::in);
	// std::string line;
	// std::string word;

	// while(std::getline(data_file, line)) {
	// 	std::getline(data_file, line); //get the target line
	// 	std::istringstream iss_input_source(line, std::istringstream::in);

	// 	while( iss_input_source >> word ) {
 //    		if(std::stoi(word) !=-1) {
 //    			unigram_counts[std::stoi(word)]++;
 //    		}
	// 	}
	// }

	//fill with random counts
	boost::uniform_real<> distribution(0,1);
	for(int i=0; i<unigram_counts.size(); i++) {
		unigram_counts[i] = (int)(1000)*distribution(gen);
	}


	// for(int i=0; i<unigram_counts.size(); i++) {
	// 	std::cout << "unigram counts: " << i << " : " << unigram_counts[i] << "\n";
	// }
	//data_file.close();
}




//prep the indicies
template<typename dType>
void NCE_layer<dType>::prep_GPU_vocab_indices(int *h_input_vocab_indicies_target,int current_target_length,int minibatch_size) {

	int curr_index = 0;
	int curr_unique_index = 0;
	std::unordered_map<int,bool> unqiue_check;
	//current target length
	for(int i=0; i<current_target_length; i++) {

		//generate the samples
		for(int j=0; j<num_negative_samples; j++) {
			h_vocab_indicies[curr_index] = unigram.sample(gen);
			h_sampling_probs[curr_index] = std::log(num_negative_samples*unigram.prob(h_vocab_indicies[curr_index]));

			if(unqiue_check.count(h_vocab_indicies[curr_index])==0) {
				unqiue_check[h_vocab_indicies[curr_index]] = true;
				h_unique_indicies[curr_unique_index] = h_vocab_indicies[curr_index];
				curr_unique_index++;
			}

			curr_index++;
		}
		for(int j=0; j<minibatch_size; j++) {
			h_vocab_indicies[curr_index] = h_input_vocab_indicies_target[j + minibatch_size*i];
			h_sampling_probs[curr_index] = std::log(num_negative_samples*unigram.prob(h_vocab_indicies[curr_index]));

			if(unqiue_check.count(h_vocab_indicies[curr_index])==0) {
				unqiue_check[h_vocab_indicies[curr_index]] = true;
				h_unique_indicies[curr_unique_index] = h_vocab_indicies[curr_index];
				curr_unique_index++;
			}

			curr_index++;
		}
	}

	curr_num_unique = curr_unique_index;
	cudaMemcpy(d_vocab_indicies, h_vocab_indicies, current_target_length*(minibatch_size + num_negative_samples)*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_unique_indicies, h_unique_indicies, curr_num_unique*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_sampling_probs, h_sampling_probs, current_target_length*(minibatch_size + num_negative_samples)*sizeof(dType), cudaMemcpyHostToDevice);
}


template<typename dType>
double NCE_layer<dType>::forward_prop() {


	//check to be sure


	cudaMemset(d_final_NCE_OBJ,0,1*sizeof(dType));
	cudaDeviceSynchronize();
	// std::cout << "PRINTING SAMPLES\n";
	// print_GPU_Matrix(d_vocab_indicies,1,num_negative_samples+minibatch_size);

	//load in the embeddings
	load_in_embeddings<<<256,256>>>(d_temp_embeddings,d_D,d_vocab_indicies,num_negative_samples+minibatch_size,LSTM_size);
	CUDA_GET_LAST_ERROR("ERROR IN KERNEL LOAD embeddings");

	
	//multiply h_t by the embeddings
	dType alpha = 1;
	dType beta = 0;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle, CUBLAS_OP_T, CUBLAS_OP_N,
		num_negative_samples + minibatch_size, minibatch_size, LSTM_size, &alpha, d_temp_embeddings, LSTM_size,
		d_h_t, LSTM_size, &beta, d_dot_products, num_negative_samples + minibatch_size),"forward h_t with embeddings failed\n");

	//compute -P(true) for all of the elements, also add in the bias at this step
	calc_p_true_kernel<<<256,256>>>(d_p_true,d_dot_products,d_sampling_probs,d_b_d,d_vocab_indicies,num_negative_samples+minibatch_size,minibatch_size);
	CUDA_GET_LAST_ERROR("ERROR IN KERNEL CALC P_TRUE");


	//get the objective function for NCE
	objective_val_p1_NCE_kernel<<<NUM_NCE_THREADS,NUM_NCE_THREADS>>>(d_p_true,d_OBJ_val_temp,num_negative_samples,minibatch_size);
	objective_val_p2_NCE_kernel<<<1,1>>>(d_p_true,d_final_NCE_OBJ,d_OBJ_val_temp,num_negative_samples,minibatch_size);

	cudaDeviceSynchronize();
	CUDA_GET_LAST_ERROR("HHHHHHHHHHHH");

	// cudaDeviceSynchronize();
	// std::cout << "PRINTING EMBEDDINGS FULL\n";
	// print_GPU_Matrix(d_D,LSTM_size,output_vocab_size);

	// cudaDeviceSynchronize();
	// std::cout << "PRINTING EMBEDDINGS SMALL\n";
	// print_GPU_Matrix(d_temp_embeddings,LSTM_size,num_negative_samples+minibatch_size);

	cudaDeviceSynchronize();
	dType NCE_OBJ;
	cudaMemcpy(&NCE_OBJ,d_final_NCE_OBJ,1*sizeof(dType),cudaMemcpyDeviceToHost);
	//std::cout << "NCE Objective function value: " << NCE_OBJ << "\n";
	cudaMemset(d_final_NCE_OBJ,0,1*sizeof(dType));
	return NCE_OBJ;

}


//prep the indicies
template<typename dType>
void NCE_layer<dType>::prep_GPU_vocab_indices_SPARSE(int *h_input_vocab_indicies_target,int current_target_length,int minibatch_size) {

	int curr_index = 0;
	int curr_unique_index = 0;
	std::unordered_map<int,bool> unqiue_check;
	//current target length
	for(int i=0; i<current_target_length; i++) {

		for(int k=0; k<minibatch_size; k++) {
			//generate the samples
			for(int j=0; j<num_negative_samples; j++) {
				h_vocab_indicies_SPARSE[curr_index] = unigram.sample(gen);
				h_sampling_probs_SPARSE[curr_index] = std::log(num_negative_samples*unigram.prob(h_vocab_indicies_SPARSE[curr_index]));

				if(unqiue_check.count(h_vocab_indicies[curr_index])==0) {
					unqiue_check[h_vocab_indicies[curr_index]] = true;
					h_unique_indicies_SPARSE[curr_unique_index] = h_vocab_indicies_SPARSE[curr_index];
					curr_unique_index++;
				}

				curr_index++;
			}
			
			h_vocab_indicies_SPARSE[curr_index] = h_input_vocab_indicies_target[k + minibatch_size*i];
			h_sampling_probs_SPARSE[curr_index] = std::log(num_negative_samples*unigram.prob(h_vocab_indicies_SPARSE[curr_index]));

			if(unqiue_check.count(h_vocab_indicies_SPARSE[curr_index])==0) {
				unqiue_check[h_vocab_indicies_SPARSE[curr_index]] = true;
				h_unique_indicies_SPARSE[curr_unique_index] = h_vocab_indicies_SPARSE[curr_index];
				curr_unique_index++;
			}
			curr_index++;
		}
	}

	curr_num_unique = curr_unique_index;
	cudaMemcpy(d_vocab_indicies_SPARSE, h_vocab_indicies_SPARSE, current_target_length*(minibatch_size*(num_negative_samples+1))*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_unique_indicies_SPARSE, h_unique_indicies_SPARSE, curr_num_unique*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_sampling_probs_SPARSE, h_sampling_probs_SPARSE, current_target_length*(minibatch_size*(num_negative_samples+1))*sizeof(dType), cudaMemcpyHostToDevice);
}


template<typename dType>
double NCE_layer<dType>::forward_prop_SPARSE() {

	nce_dot_product_SPARSE<<<256,NUM_NCE_THREADS>>>(d_dot_products_SPARSE,d_D,d_h_t,d_vocab_indicies_SPARSE,LSTM_size,minibatch_size,num_negative_samples+1,output_vocab_size);

	calc_p_true_kernel<<<256,256>>>(d_p_true_SPARSE,d_dot_products_SPARSE,d_sampling_probs_SPARSE,d_b_d,d_vocab_indicies_SPARSE,num_negative_samples+1,minibatch_size);
}


template<typename dType>
void NCE_layer<dType>::back_prop() {


	//calculate the error with respect to h_t
	//negative part
	dType alpha = 1;
	dType beta = 0;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle, CUBLAS_OP_N, CUBLAS_OP_T,
		LSTM_size, minibatch_size, num_negative_samples, &alpha, d_temp_embeddings, LSTM_size,
		d_p_true, minibatch_size, &beta, d_d_ERRt_ht, LSTM_size),"error h_t negative failed\n");

	//positive part
	error_ht_positive_kernel<<<256,256>>>(d_d_ERRt_ht,d_p_true,d_temp_embeddings + num_negative_samples*LSTM_size,num_negative_samples,LSTM_size,minibatch_size);
	CUDA_GET_LAST_ERROR("ERROR IN KERNEL error ht positive");


	//calculare the error with respect to D
	//negative part
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle, CUBLAS_OP_N, CUBLAS_OP_N,
		LSTM_size, num_negative_samples, minibatch_size, &alpha, d_h_t, LSTM_size,
		d_p_true, minibatch_size, &beta, d_temp_D_grad, LSTM_size),"error D negative failed\n");

	//now send them to the d_Grad
	negative_embedding_NCE<<<256,256>>>(d_temp_D_grad,d_D_grad,d_vocab_indicies,num_negative_samples,LSTM_size);
	CUDA_GET_LAST_ERROR("ERROR IN KERNEL error negative embeddings positive");

	//positive embeddings update
	positive_embedding_NCE<<<256,256>>>(d_h_t,d_D_grad,d_p_true+num_negative_samples*minibatch_size,d_vocab_indicies+num_negative_samples,LSTM_size,minibatch_size);
	CUDA_GET_LAST_ERROR("ERROR IN KERNEL error positive embeddings positive");


	//calculate error with respect to b_d
	//negative part
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_T,minibatch_size,num_negative_samples,&alpha,d_p_true,minibatch_size,
		d_ones,1,&beta,d_temp_b_d_grad,1),"cuBLAS normaliztion failed\n");

	//now send them off to b_d_grad
	negative_bias_NCE<<<256,256>>>(d_temp_b_d_grad,d_b_d_grad,d_vocab_indicies,num_negative_samples);

	//positive part
	positive_bias_NCE<<<256,256>>>(d_b_d_grad,d_p_true,d_vocab_indicies,minibatch_size,num_negative_samples);

	//cudaDeviceSynchronize();
}


template<typename dType>
double NCE_layer<dType>::back_prop_SPARSE() {
	
	

}


template<typename dType>
void NCE_layer<dType>::clear_gradients() {

	cudaMemset(d_b_d_grad,0,output_vocab_size*sizeof(dType));
	// cudaMemset(d_D_grad,0,output_vocab_size*LSTM_size*sizeof(dType));

	//smarter zeroing of the gradients

	int threads_per_block = 256;
	int num_block = (LSTM_size + threads_per_block-1)/threads_per_block;
	dim3 kernel(num_block,256,1);
	zero_W_gradient<<<kernel,threads_per_block>>>(d_D_grad,d_unique_indicies,LSTM_size,curr_num_unique);
}



template<typename dType>
void NCE_layer<dType>::update_weights() {

	//scale the gradients
	scale_functor unary_op(minibatch_size);
	thrust::for_each(thrust_d_b_d_grad,thrust_d_b_d_grad + output_vocab_size,unary_op);

	int threads_per_block = 256;
	int num_block = (LSTM_size + threads_per_block-1)/threads_per_block;
	dim3 kernel(num_block,256,1);
	scale_W_gradient<<<kernel,threads_per_block>>>(d_D_grad,d_unique_indicies,LSTM_size,((dType)1.0)/minibatch_size,curr_num_unique);
	CUDA_GET_LAST_ERROR();



	//now add the gradients
	//special D_grad
	update_W_gradient<<<kernel,threads_per_block>>>(d_D,d_D_grad,d_unique_indicies,learning_rate,LSTM_size,curr_num_unique);
	CUDA_GET_LAST_ERROR();


	gradient_update_mats<<<std::min(256,(output_vocab_size + 256 - 1)/256),256>>>(d_b_d,d_b_d_grad,learning_rate,output_vocab_size*1);
}




