//CUDA transpose benchmark
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include "BZ_CUDA_UTIL.h"

int main() {
	const int num_trials = 100;
	const int hiddenstate_size = 1000;
	const int minibatch_size = 128;
	const int output_vocab_size = 20000;
	bool regular = false;

	//host pointers
	float *h_D;
	float *h_D_trans;
	float *h_h_t;
	float *h_result;

	//device pointers
	float *d_D;
	float *d_D_trans;
	float *d_h_t;
	float *d_result;

	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");

	full_matrix_setup(&h_D,&d_D,output_vocab_size,hiddenstate_size);
	full_matrix_setup(&h_D_trans,&d_D_trans,hiddenstate_size,output_vocab_size);
	full_matrix_setup(&h_result,&d_result,output_vocab_size,minibatch_size);
	full_matrix_setup(&h_h_t,&d_h_t,hiddenstate_size,minibatch_size);

	float alpha = 1.0f;
	float beta = 0.0f;
	//non transpose
	// if(regular) {
	// 	for(int i=0; i<num_trials; i++) {
	// 		CUBLAS_ERROR_WRAPPER(cublasSgemm(handle,CUBLAS_OP_N,CUBLAS_OP_N,output_vocab_size,minibatch_size,hiddenstate_size,
	// 			&alpha,d_D,output_vocab_size,d_h_t,hiddenstate_size,&beta,d_result,output_vocab_size),"non transpose failed\n");
	// 	}
	// }
	// else {
	// 	//transpose
	// 	for(int i=0; i<num_trials; i++) {
	// 		CUBLAS_ERROR_WRAPPER(cublasSgemm(handle,CUBLAS_OP_T,CUBLAS_OP_N,output_vocab_size,minibatch_size,hiddenstate_size,&alpha,
	// 			d_D_trans,hiddenstate_size,d_h_t,hiddenstate_size,&beta,d_result,output_vocab_size),"tranpose failed\n");
	// 	}
	// }

	if(regular) {
		for(int i=0; i<num_trials; i++) {
			CUBLAS_ERROR_WRAPPER(cublasSgemm(handle,CUBLAS_OP_N,CUBLAS_OP_N,output_vocab_size,minibatch_size,hiddenstate_size,
				&alpha,d_D,output_vocab_size,d_h_t,hiddenstate_size,&beta,d_result,output_vocab_size),"non transpose failed\n");
		}
	}
	else {
		//transpose
		for(int i=0; i<num_trials; i++) {
			CUBLAS_ERROR_WRAPPER(cublasSgemm(handle,CUBLAS_OP_T,CUBLAS_OP_N,output_vocab_size,minibatch_size,hiddenstate_size,&alpha,
				d_D_trans,hiddenstate_size,d_h_t,hiddenstate_size,&beta,d_result,output_vocab_size),"tranpose failed\n");
		}
	}

	get_matrix_cuBLAS(h_result,d_result,output_vocab_size,minibatch_size);
	//print_matrix(h_result,output_vocab_size,minibatch_size);

}