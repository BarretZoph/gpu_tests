//Kernel for the sparse lookups

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "BZ_CUDA_UTIL.h"

#include "Eigen/Dense"
#include <chrono>

struct sigmoid_functor {
  double operator() (double x) const { return 1.0/(1.0+std::exp(-x)); }
};

//Does elementwise sigmoid operation on matrix
template <typename DerivedIn, typename DerivedOut>
void elemwise_sigmoid(const Eigen::MatrixBase<DerivedIn> &input_const, const Eigen::MatrixBase<DerivedOut> &output_const) {
	UNCONST(DerivedOut, output_const, output);
	output = input_const.array().unaryExpr(sigmoid_functor());
	output.matrix();
}

#define UNCONST(t,c,uc) Eigen::MatrixBase<t> &uc = const_cast<Eigen::MatrixBase<t>&>(c);

template <typename Dtype>
__global__ void sigmForward(const int n, const Dtype* in, Dtype* out) {
  CUDA_KERNEL_LOOP(index, n) {
    out[index] = tanh(in[index]);
  }
}

//W is the embedding matrix
//d_lookup is the (hidden state size)x(minibatch size) matrix
//Note that W is stored in column major
//Row size and col size
__global__ 
void sigm_elemwise_32(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf( -d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

__global__ 
void sigm_elemwise_64(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf( -d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

__global__ 
void sigm_elemwise_128(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

__global__ 
void sigm_elemwise_128_exp(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + __expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

__global__ 
void sigm_elemwise_256(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

__global__ 
void sigm_elemwise_512(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

__global__ 
void sigm_elemwise_1024(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

__global__ 
void sigm_elemwise_1000(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

//1024 threads per block with the number of blocks being the minibatch size
// __global__ 
// void sigm_elemwise_1(float *d_result, float *d_matrix, int rows, int cols)
// {
// 	//Each block is responsible for copying one column of d_W to d_lookup
// 	if(threadIdx.x < hiddenstate_size)
// 		d_lookup[IDX2C(threadIdx.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
// }

// //Grid is (size of minibatch x 4) since each block is 128x1 responsible for doing 1/4th of the transfer of a column
// __global__ 
// void sigm_elemwise_2(float *d_result, float *d_matrix, int rows, int cols)
// {
// 	//Each block is responsible for copying one column of d_W to d_lookup
// 	if(threadIdx.x+blockIdx.y*blockDim.x < hiddenstate_size)
// 		d_lookup[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
// }


int main() {

	int hiddenSize = 1000;
	int miniBatchSize = 128;

	const int num_trials = 100;

	float *d_matrix;
	float *h_matrix;

	float *h_result;
	float *d_result;	

	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_input(hiddenSize, miniBatchSize);
	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_result(hiddenSize, miniBatchSize);

	//initialize cuBLAS
	std::cout << "\n";
	full_matrix_setup(&h_matrix, &d_matrix, hiddenSize, miniBatchSize);
	full_matrix_setup(&h_result, &d_result, hiddenSize, miniBatchSize);	

	copy_to_eigen(h_eigen_input, h_matrix);



	int threadsPerBlock = 32;
	int numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	dim3 kernel32(miniBatchSize, numBlocks, 1 );

	for(int i=0; i<num_trials; i++) {
		sigm_elemwise_32<<<kernel32, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
		CUDA_GET_LAST_ERROR();
	}

	// threadsPerBlock = 64;
	// numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	// dim3 kernel64(miniBatchSize, numBlocks, 1 );

	// for(int i=0; i<num_trials; i++) {
	// 	sigm_elemwise_64<<<kernel64, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
	// 	CUDA_GET_LAST_ERROR();
	// }

	// int total_data = hiddenSize * miniBatchSize;
	// int blocks = GET_BLOCKS(total_data);
	// for(int i = 0; i < num_trials; i++){
	// 	sigmForward<<<blocks, CUDA_NUM_THREADS>>>(total_data, d_matrix, d_result);
	// }

	// threadsPerBlock = 128;
	// numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	// dim3 kernel128(miniBatchSize, numBlocks, 1 );


	// for(int i=0; i<num_trials; i++) {
	// 	sigm_elemwise_128<<<kernel128, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
	// 	CUDA_GET_LAST_ERROR();
	// }

	// threadsPerBlock = 256;
	// numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	// dim3 kernel256(miniBatchSize, numBlocks, 1 );

	// for(int i=0; i<num_trials; i++) {
	// 	sigm_elemwise_256<<<kernel256, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
	// 	CUDA_GET_LAST_ERROR();
	// }

	// threadsPerBlock = 512;
	// numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	// dim3 kernel512(miniBatchSize, numBlocks, 1 );

	// for(int i=0; i<num_trials; i++) {
	// 	sigm_elemwise_512<<<kernel512, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
	// 	CUDA_GET_LAST_ERROR();
	// }

	// threadsPerBlock = 1000;
	// numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	// dim3 kernel1000(miniBatchSize, numBlocks, 1 );

	// for(int i=0; i<num_trials; i++) {
	// 	sigm_elemwise_1000<<<kernel1000, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
	// 	CUDA_GET_LAST_ERROR();
	// }

	// threadsPerBlock = 1024;
	// numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	// dim3 kernel1024(miniBatchSize, numBlocks, 1 );

	// for(int i=0; i<num_trials; i++) {
	// 	sigm_elemwise_1024<<<kernel1024, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
	// 	CUDA_GET_LAST_ERROR();
	// }

	threadsPerBlock = 128;
	numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	dim3 kernel128(miniBatchSize, numBlocks, 1 );

	for(int i=0; i<num_trials; i++) {
		sigm_elemwise_128_exp<<<kernel128, threadsPerBlock >>>(d_result, d_matrix, hiddenSize);
		CUDA_GET_LAST_ERROR();
	}

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	
	//Eigen code timing
	for(int i=0; i<num_trials; i++) {
		elemwise_sigmoid( h_eigen_input, h_eigen_result );
	}

	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of Eigen: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	get_matrix_cuBLAS(h_result, d_result, hiddenSize, miniBatchSize);

	if(eigen_check_thres(h_eigen_result, h_result, 0.0001f))
	{
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}

}


