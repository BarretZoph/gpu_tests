#ifndef TRUNC_SOFTMAX_H
#define TRUNC_SOFTMAX_H

#include <unordered_map>
#include <vector>


template<typename dType>
class trunc_softmax {
public:

	int output_vocab_size;
	int LSTM_size;
	int minibatch_size;
	bool dropout;
	dType dropout_rate;

	int shortlist_size; //top most frequent words always being updates
	int sampled_size; //how many words to sample for each minibatch
	int trunc_size; //
	dType sample_correction;
	int shortlist_size_plus;//shortlist plus the unique words sampled in minibatch
	int cutoff; //At what index in the truncated softmax should the correct term be multiplied
	

	int *h_sampled_indicies; //these are the unqiue words in minibatch + sampled indicies
	int *d_sampled_indicies;
	int *h_vocab_indicies; //these store the actual indicies, or the new mapped ones if during training
	int *d_vocab_indicies;
	int *h_vocab_indicies_01;
	int *d_vocab_indicies_01;
	dType *h_vocab_indicies_01_float;
	dType *d_vocab_indicies_01_float;

	int *d_vocab_indicies_single;
	int *d_vocab_indicies_01_single;
	dType *d_vocab_indicies_01_float_single;

	cublasHandle_t handle;

	std::unordered_map<int,int> resevoir_mapping; //stores mapping for word in vocab to column in D matrix
	bool *bitmap;

	dType *d_D;
	dType *d_b_d;
	dType *d_D_small; //temp embeddings loaded once per minibatch
	dType *d_b_d_small; //temp bias loaded once per minibatch
	dType *d_outputdist;
	dType *d_outputdist_small; 

	dType *d_D_grad_small;
	dType *d_b_d_grad_small;

	dType *d_h_t; //not used

	dType *d_result;
	dType *d_temp_result;
	double *d_train_perplexity;

	std::vector<softmax_node<dType>> nodes;

	void init_loss_layer(int output_vocab_size,int LSTM_size,int minibatch_size,int shortlist_size,int sampled_size,int longest_sent);
	void zero_bitmap();
	void prep_GPU_vocab_indices(int *h_output_vocab_indicies_target,int current_target_length);
	void forward_prop(int index);
	void back_prop1(int index);
	void back_prop2(int index);
	void backprop_prep_GPU(dType *d_h_t,int step);
	void backprop_prep_GPU_mgpu(int step);
	void clear_gradients();
	void update_learning_rate(dType learning_rate);
	void update_weights();

};




#endif

