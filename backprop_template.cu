//Backprop for the GPU

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"
#include <thrust/transform_reduce.h>

//These are for floats for doubles
typedef float precision;


//-----------------------------------------backprop kernels-----------------------------------------


///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__ 
void d_ERRt_ct_kernel(float *d_d_ERRt_ct,float *d_d_ERRnTOt_ht,float *d_o_t,float *d_c_t,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
  		float val = tanhf(d_c_t[index]);
		d_d_ERRt_ct[index] = d_d_ERRnTOt_ht[index] * d_o_t[index] * (1.0f - val*val);
	}
}


__global__ 
void d_ERRt_ct_kernel(double *d_d_ERRt_ct,double *d_d_ERRnTOt_ht,double *d_o_t,double *d_c_t,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
  		double val = tanh(d_c_t[index]);
		d_d_ERRt_ct[index] = d_d_ERRnTOt_ht[index] * d_o_t[index] * (1.0f - val*val);
	}
}
///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////


///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__ 
void d_ERRnTOt_ot_kernel(float *d_d_ERRnTOt_ot,float *d_d_ERRnTOt_ht,float *d_o_t,float *d_c_t,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_d_ERRnTOt_ot[index] = d_d_ERRnTOt_ht[index] *  tanhf(d_c_t[index]) * d_o_t[index] * (1 - d_o_t[index]);
	}
}


__global__ 
void d_ERRnTOt_ot_kernel(double *d_d_ERRnTOt_ot,double *d_d_ERRnTOt_ht,double *d_o_t,double *d_c_t,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_d_ERRnTOt_ot[index] = d_d_ERRnTOt_ht[index] *  tanh(d_c_t[index]) * d_o_t[index] * (1 - d_o_t[index]);
	}
}

///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////

//For floats or doubles
template<typename dType>
__global__ 
void d_ERRnTOt_ft_it_kernel(dType *d_d_ERRnTOt,dType *d_d_ERRnTOt_ct,dType *d_single_err,dType *d_double_err,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_d_ERRnTOt[index] = d_d_ERRnTOt_ct[index] * d_single_err[index] * d_double_err[index] * (1 - d_double_err[index]);
	}
}


template<typename dType>
__global__ 
void d_ERRnTOt_tanhcpt_kernel(dType *d_d_ERRnTOt_tanhcpt,dType *d_d_ERRnTOt_ct,dType *d_i_t,dType *d_c_prime_t_tanh,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_d_ERRnTOt_tanhcpt[index] = d_d_ERRnTOt_ct[index] * d_i_t[index] * (1 -d_c_prime_t_tanh[index]*d_c_prime_t_tanh[index]);
	}
}


template<typename dType>
__global__ 
void zero_columns_kernel(int hiddenstate_size, dType *d_mat,int *d_vec,dType *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
		d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_vec[blockIdx.x];
	}
}


template<typename dType>
__global__ 
void add_four_matrices_kernel(dType *d_final,dType *d_mat1,dType *d_mat2,dType *d_mat3,dType *d_mat4,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_final[index] = d_mat1[index] + d_mat2[index] + d_mat3[index] + d_mat4[index];
	}
}


template<typename dType>
__global__ 
void elementwise_mult_kernel(dType *d_mat1,dType *d_mat2,dType *d_final,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_mat1[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_mat2[IDX2C(idx,blockIdx.x,hiddenstate_size)];
	}
}


template<typename dType>
__global__ 
void sparse_lookup_kernel(dType *d_lookup, dType *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size)
		d_lookup[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(idx,d_vocab_indices[blockIdx.x],hiddenstate_size)];
}



///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__
void W_gradient_kernel(float *d_W_grad,int *d_vocab_indices,float *temp1,float *temp2,float *temp3,
	float *temp4,int hiddenstate_size) 
{
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index_cols = IDX2C(idx,blockIdx.x,hiddenstate_size);
		float sum = temp1[index_cols] + temp2[index_cols] + temp3[index_cols] + temp4[index_cols];
		atomicAdd(&d_W_grad[IDX2C(idx,d_vocab_indices[blockIdx.x],hiddenstate_size)],sum);
	}
}


__global__
void W_gradient_kernel(double *d_W_grad,int *d_vocab_indices,double *temp1,double *temp2,double *temp3,
	double *temp4,int hiddenstate_size) 
{
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index_cols = IDX2C(idx,blockIdx.x,hiddenstate_size);
		double sum = temp1[index_cols] + temp2[index_cols] + temp3[index_cols] + temp4[index_cols];
		atomicAddDouble(&d_W_grad[IDX2C(idx,d_vocab_indices[blockIdx.x],hiddenstate_size)],sum);
	}
}
///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////


//------------------------------------------FORWARD PROP KERNELS---------------------------------------------


struct sigmoid_functor {
	template<typename dType>
	dType operator() (dType x) const { return 1.0f/(1.0f+std::exp(-x)); }
};



///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__
void forward_sigmoid_kernel(float *d_final,float *temp1,float *temp2,float *d_bias,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		float temp_val = temp1[index] + temp2[index] + d_bias[idx];
		d_final[index] = 1.0f/(1.0f + expf(-1.0f*temp_val));
	}
}

__global__
void forward_sigmoid_kernel(double *d_final,double *temp1,double *temp2,double *d_bias,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		double temp_val = temp1[index] + temp2[index] + d_bias[idx];
		d_final[index] = 1.0/(1.0 + exp(-1.0*temp_val));
	}
}

///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////


///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__
void forward_tanh_kernel(float *d_final,float *temp1,float *temp2,float *d_bias,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		float temp_val = temp1[index] + temp2[index] + d_bias[idx];
		d_final[index] = tanhf(temp_val);
	}
}

__global__
void forward_tanh_kernel(double *d_final,double *temp1,double *temp2,double *d_bias,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		double temp_val = temp1[index] + temp2[index] + d_bias[idx];
		d_final[index] = tanh(temp_val);
	}
}
///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////


template<typename dType>
__global__
void forward_c_t_kernel(dType *d_c_t,dType *d_f_t, dType *d_c_t_prev,dType *d_i_t,dType *d_c_prime_t_tanh,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_c_t[index] = d_f_t[index] * d_c_t_prev[index] + d_i_t[index] * d_c_prime_t_tanh[index];
	}
}


///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__
void forward_h_t_kernel(float *d_h_t,float *d_o_t, float *d_c_t,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_h_t[index] = d_o_t[index] * tanhf(d_c_t[index]);
	}
}

__global__
void forward_h_t_kernel(double *d_h_t,double *d_o_t,double *d_c_t,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_h_t[index] = d_o_t[index] * tanh(d_c_t[index]);
	}
}

///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////


template<typename dType>
__global__ 
void zero_c_t_and_h_t(dType *d_h_t,dType *d_c_t,int *d_vocab_indices_01,int hiddenstate_size) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
  	int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_h_t[index] = d_h_t[index] * d_vocab_indices_01[blockIdx.x];
		d_c_t[index] = d_c_t[index] * d_vocab_indices_01[blockIdx.x];
	}
}

//GPU pointers for convience
//host pointers
precision *h_d_ERRnTOtp1_ht;
precision *h_d_ERRnTOtp1_ct;
precision *h_d_ERRt_ht;
precision *h_d_ERRnTOt_ht;
precision *h_o_t;
precision *h_c_t;
precision *h_d_ERRt_ct;
precision *h_d_ERRnTOt_ct;
int *h_input_vocab_indices_01;
int *h_input_vocab_indices;
precision *h_d_ERRnTOt_ot;
precision *h_f_t;
precision *h_c_t_prev;
precision *h_d_ERRnTOt_ft;
precision *h_c_prime_t_tanh;
precision *h_i_t;
precision *h_d_ERRnTOt_tanhcpt;
precision *h_d_ERRnTOt_it;

precision *h_temp1;
precision *h_temp2;
precision *h_temp3;
precision *h_temp4;

precision *h_W_ho;
precision *h_W_hf;
precision *h_W_hi;
precision *h_W_hc;

precision *h_d_ERRnTOt_htM1;
precision *h_d_ERRnTOt_ctM1;

precision *h_W_hi_grad;
precision *h_W_hf_grad;
precision *h_W_hc_grad;
precision *h_W_ho_grad;

precision *h_h_t_prev;

precision *h_M_i_grad;
precision *h_M_f_grad;
precision *h_M_o_grad;
precision *h_M_c_grad;

precision *h_W;
precision *h_sparse_lookup;

precision *h_b_i_grad;
precision *h_b_f_grad;
precision *h_b_c_grad;
precision *h_b_o_grad;

precision *h_ones_minibatch;

precision *h_M_i;
precision *h_M_f;
precision *h_M_o;
precision *h_M_c;

precision *h_W_grad;

precision *h_b_i;
precision *h_b_f;
precision *h_b_c;
precision *h_b_o;

precision *h_temp5;
precision *h_temp6;

precision *h_h_t;

precision *h_temp7;
precision *h_temp8;

//device pointers
precision *d_d_ERRnTOtp1_ht;
precision *d_d_ERRnTOtp1_ct;
precision *d_d_ERRt_ht;
precision *d_d_ERRnTOt_ht;
precision *d_o_t;
precision *d_c_t;
precision *d_d_ERRt_ct;
precision *d_d_ERRnTOt_ct;
int *d_input_vocab_indices_01;
int *d_input_vocab_indices;
precision *d_d_ERRnTOt_ot;
precision *d_f_t;
precision *d_c_t_prev;
precision *d_d_ERRnTOt_ft;
precision *d_c_prime_t_tanh;
precision *d_i_t;
precision *d_d_ERRnTOt_tanhcpt;
precision *d_d_ERRnTOt_it;

precision *d_temp1;
precision *d_temp2;
precision *d_temp3;
precision *d_temp4;

precision *d_W_ho;
precision *d_W_hf;
precision *d_W_hi;
precision *d_W_hc;

precision *d_d_ERRnTOt_htM1;
precision *d_d_ERRnTOt_ctM1;

precision *d_W_hi_grad;
precision *d_W_hf_grad;
precision *d_W_hc_grad;
precision *d_W_ho_grad;

precision *d_h_t_prev;

precision *d_M_i_grad;
precision *d_M_f_grad;
precision *d_M_o_grad;
precision *d_M_c_grad;

precision *d_W;
precision *d_sparse_lookup;

precision *d_b_i_grad;
precision *d_b_f_grad;
precision *d_b_c_grad;
precision *d_b_o_grad;

precision *d_ones_minibatch;

precision *d_M_i;
precision *d_M_f;
precision *d_M_o;
precision *d_M_c;

precision *d_W_grad;

precision *d_b_i;
precision *d_b_f;
precision *d_b_c;
precision *d_b_o;

precision *d_temp5;
precision *d_temp6;

precision *d_h_t;

precision *d_temp7;
precision *d_temp8;


//stream stuff
cudaStream_t s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s25,s26,s27;

cudaEvent_t sparse_forward_start;
cudaEvent_t i_t_part1,i_t_full;
cudaEvent_t f_t_part1,f_t_full;
cudaEvent_t c_prime_t_tanh_part1,c_prime_t_tanh_full;
cudaEvent_t o_t_part1,o_t_full;

//backprop events
cudaEvent_t backprop_init;
cudaEvent_t err_ot_done;
cudaEvent_t err_ft_done;
cudaEvent_t err_tanhcpt_done;
cudaEvent_t err_it_done;

cudaEvent_t htm1_p1_done;
cudaEvent_t htm1_p2_done;
cudaEvent_t htm1_p3_done;
cudaEvent_t htm1_p4_done;

cudaEvent_t W_grad_p1_done;
cudaEvent_t W_grad_p2_done;
cudaEvent_t W_grad_p3_done;
cudaEvent_t W_grad_p4_done;

void compute_gradients(int hiddenstate_size,int minibatch_size,cublasHandle_t handle,int input_vocab_size);
void compute_gradients_eigen();

//Note that jacobians are stored transposed to minimize tranpose operations
void back_prop(int hiddenstate_size,int minibatch_size,cublasHandle_t handle,int input_vocab_size) {

	//DEBUGGING
	// get_matrix_cuBLAS(h_d_ERRt_ht,d_d_ERRt_ht,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_d_ERRnTOtp1_ht,d_d_ERRnTOtp1_ht,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRt_ht,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRnTOtp1_ht,hiddenstate_size,minibatch_size);





	//OPERATION
	//d_ERRnTOt_ht = d_ERRnTOtp1_ht + d_ERRt_ht;
	precision alpha = 1;
	precision beta = 1;
	CUBLAS_ERROR_WRAPPER(cublas_geam_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,&alpha,d_d_ERRnTOtp1_ht,hiddenstate_size,
		&beta,d_d_ERRt_ht,hiddenstate_size,d_d_ERRnTOt_ht,hiddenstate_size),"backprop addition failed d_ERRnTOt_ht\n");

	//OPERATION
	//d_ERRt_ct.transpose() = d_ERRnTOt_ht.transpose().array() * (o_t.array()*(1-(c_t).array().unaryExpr(tanh_sq_functor())));
	int threads_per_block = 128;
	int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
	dim3 kernel(minibatch_size,num_block,1);
	d_ERRt_ct_kernel<<<kernel,threads_per_block>>>(d_d_ERRt_ct,d_d_ERRnTOt_ht,d_o_t,d_c_t,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

//---------------------------------------------------------------------------------------------------------
	//NOTE REMOVE THIS WHEN DOING REAL MODEL IT IS NOT NEEDED AS VALUE IS COMPUTED IN FORWARD PROP
	sparse_lookup_kernel<<< kernel,threads_per_block>>>(d_sparse_lookup,d_W,d_input_vocab_indices,minibatch_size,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

	//DEBUGGING
	// get_matrix_cuBLAS(h_d_ERRt_ct,d_d_ERRt_ct,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_o_t,d_o_t,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_c_t,d_c_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRt_ct,hiddenstate_size,minibatch_size);
	// print_matrix(h_o_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_c_t,hiddenstate_size,minibatch_size);

	//OPERATION
	//d_ERRnTOt_ct = d_ERRnTOtp1_ct + d_ERRt_ct;
	CUBLAS_ERROR_WRAPPER(cublas_geam_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,&alpha,d_d_ERRnTOtp1_ct,hiddenstate_size,
		&beta,d_d_ERRt_ct,hiddenstate_size,d_d_ERRnTOt_ct,hiddenstate_size),"backprop addition failed, d_ERRnTOt_ct \n");

	//OPERATION
	//zero out columns of d_ERRnTOt_ht and d_ERRnTOt_ct
	zero_columns_kernel<<<kernel,threads_per_block>>>(hiddenstate_size, d_d_ERRnTOt_ht,d_input_vocab_indices_01,d_d_ERRnTOt_ht);
	CUDA_GET_LAST_ERROR();
	zero_columns_kernel<<<kernel,threads_per_block>>>(hiddenstate_size, d_d_ERRnTOt_ct,d_input_vocab_indices_01,d_d_ERRnTOt_ct);
	CUDA_GET_LAST_ERROR();

	//EVENT FOR FINISHING THE FIRST STUFF
	cudaEventRecord(backprop_init,0);

	//DEBUGGING
	// get_matrix_cuBLAS(h_d_ERRnTOt_ht,d_d_ERRnTOt_ht,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_d_ERRnTOt_ct,d_d_ERRnTOt_ct,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRnTOt_ht,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRnTOt_ct,hiddenstate_size,minibatch_size);

	//STARTING FROM THIS POINT STREAMS WILL BE USED

	//OPERATION
	//USING STREAM 1
	//d_ERRnTOt_ot.transpose() = d_ERRnTOt_ht.transpose().array()*( c_t.array().unaryExpr(tanh_functor()) )*o_t*(1-o_t);
	cudaStreamWaitEvent(s1,backprop_init,0);
	d_ERRnTOt_ot_kernel<<<kernel,threads_per_block,0,s1>>>(d_d_ERRnTOt_ot,d_d_ERRnTOt_ht,d_o_t,d_c_t,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(err_ot_done,s1);

	//OPERATION
	//USING STREAM 2
	//d_ERRnTOt_ft.transpose() = d_ERRnTOt_ct.transpose().array()*(c_t_prev.array())*f_t*(1-f_t);
	cudaStreamWaitEvent(s2,backprop_init,0);
	d_ERRnTOt_ft_it_kernel<<<kernel,threads_per_block,0,s2>>>(d_d_ERRnTOt_ft,d_d_ERRnTOt_ct,d_c_t_prev,d_f_t,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(err_ft_done,s2);

	//OPERATION
	//USING STREAM 3
	//d_ERRnTOt_tanhcpt.transpose() = d_ERRnTOt_ct.transpose().array()*(i_t.array());
	cudaStreamWaitEvent(s3,backprop_init,0);
	d_ERRnTOt_tanhcpt_kernel<<<kernel,threads_per_block,0,s3>>>(d_d_ERRnTOt_tanhcpt,d_d_ERRnTOt_ct,d_i_t,d_c_prime_t_tanh,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(err_tanhcpt_done,s3);
	
	//OPERATION
	//USING STREAM 4
	//d_ERRnTOt_it.transpose() = d_ERRnTOt_ct.transpose().array()*(c_prime_t_tanh.array());
	cudaStreamWaitEvent(s4,backprop_init,0);
	d_ERRnTOt_ft_it_kernel<<<kernel,threads_per_block,0,s4>>>(d_d_ERRnTOt_it,d_d_ERRnTOt_ct,d_c_prime_t_tanh,d_i_t,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(err_it_done,s4);

	//OPERATION
	//USING STREAM 5,6,7,8,9
	// d_ERRnTOt_htM1.transpose() = (W_ho.transpose()*( (d_ERRnTOt_ot.transpose().array() * o_t.array() * (1- o_t.array())).matrix() )) \
	// + (W_hf.transpose()*((d_ERRnTOt_ft.transpose().array() * f_t.array() *(1-f_t.array())).matrix())) \
	// + (W_hi.transpose()*((d_ERRnTOt_it.transpose().array()*i_t.array()*(1-i_t.array())).matrix())) \
	// + (W_hc.transpose()*((d_ERRnTOt_tanhcpt.transpose().array()*(1-c_prime_t_tanh.array().square())).matrix()));
	precision alpha2 = 1;
	precision beta2 = 0;



	cublasSetStream(handle,s5);
	cudaStreamWaitEvent(s5,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,
		&alpha2,d_W_ho,hiddenstate_size,d_d_ERRnTOt_ot,hiddenstate_size,&beta2,d_temp1,hiddenstate_size),"Error backprop temp1 htM1\n");
	cudaEventRecord(htm1_p1_done,s5);

	cublasSetStream(handle,s6);
	cudaStreamWaitEvent(s6,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,
		&alpha2,d_W_hf,hiddenstate_size,d_d_ERRnTOt_ft,hiddenstate_size,&beta2,d_temp2,hiddenstate_size),"Error backprop temp2 htM1\n");
	cudaEventRecord(htm1_p2_done,s6);

	cublasSetStream(handle,s7);
	cudaStreamWaitEvent(s7,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,
		&alpha2,d_W_hi,hiddenstate_size,d_d_ERRnTOt_it,hiddenstate_size,&beta2,d_temp3,hiddenstate_size),"Error backprop temp3 htM1\n");
	cudaEventRecord(htm1_p3_done,s7);

	cublasSetStream(handle,s8);
	cudaStreamWaitEvent(s8,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,
		&alpha2,d_W_hc,hiddenstate_size,d_d_ERRnTOt_tanhcpt,hiddenstate_size,&beta2,d_temp4,hiddenstate_size),"Error backprop temp4 htM1\n");
	cudaEventRecord(htm1_p4_done,s8);

	cudaStreamWaitEvent(s9,htm1_p1_done,0);
	cudaStreamWaitEvent(s9,htm1_p2_done,0);
	cudaStreamWaitEvent(s9,htm1_p3_done,0);
	cudaStreamWaitEvent(s9,htm1_p4_done,0);
	add_four_matrices_kernel<<< kernel,threads_per_block,0,s9>>>(d_d_ERRnTOt_htM1,d_temp1,d_temp2,d_temp3,d_temp4,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

	//DEBUGGING
	// get_matrix_cuBLAS(h_d_ERRnTOt_htM1,d_d_ERRnTOt_htM1,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_d_ERRnTOt_ctM1,d_d_ERRnTOt_ctM1,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRnTOt_htM1,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRnTOt_ctM1,hiddenstate_size,minibatch_size);

	//cudaDeviceSynchronize();

	//OPERATION
	//USING STREAM 10
	//d_ERRnTOt_ctM1.transpose() = (d_ERRnTOt_ct.transpose().array()*f_t.array());
	cudaStreamWaitEvent(s10,backprop_init,0);
	elementwise_mult_kernel<<<kernel,threads_per_block,0,s10>>>(d_d_ERRnTOt_ct,d_f_t,d_d_ERRnTOt_ctM1,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

	//DEBUGGING
	// get_matrix_cuBLAS(h_temp1,d_temp1,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_temp2,d_temp2,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_temp3,d_temp3,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_temp4,d_temp4,hiddenstate_size,minibatch_size);
	// print_matrix(h_temp1,hiddenstate_size,minibatch_size);
	// print_matrix(h_temp2,hiddenstate_size,minibatch_size);
	// print_matrix(h_temp3,hiddenstate_size,minibatch_size);
	// print_matrix(h_temp4,hiddenstate_size,minibatch_size);


	// get_matrix_cuBLAS(h_d_ERRnTOt_htM1,d_d_ERRnTOt_htM1,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_d_ERRnTOt_ctM1,d_d_ERRnTOt_ctM1,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRnTOt_htM1,hiddenstate_size,minibatch_size);
	// print_matrix(h_d_ERRnTOt_ctM1,hiddenstate_size,minibatch_size);
	compute_gradients(hiddenstate_size,minibatch_size,handle,input_vocab_size);

}

void compute_gradients(int hiddenstate_size,int minibatch_size,cublasHandle_t handle,int input_vocab_size) {

	//OPERATION
	//USING STREAMS 11,12,13,14
	//model->W_hi_grad.noalias() += (h_t_prev*(d_ERRnTOt_it.array() * i_t.transpose().array()*(1-i_t.transpose().array())).matrix()).transpose();
	//model->W_hf_grad.noalias() += (h_t_prev*(d_ERRnTOt_ft.array()*f_t.transpose().array()*(1-f_t.transpose().array())).matrix()).transpose();
	//model->W_hc_grad.noalias() += (h_t_prev*(d_ERRnTOt_ct.array()*(i_t.transpose().array())*(1-c_prime_t_tanh.transpose().array().square())).matrix()).transpose();
	//model->W_ho_grad.noalias() += (h_t_prev*(d_ERRnTOt_ot.array()*o_t.transpose().array()*(1-o_t.transpose().array())).matrix()).transpose();
	precision alpha = 1;
	precision beta = 1;

	cublasSetStream(handle,s11);
	cudaStreamWaitEvent(s11,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_it,hiddenstate_size,d_h_t_prev,hiddenstate_size,&beta,d_W_hi_grad,hiddenstate_size),"Backprop W_hi grad cublas gemm failed\n");

	cublasSetStream(handle,s12);
	cudaStreamWaitEvent(s12,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_ft,hiddenstate_size,d_h_t_prev,hiddenstate_size,&beta,d_W_hf_grad,hiddenstate_size),"Backprop W_hf grad cublas gemm failed\n");

	cublasSetStream(handle,s13);
	cudaStreamWaitEvent(s13,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_tanhcpt,hiddenstate_size,d_h_t_prev,hiddenstate_size,&beta,d_W_hc_grad,hiddenstate_size),"Backprop W_hc grad cublas gemm failed\n");

	cublasSetStream(handle,s14);
	cudaStreamWaitEvent(s14,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_ot,hiddenstate_size,d_h_t_prev,hiddenstate_size,&beta,d_W_ho_grad,hiddenstate_size),"Backprop W_ho grad cublas gemm failed\n");


	//DEBUGGING
	// get_matrix_cuBLAS(h_W_hi_grad,d_W_hi_grad,hiddenstate_size,hiddenstate_size);
	// get_matrix_cuBLAS(h_W_hf_grad,d_W_hf_grad,hiddenstate_size,hiddenstate_size);
	// get_matrix_cuBLAS(h_W_hc_grad,d_W_hc_grad,hiddenstate_size,hiddenstate_size);
	// get_matrix_cuBLAS(h_W_ho_grad,d_W_ho_grad,hiddenstate_size,hiddenstate_size);
	
	// print_matrix(h_W_hi_grad,hiddenstate_size,hiddenstate_size);
	// print_matrix(h_W_hf_grad,hiddenstate_size,hiddenstate_size);
	// print_matrix(h_W_hc_grad,hiddenstate_size,hiddenstate_size);
	// print_matrix(h_W_ho_grad,hiddenstate_size,hiddenstate_size);

	//OPERATION
	//USING STREAMS 15,16,17,18
	//compute_temp_mat(model->W);
	//model->M_i_grad.noalias() += (d_ERRnTOt_it.transpose().array() * i_t.array() * (1-i_t.array())).matrix() * temp_mat.transpose();
	//model->M_f_grad.noalias() += (d_ERRnTOt_ft.transpose().array() * f_t.array() * (1-f_t.array())).matrix() * temp_mat.transpose();
	//model->M_o_grad.noalias() += (d_ERRnTOt_ot.transpose().array() * o_t.array() * (1-o_t.array())).matrix() * temp_mat.transpose();
	//model->M_c_grad.noalias() += (d_ERRnTOt_tanhcpt.transpose().array() * (1-c_prime_t_tanh.array().square())).matrix() * temp_mat.transpose();
	alpha = 1;
	beta = 1;

	cublasSetStream(handle,s15);
	cudaStreamWaitEvent(s15,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_it,hiddenstate_size,d_sparse_lookup,hiddenstate_size,&beta,d_M_i_grad,hiddenstate_size),"Backprop M_i grad cublas gemm failed\n");

	cublasSetStream(handle,s16);
	cudaStreamWaitEvent(s16,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_ft,hiddenstate_size,d_sparse_lookup,hiddenstate_size,&beta,d_M_f_grad,hiddenstate_size),"Backprop M_f grad cublas gemm failed\n");

	cublasSetStream(handle,s17);
	cudaStreamWaitEvent(s17,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_ot,hiddenstate_size,d_sparse_lookup,hiddenstate_size,&beta,d_M_o_grad,hiddenstate_size),"Backprop M_o grad cublas gemm failed\n");

	cublasSetStream(handle,s18);
	cudaStreamWaitEvent(s18,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,hiddenstate_size,hiddenstate_size,minibatch_size,&alpha,
		d_d_ERRnTOt_tanhcpt,hiddenstate_size,d_sparse_lookup,hiddenstate_size,&beta,d_M_c_grad,hiddenstate_size),"Backprop M_c grad cublas gemm failed\n");


	//DEBUGGING
	// get_matrix_cuBLAS(h_M_i_grad,d_M_i_grad,hiddenstate_size,hiddenstate_size);
	// get_matrix_cuBLAS(h_M_f_grad,d_M_f_grad,hiddenstate_size,hiddenstate_size);
	// get_matrix_cuBLAS(h_M_o_grad,d_M_o_grad,hiddenstate_size,hiddenstate_size);
	// get_matrix_cuBLAS(h_M_c_grad,d_M_c_grad,hiddenstate_size,hiddenstate_size);
	
	// print_matrix(h_M_i_grad,hiddenstate_size,hiddenstate_size);
	// print_matrix(h_M_f_grad,hiddenstate_size,hiddenstate_size);
	// print_matrix(h_M_o_grad,hiddenstate_size,hiddenstate_size);
	// print_matrix(h_M_c_grad,hiddenstate_size,hiddenstate_size);

	//OPERATION
	//USING STREAMS 19,20,21,22
	//b_i_grad.noalias() += ((d_ERRnTOt_it.array() * (i_t.array() * (1-i_t.array())).matrix().transpose().array()).colwise().sum()).matrix().transpose();
	//b_f_grad.noalias() += ((d_ERRnTOt_ft.array() * (f_t.array() * (1-f_t.array())).matrix().transpose().array()).colwise().sum()).matrix().transpose();
	//b_c_grad.noalias() += (d_ERRnTOt_tanhcpt.array() * (1-c_prime_t_tanh.array().square()).matrix().transpose().array()).colwise().sum().matrix().transpose();
	//b_o_grad.noalias() += ((d_ERRnTOt_ot.array() * (o_t.array() * (1-o_t.array())).matrix().transpose().array()).colwise().sum()).matrix().transpose();
	cublasSetStream(handle,s19);
	cudaStreamWaitEvent(s19,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,hiddenstate_size,minibatch_size,&alpha,d_d_ERRnTOt_it,hiddenstate_size,
		d_ones_minibatch,1,&beta,d_b_i_grad,1),"backprop b_i_grad failed\n");

	cublasSetStream(handle,s20);
	cudaStreamWaitEvent(s20,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,hiddenstate_size,minibatch_size,&alpha,d_d_ERRnTOt_ft,hiddenstate_size,
		d_ones_minibatch,1,&beta,d_b_f_grad,1),"backprop b_i_grad failed\n");

	cublasSetStream(handle,s21);
	cudaStreamWaitEvent(s21,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,hiddenstate_size,minibatch_size,&alpha,d_d_ERRnTOt_ot,hiddenstate_size,
		d_ones_minibatch,1,&beta,d_b_o_grad,1),"backprop b_i_grad failed\n");

	cublasSetStream(handle,s22);
	cudaStreamWaitEvent(s22,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,hiddenstate_size,minibatch_size,&alpha,d_d_ERRnTOt_tanhcpt,hiddenstate_size,
		d_ones_minibatch,1,&beta,d_b_c_grad,1),"backprop b_i_grad failed\n");

	//DEBUGGING
	// get_matrix_cuBLAS(h_b_i_grad,d_b_i_grad,hiddenstate_size,1);
	// get_matrix_cuBLAS(h_b_f_grad,d_b_f_grad,hiddenstate_size,1);
	// get_matrix_cuBLAS(h_b_o_grad,d_b_o_grad,hiddenstate_size,1);
	// get_matrix_cuBLAS(h_b_c_grad,d_b_c_grad,hiddenstate_size,1);

	// print_matrix(h_b_i_grad,hiddenstate_size,1);
	// print_matrix(h_b_f_grad,hiddenstate_size,1);
	// print_matrix(h_b_o_grad,hiddenstate_size,1);
	// print_matrix(h_b_c_grad,hiddenstate_size,1);

	//OPERATION
	//USING STREAMS 23,24,25,26
	// Z_i = d_ERRnTOt_it.array()*(i_t.array() * (1-i_t.array())).matrix().transpose().array();
	// Z_f = d_ERRnTOt_ft.array()*(f_t.array() * (1-f_t.array())).matrix().transpose().array();
	// Z_o = d_ERRnTOt_ot.array()*(o_t.array() * (1-o_t.array())).matrix().transpose().array();
	// Z_c = d_ERRnTOt_tanhcpt.array()*(1-c_prime_t_tanh.array().square()).matrix().transpose().array();

	// for(int i=0; i<vocab_indices_input.rows(); i++) {
	// 	if(vocab_indices_input(i)!=-1) {
	// 		for(int j=0; j<model->W_grad.rows(); j++) {
	// 			double sumtemp = Z_i.row(i) * model->M_i.col(j);
	// 			sumtemp += Z_f.row(i) * model->M_f.col(j);
	// 			sumtemp += Z_o.row(i) * model->M_o.col(j);
	// 			sumtemp += Z_c.row(i) * model->M_c.col(j);
	// 			model->W_grad(j,vocab_indices_input(i)) += sumtemp;
	// 		}
	// 	}
	// }
	alpha = 1;
	beta = 0;
	cublasSetStream(handle,s23);
	cudaStreamWaitEvent(s23,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,
		hiddenstate_size,&alpha,d_M_i,hiddenstate_size,d_d_ERRnTOt_it,hiddenstate_size,&beta,
		d_temp1,hiddenstate_size),"cublas W gradient failed temp1\n");
	cudaEventRecord(W_grad_p1_done,s23);

	cublasSetStream(handle,s24);
	cudaStreamWaitEvent(s24,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,
		hiddenstate_size,&alpha,d_M_f,hiddenstate_size,d_d_ERRnTOt_ft,hiddenstate_size,&beta,
		d_temp2,hiddenstate_size),"cublas W gradient failed temp2\n");
	cudaEventRecord(W_grad_p2_done,s24);

	cublasSetStream(handle,s25);
	cudaStreamWaitEvent(s25,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,
		hiddenstate_size,&alpha,d_M_o,hiddenstate_size,d_d_ERRnTOt_ot,hiddenstate_size,&beta,
		d_temp3,hiddenstate_size),"cublas W gradient failed temp3\n");
	cudaEventRecord(W_grad_p3_done,s25);

	cublasSetStream(handle,s26);
	cudaStreamWaitEvent(s26,backprop_init,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,
		hiddenstate_size,&alpha,d_M_c,hiddenstate_size,d_d_ERRnTOt_tanhcpt,hiddenstate_size,&beta,
		d_temp4,hiddenstate_size),"cublas W gradient failed temp4\n");
	cudaEventRecord(W_grad_p4_done,s26);

	cudaStreamWaitEvent(s27,W_grad_p1_done,0);
	cudaStreamWaitEvent(s27,W_grad_p2_done,0);
	cudaStreamWaitEvent(s27,W_grad_p3_done,0);
	cudaStreamWaitEvent(s27,W_grad_p4_done,0);
	int threads_per_block = 128;
	int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
	dim3 kernel(minibatch_size,num_block,1);
	W_gradient_kernel<<<kernel,threads_per_block,0,s27>>>(d_W_grad,d_input_vocab_indices,d_temp1,d_temp2,d_temp3,d_temp4,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

	//DEBUGGING
	// get_matrix_cuBLAS(h_b_i_grad,d_b_i_grad,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_b_f_grad,d_b_f_grad,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_b_o_grad,d_b_o_grad,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_b_c_grad,d_b_c_grad,hiddenstate_size,minibatch_size);

	// print_matrix(h_b_i_grad,hiddenstate_size,1);
	// print_matrix(h_b_f_grad,hiddenstate_size,1);
	// print_matrix(h_b_o_grad,hiddenstate_size,1);
	// print_matrix(h_b_c_grad,hiddenstate_size,1);
	//get_matrix_cuBLAS(h_W_grad,d_W_grad,hiddenstate_size,input_vocab_size);
	//print_matrix(h_W_grad,hiddenstate_size,input_vocab_size);
	cudaDeviceSynchronize();

}

void forward_prop(int hiddenstate_size,int minibatch_size,cublasHandle_t handle,int input_vocab_size) {


	//OPERATION
	//USING STREAM 1
	//compute_temp_mat(model->W);
	int threads_per_block = 128;
	int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
	dim3 kernel(minibatch_size,num_block,1);
	sparse_lookup_kernel<<< kernel,threads_per_block,0,0>>>(d_sparse_lookup,d_W,d_input_vocab_indices,minibatch_size,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(sparse_forward_start,0);

	// //DEBUGGING
	// get_matrix_cuBLAS(h_sparse_lookup,d_sparse_lookup,hiddenstate_size,minibatch_size);
	// print_matrix(h_sparse_lookup,hiddenstate_size,minibatch_size);
	// get_matrix_cuBLAS(h_input_vocab_indices,d_input_vocab_indices,minibatch_size,1);
	// print_matrix(h_input_vocab_indices,minibatch_size,1);
	// get_matrix_cuBLAS(h_input_vocab_indices_01,d_input_vocab_indices_01,minibatch_size,1);
	// print_matrix(h_input_vocab_indices_01,minibatch_size,1);


	//OPERATION
	//USING STREAMS 1 and 2
	//i_t = ((model->M_i*temp_mat + model->W_hi*h_t_prev).colwise() + model->b_i).array().unaryExpr(sigmoid_functor());
	precision alpha =1;
	precision beta = 0;

	cublasSetStream(handle,s1);
	cudaStreamWaitEvent(s1,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_M_i,hiddenstate_size,
		d_sparse_lookup,hiddenstate_size,&beta,d_temp1,hiddenstate_size),"Forward prop i_t temp1 failed\n");
	cudaEventRecord(i_t_part1,s1);

	cublasSetStream(handle,s2);
	cudaStreamWaitEvent(s2,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_W_hi,hiddenstate_size,
		d_h_t_prev,hiddenstate_size,&beta,d_temp2,hiddenstate_size),"Forward prop i_t temp2 failed\n");

	cudaStreamWaitEvent(s2,i_t_part1,0);
	forward_sigmoid_kernel<<<kernel,threads_per_block,0,s2>>>(d_i_t,d_temp1,d_temp2,d_b_i,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(i_t_full,s2);


	// //DEBUGGING
	// get_matrix_cuBLAS(h_i_t,d_i_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_i_t,hiddenstate_size,minibatch_size);

	//OPERATION
	//f_t = ((model->M_f*temp_mat + model->W_hf*h_t_prev).colwise() + model->b_f).array().unaryExpr(sigmoid_functor());
	//USING STREAMS 3 and 4
	alpha =1;
	beta = 0;
	cublasSetStream(handle,s3);
	cudaStreamWaitEvent(s3,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_M_f,hiddenstate_size,
		d_sparse_lookup,hiddenstate_size,&beta,d_temp3,hiddenstate_size),"Forward prop f_t temp3 failed\n");
	cudaEventRecord(f_t_part1,s3);

	cublasSetStream(handle,s4);
	cudaStreamWaitEvent(s4,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_W_hf,hiddenstate_size,
		d_h_t_prev,hiddenstate_size,&beta,d_temp4,hiddenstate_size),"Forward prop f_t temp4 failed\n");

	cudaStreamWaitEvent(s4,f_t_part1,0);
	forward_sigmoid_kernel<<<kernel,threads_per_block,0,s4>>>(d_f_t,d_temp3,d_temp4,d_b_f,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(f_t_full,s4);

	// //DEBUGGING
	// get_matrix_cuBLAS(h_f_t,d_f_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_f_t,hiddenstate_size,minibatch_size);


	//OPERATION
	//USING STREAMS 5 and 6
	//c_prime_t_tanh = ((model->M_c*temp_mat + model->W_hc*h_t_prev).colwise() + model->b_c).array().unaryExpr(tanh_functor());
	alpha =1;
	beta = 0;
	cublasSetStream(handle,s5);
	cudaStreamWaitEvent(s5,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_M_c,hiddenstate_size,
		d_sparse_lookup,hiddenstate_size,&beta,d_temp5,hiddenstate_size),"Forward prop c_prime_t_tanh temp5 failed\n");
	cudaEventRecord(c_prime_t_tanh_part1,s5);

	cublasSetStream(handle,s6);
	cudaStreamWaitEvent(s6,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_W_hc,hiddenstate_size,
		d_h_t_prev,hiddenstate_size,&beta,d_temp6,hiddenstate_size),"Forward prop c_prime_t_tanh temp6 failed\n");

	cudaStreamWaitEvent(s6,c_prime_t_tanh_part1,0);
	forward_tanh_kernel<<<kernel,threads_per_block,0,s6>>>(d_c_prime_t_tanh,d_temp5,d_temp6,d_b_c,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(c_prime_t_tanh_full,s6);


	// //DEBUGGING
	// get_matrix_cuBLAS(h_c_prime_t_tanh,d_c_prime_t_tanh,hiddenstate_size,minibatch_size);
	// print_matrix(h_c_prime_t_tanh,hiddenstate_size,minibatch_size);


	//OPERATION
	//USING STREAMS 7 and 8
	//o_t = ((model->M_o*temp_mat + model->W_ho*h_t_prev).colwise() + model->b_o).unaryExpr(sigmoid_functor());
	alpha = 1;
	beta = 0;
	cublasSetStream(handle,s7);
	cudaStreamWaitEvent(s7,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_M_o,hiddenstate_size,
		d_sparse_lookup,hiddenstate_size,&beta,d_temp7,hiddenstate_size),"Forward prop o_t temp1 failed\n");
	cudaEventRecord(o_t_part1,s7);

	cublasSetStream(handle,s8);
	cudaStreamWaitEvent(s8,sparse_forward_start,0);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,hiddenstate_size,&alpha,d_W_ho,hiddenstate_size,
		d_h_t_prev,hiddenstate_size,&beta,d_temp8,hiddenstate_size),"Forward prop o_t temp2 failed\n");

	cudaStreamWaitEvent(s8,o_t_part1,0);
	forward_sigmoid_kernel<<<kernel,threads_per_block,0,s8>>>(d_o_t,d_temp7,d_temp8,d_b_o,hiddenstate_size);
	CUDA_GET_LAST_ERROR();
	cudaEventRecord(o_t_full,s8);


	//OPERATION
	//FOR NOW THE REST ARE USING THE DEFAULT STREAM
	//c_t = ((f_t.array())*(c_t_prev.array())).matrix() + (i_t.array()*(c_prime_t_tanh.array())).matrix();
	cudaStreamWaitEvent(0,i_t_full,0);
	cudaStreamWaitEvent(0,f_t_full,0);
	cudaStreamWaitEvent(0,c_prime_t_tanh_full,0);
	cudaStreamWaitEvent(0,o_t_full,0);
	forward_c_t_kernel<<<kernel,threads_per_block>>>(d_c_t,d_f_t, d_c_t_prev,d_i_t,d_c_prime_t_tanh,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

	// //DEBUGGING
	// get_matrix_cuBLAS(h_c_t,d_c_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_c_t,hiddenstate_size,minibatch_size);



	// //DEBUGGING
	// get_matrix_cuBLAS(h_o_t,d_o_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_o_t,hiddenstate_size,minibatch_size);


	//OPERATION
	//h_t = o_t.array()*(c_t.array().unaryExpr(tanh_functor()));
	forward_h_t_kernel<<<kernel,threads_per_block>>>(d_h_t,d_o_t,d_c_t,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

	// //DEBUGGING
	// get_matrix_cuBLAS(h_h_t,d_h_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_h_t,hiddenstate_size,minibatch_size);

	//OPERATION
	// for(int i=0; i< vocab_indices_input.rows(); i++) {
	// 	if(vocab_indices_input(i)==-1) {
	// 		h_t.col(i).setZero();
	// 		c_t.col(i).setZero();
	// 	}
	// }
	zero_c_t_and_h_t<<< kernel,threads_per_block>>>(d_h_t,d_c_t,d_input_vocab_indices_01,hiddenstate_size);
	CUDA_GET_LAST_ERROR();

	// get_matrix_cuBLAS(h_h_t,d_h_t,hiddenstate_size,minibatch_size);
	// print_matrix(h_h_t,hiddenstate_size,minibatch_size);
}

//EIGEN globals for convience
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> i_t;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> f_t;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> c_t;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> c_prime_t_tanh;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> o_t;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> h_t;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> h_t_prev;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> c_t_prev;
//Eigen::Matrix<int,Eigen::Dynamic,1> vocab_indices_input;
Eigen::Matrix<int,Eigen::Dynamic,1> vocab_indices_input;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_ht;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_ot;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_ct;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRt_ct;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_ft;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_tanhcpt;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_it;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_htM1;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOt_ctM1;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> Z_i;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> Z_f;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> Z_o;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> Z_c;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOtp1_ht;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRnTOtp1_ct;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> d_ERRt_ht;

Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_ho;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_hf;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_hi;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_hc;

Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_ho_grad;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_hf_grad;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_hi_grad;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_hc_grad;

Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_i_grad;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_f_grad;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_o_grad;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_c_grad;

Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> sparse_lookup;

Eigen::Matrix<precision, Eigen::Dynamic, 1> b_i_grad;
Eigen::Matrix<precision, Eigen::Dynamic, 1> b_f_grad;
Eigen::Matrix<precision, Eigen::Dynamic, 1> b_c_grad;
Eigen::Matrix<precision, Eigen::Dynamic, 1> b_o_grad;

Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_i;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_f;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_o;
Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> M_c;

Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> W_grad;

Eigen::Matrix<precision, Eigen::Dynamic, 1> b_i;
Eigen::Matrix<precision, Eigen::Dynamic, 1> b_f;
Eigen::Matrix<precision, Eigen::Dynamic, 1> b_c;
Eigen::Matrix<precision, Eigen::Dynamic, 1> b_o;

//functors for eigen code
struct tanh_sq_functor {
	template<typename dType>
  	dType operator() (dType x) const { return std::tanh(x)*std::tanh(x); }
};


struct tanh_functor {
	template<typename dType>
  	dType operator() (dType x) const { return std::tanh(x); }
};



void back_prop_eigen() {

	//print_eigen_matrix(d_ERRt_ht.transpose());
	//print_eigen_matrix(d_ERRnTOtp1_ht.transpose());
	//Now get the derivative of h_t with respect to this error and all after it (t-n)
	d_ERRnTOt_ht = d_ERRnTOtp1_ht + d_ERRt_ht;

	//Derivative of error at time t with respect to c_t
	//d_ERRt_ct = d_ERRnTOt_ht.array() * (o_t.array()*(1- (c_t).array().unaryExpr(tanh_sq_functor()))).matrix().transpose().array();

	//NON-TRANSPOSE
	d_ERRt_ct.transpose() = d_ERRnTOt_ht.transpose().array() * (o_t.array()*(1-(c_t).array().unaryExpr(tanh_sq_functor())));


	//print_eigen_matrix(d_ERRt_ct.transpose());
	//print_eigen_matrix(o_t);
	//print_eigen_matrix(c_t);

	d_ERRnTOt_ct = d_ERRnTOtp1_ct + d_ERRt_ct;

	//Check to see if we should zero out derivatives
	//Now do a check to see if h_t or c_t should be zeroed out
	for(int i=0; i< vocab_indices_input.rows(); i++) {
		if(vocab_indices_input(i)==-1) {
			d_ERRnTOt_ht.row(i).setZero();
			d_ERRnTOt_ct.row(i).setZero();
		}
	}

	//print_eigen_matrix(d_ERRnTOt_ht.transpose());
	//print_eigen_matrix(d_ERRnTOt_ct.transpose());

	//Derivative of error from time t to n with respect to o_t
	//d_ERRnTOt_ot = d_ERRnTOt_ht.array()*( (c_t.array().unaryExpr(tanh_functor())).matrix().transpose().array() );

	//NON-TRANSPOSE
	d_ERRnTOt_ot.transpose() = d_ERRnTOt_ht.transpose().array()*( c_t.array().unaryExpr(tanh_functor()) );

	//Derivative of Error from t to n with respect to f_t
	//d_ERRnTOt_ft = d_ERRnTOt_ct.array()*(c_t_prev.transpose().array());

	//NON-TRANSPOSE
	d_ERRnTOt_ft.transpose() = d_ERRnTOt_ct.transpose().array()*(c_t_prev.array());

	//This is the derivative of the error from time n to time t with respect to tanhc'_t
	//d_ERRnTOt_tanhcpt = d_ERRnTOt_ct.array()*(i_t.transpose().array());

	//NON-TRANSPOSE
	d_ERRnTOt_tanhcpt.transpose() = d_ERRnTOt_ct.transpose().array()*(i_t.array());

	//This is the derivative of the error from time n to time t with respect to i_t
	//d_ERRnTOt_it = d_ERRnTOt_ct.array()*(c_prime_t_tanh.transpose().array());

	//NON-TRANSPOSE
	d_ERRnTOt_it.transpose() = d_ERRnTOt_ct.transpose().array()*(c_prime_t_tanh.array());



	//This is the derivative of the error from time n to t with respect to h_(t-1)
	// d_ERRnTOt_htM1 = (model->W_ho.transpose()*( (d_ERRnTOt_ot.transpose().array() * o_t.array() * (1- o_t.array())).matrix() )).transpose() \
	// + (model->W_hf.transpose()*((d_ERRnTOt_ft.transpose().array() * f_t.array() *(1-f_t.array())).matrix())).transpose() \
	// + (model->W_hi.transpose()*((d_ERRnTOt_it.transpose().array()*i_t.array()*(1-i_t.array())).matrix())).transpose() \
	// + (model->W_hc.transpose()*((d_ERRnTOt_tanhcpt.transpose().array()*(1-c_prime_t_tanh.array().square())).matrix())).transpose();

	//NON-TRANSPOSE
	d_ERRnTOt_htM1.transpose() = (W_ho.transpose()*( (d_ERRnTOt_ot.transpose().array() * o_t.array() * (1- o_t.array())).matrix() )) \
	+ (W_hf.transpose()*((d_ERRnTOt_ft.transpose().array() * f_t.array() *(1-f_t.array())).matrix())) \
	+ (W_hi.transpose()*((d_ERRnTOt_it.transpose().array()*i_t.array()*(1-i_t.array())).matrix())) \
	+ (W_hc.transpose()*((d_ERRnTOt_tanhcpt.transpose().array()*(1-c_prime_t_tanh.array().square())).matrix()));

	//Derivative from error from time t to n with respect to ctM1
	//d_ERRnTOt_ctM1 = (d_ERRnTOt_ct.array()*f_t.transpose().array());

	//NON-TRANSPOSE
	d_ERRnTOt_ctM1.transpose() = (d_ERRnTOt_ct.transpose().array()*f_t.array());



	// std::cout << "--------------Eigen------------\n";
	// Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> temp1;
	// Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> temp2;
	// Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> temp3;
	// Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> temp4;
	// temp1 = (W_ho.transpose()*( (d_ERRnTOt_ot.transpose().array() * o_t.array() * (1- o_t.array())).matrix() ));
	// temp2 = (W_hf.transpose()*((d_ERRnTOt_ft.transpose().array() * f_t.array() *(1-f_t.array())).matrix()));
	// temp3 = (W_hi.transpose()*((d_ERRnTOt_it.transpose().array()*i_t.array()*(1-i_t.array())).matrix()));
	// temp4 = (W_hc.transpose()*((d_ERRnTOt_tanhcpt.transpose().array()*(1-c_prime_t_tanh.array().square())).matrix()));
	// print_eigen_matrix(temp1);
	// print_eigen_matrix(temp2);
	// print_eigen_matrix(temp3);
	// print_eigen_matrix(temp4);
	// print_eigen_matrix(d_ERRnTOt_htM1.transpose());
	// print_eigen_matrix(d_ERRnTOt_ctM1.transpose());
	//Update the gradients
	compute_gradients_eigen();
}

void compute_temp_mat() {

	for(int i=0; i<vocab_indices_input.rows(); i++) {
		if(vocab_indices_input(i)!=-1) {
			sparse_lookup.col(i) = W.col(vocab_indices_input(i));
		}
		else {
			//Just assign it to a vector, since it does not matter
			sparse_lookup.col(i) = W.col(0);
		}
	}
}

void compute_W_gradient_eigen() {
	Z_i = d_ERRnTOt_it.array()*(i_t.array() * (1-i_t.array())).matrix().transpose().array();
	Z_f = d_ERRnTOt_ft.array()*(f_t.array() * (1-f_t.array())).matrix().transpose().array();
	Z_o = d_ERRnTOt_ot.array()*(o_t.array() * (1-o_t.array())).matrix().transpose().array();
	Z_c = d_ERRnTOt_tanhcpt.array()*(1-c_prime_t_tanh.array().square()).matrix().transpose().array();

	for(int i=0; i<vocab_indices_input.rows(); i++) {
		if(vocab_indices_input(i)!=-1) {
			for(int j=0; j<W_grad.rows(); j++) {
				double sumtemp = Z_i.row(i) * M_i.col(j);
				sumtemp += Z_f.row(i) * M_f.col(j);
				sumtemp += Z_o.row(i) * M_o.col(j);
				sumtemp += Z_c.row(i) * M_c.col(j);
				W_grad(j,vocab_indices_input(i)) += sumtemp;
			}
		}
	}

	//std::cout << "--------------Eigen------------\n";
	//print_eigen_matrix(W_grad);
}


void compute_gradients_eigen() {
	//Hiden state matrices
	W_hi_grad.noalias() += (h_t_prev*(d_ERRnTOt_it.array() * i_t.transpose().array()*(1-i_t.transpose().array())).matrix()).transpose();
	W_hf_grad.noalias() += (h_t_prev*(d_ERRnTOt_ft.array()*f_t.transpose().array()*(1-f_t.transpose().array())).matrix()).transpose();
	W_hc_grad.noalias() += (h_t_prev*(d_ERRnTOt_tanhcpt.array()*(i_t.transpose().array())*(1-c_prime_t_tanh.transpose().array().square())).matrix()).transpose();
	W_ho_grad.noalias() += (h_t_prev*(d_ERRnTOt_ot.array()*o_t.transpose().array()*(1-o_t.transpose().array())).matrix()).transpose();

	// std::cout << "------------Eigen------------\n";
	// print_eigen_matrix(W_hi_grad);
	// print_eigen_matrix(W_hf_grad);
	// print_eigen_matrix(W_hc_grad);
	// print_eigen_matrix(W_ho_grad);

	compute_temp_mat();
	M_i_grad.noalias() += (d_ERRnTOt_it.transpose().array() * i_t.array() * (1-i_t.array())).matrix() * sparse_lookup.transpose();
	M_f_grad.noalias() += (d_ERRnTOt_ft.transpose().array() * f_t.array() * (1-f_t.array())).matrix() * sparse_lookup.transpose();
	M_o_grad.noalias() += (d_ERRnTOt_ot.transpose().array() * o_t.array() * (1-o_t.array())).matrix() * sparse_lookup.transpose();
	M_c_grad.noalias() += (d_ERRnTOt_tanhcpt.transpose().array() * (1-c_prime_t_tanh.array().square())).matrix() * sparse_lookup.transpose();

	// std::cout << "------------Eigen------------\n";
	// print_eigen_matrix(M_i_grad);
	// print_eigen_matrix(M_f_grad);
	// print_eigen_matrix(M_o_grad);
	// print_eigen_matrix(M_c_grad);

	// //Update the bias gradients
	b_i_grad.noalias() += ((d_ERRnTOt_it.array() * (i_t.array() * (1-i_t.array())).matrix().transpose().array()).colwise().sum()).matrix().transpose();
	b_f_grad.noalias() += ((d_ERRnTOt_ft.array() * (f_t.array() * (1-f_t.array())).matrix().transpose().array()).colwise().sum()).matrix().transpose();
	b_o_grad.noalias() += ((d_ERRnTOt_ot.array() * (o_t.array() * (1-o_t.array())).matrix().transpose().array()).colwise().sum()).matrix().transpose();
	b_c_grad.noalias() += (d_ERRnTOt_tanhcpt.array() * (1-c_prime_t_tanh.array().square()).matrix().transpose().array()).colwise().sum().matrix().transpose();

	// std::cout << "------------Eigen------------\n";
	// print_eigen_matrix(b_i_grad);
	// print_eigen_matrix(b_f_grad);
	// print_eigen_matrix(b_o_grad);
	// print_eigen_matrix(b_c_grad);

	compute_W_gradient_eigen();
}


void forward_prop_eigen( ) {

	compute_temp_mat();

	// std::cout << "--------------Eigen---------------\n";
	// print_eigen_matrix(sparse_lookup);
	// print_eigen_matrix(vocab_indices_input);

	//input gate
	i_t = ((M_i*sparse_lookup + W_hi*h_t_prev).colwise() + b_i).array().unaryExpr(sigmoid_functor());

	//print_eigen_matrix(i_t);

	//Forget gate
	f_t = ((M_f*sparse_lookup + W_hf*h_t_prev).colwise() + b_f).array().unaryExpr(sigmoid_functor());

	//print_eigen_matrix(f_t);

	//Cell gate
	c_prime_t_tanh = ((M_c*sparse_lookup + W_hc*h_t_prev).colwise() + b_c).array().unaryExpr(tanh_functor());

	//print_eigen_matrix(c_prime_t_tanh);

	c_t = ((f_t.array())*(c_t_prev.array())).matrix() + (i_t.array()*(c_prime_t_tanh.array())).matrix();

	//print_eigen_matrix(c_t);

	//Output gate
	o_t = ((M_o*sparse_lookup + W_ho*h_t_prev).colwise() + b_o).unaryExpr(sigmoid_functor());

	//print_eigen_matrix(o_t);

	//Output hidden state
	h_t = o_t.array()*(c_t.array().unaryExpr(tanh_functor()));

	//print_eigen_matrix(h_t);

	//Now do a check to see if h_t or c_t should be zeroed out
	for(int i=0; i< vocab_indices_input.rows(); i++) {
		if(vocab_indices_input(i)==-1) {
			h_t.col(i).setZero();
			c_t.col(i).setZero();
		}
	}

	//print_eigen_matrix(h_t);

}


struct scale_functor {
	const int minibatch_size;

	scale_functor(int _minibatch_size) : minibatch_size(_minibatch_size) {}

	__host__ __device__ void operator()(float &x) {
		x = (1.0f/minibatch_size)*x;
	}
	__host__ __device__ void operator()(double &x) {
		x = (1.0/minibatch_size)*x;
	}
};




struct square {
    __host__ __device__
    float operator()(const float& x) const { 
        return x * x;
    }

    __host__ __device__
    double operator()(const double& x) const { 
        return x * x;
    }
};

template<typename dType>
struct re_scale_norm_functor {
	const dType norm_threshold;
	const dType norm;

	re_scale_norm_functor(dType _norm_threshold,dType _norm) : norm_threshold(_norm_threshold),norm(_norm) {}

	__host__ __device__ void operator()(dType &x) {
		x = (norm_threshold/norm)*x;
	}
};

//scale the gradient by 1.0/minibatch_size
template<typename dType>
void scale_gradient(thrust::device_vector<dType> &thrust_d_gradient,int minibatch_size) {
	scale_functor unary_op(minibatch_size);
	thrust::for_each(thrust_d_gradient.begin(),thrust_d_gradient.end(),unary_op);
}

//clip the norm if it is greater than the threshold
template<typename dType>
void norm_clip(thrust::device_vector<dType> &thrust_d_gradient,dType norm_threshold) {
	dType norm = std::sqrt( thrust::transform_reduce(thrust_d_gradient.begin(), 
		thrust_d_gradient.end(), square(), (dType)0, thrust::plus<dType>()) );
	if(norm > norm_threshold) {
		re_scale_norm_functor<dType> unary_op(norm_threshold,norm);
		thrust::for_each(thrust_d_gradient.begin(),thrust_d_gradient.end(),unary_op);
	}
}

//For non W matrix
template<typename dType>
void update_parameters(dType *d_gradient,dType *d_parameter,dType learning_rate,cublasHandle_t handle,int rows,int cols) {
	dType alpha = learning_rate;
	dType beta = 1;
	CUBLAS_ERROR_WRAPPER(cublas_geam_wrapper(handle, CUBLAS_OP_N, CUBLAS_OP_N,rows, cols, &alpha, d_gradient, rows, &beta, 
		d_parameter, rows, d_parameter, rows),"CUBLAS addition update parameter failed\n"); 
}

//Set gradients to zero
template<typename dType>
void zero_gradient(thrust::device_vector<dType> &thrust_d_gradient) {
	thrust::fill(thrust_d_gradient.begin(), thrust_d_gradient.end(), (dType)0);
}

template<typename Derived>
void clipNorm(const Eigen::MatrixBase<Derived> &gradient_const,double norm,double norm_clip) {
	UNCONST(Derived, gradient_const, gradient);
	gradient = (norm_clip/norm)*gradient;
}

//get the norm for matrix
template<typename Derived>
void computeNorm(const Eigen::MatrixBase<Derived> &gradient,double norm_clip) {
	double norm = std::sqrt(gradient.array().square().sum());
	if(norm>norm_clip) {
		clipNorm(gradient,norm,norm_clip);
	}
}


//Kernel for updating the W gradient
template<typename dType>
__global__ 
void update_W_gradient(dType *d_W, dType *d_W_gradient,int *d_vocab_indicies_m1,dType learning_rate,int hiddenstate_size,int total_length) {
	for(int j = blockIdx.y; j<total_length; j+=gridDim.y) {
		if(d_vocab_indicies_m1[j]!=-1) {
			int idx = IDX2C(threadIdx.x + blockIdx.x*blockDim.x,d_vocab_indicies_m1[j],hiddenstate_size);
			d_W[idx] += learning_rate* d_W_gradient[idx];
		}
	}
}


//Kernel for zeroing the W gradient
//length is stopping point for threads
//vocab indicies should contain special form, so no -1's
template<typename dType>
__global__ 
void zero_W_gradient(dType *d_W_gradient,int *d_vocab_indicies_m1,int hiddenstate_size,int total_length) {
	for(int j=blockIdx.y; j<total_length; j+=gridDim.y) {
		if(d_vocab_indicies_m1[j]!=-1) {
			d_W_gradient[IDX2C(threadIdx.x + blockIdx.x*blockDim.x,d_vocab_indicies_m1[j],hiddenstate_size)] = 0;
		}
	}
}

//Kernel for getting scaling the gradient of W by 1/(minibatch size)
template<typename dType>
__global__
void scale_W_gradient(dType *d_W_gradient,int *d_vocab_indicies_m1,int hiddenstate_size,dType scale,int total_length) {
	for(int j=blockIdx.y; j<total_length; j+=gridDim.y) {
		if(d_vocab_indicies_m1[j]!=-1) {
			int idx = IDX2C(threadIdx.x + blockIdx.x*blockDim.x,d_vocab_indicies_m1[j],hiddenstate_size);
			d_W_gradient[idx] = scale * d_W_gradient[idx];
		}
	}
}




template<typename dType>
void norm_clip_W(thrust::device_vector<dType> &thrust_d_gradient,dType * d_grad,int *d_vocab_indicies_m1 ,dType norm_threshold,int total_length,int hiddenstate_size) {
	dType norm = std::sqrt( thrust::transform_reduce(thrust_d_gradient.begin(), 
		thrust_d_gradient.end(), square(), (dType)0, thrust::plus<dType>()) );
	if(norm > norm_threshold) {
		int threads_per_block = 256;
		int num_block = (hiddenstate_size + threads_per_block-1)/threads_per_block;
		dim3 kernel(num_block,256,1);
		dType scalar = (norm_threshold/norm);
		scale_W_gradient<<<kernel,threads_per_block>>>(d_grad,d_vocab_indicies_m1,hiddenstate_size,scalar,total_length);
	}
}


template<typename dType>
void norm_clip_W_v2(dType * d_grad,int *d_vocab_indicies_m1,int *h_vocab_indicies_m1,
	dType norm_threshold,int total_length,int hiddenstate_size) 
{
	//Do first level sum
	dType sum = 0;
	thrust::device_ptr<precision> d_ptr = thrust::device_pointer_cast(d_grad);
	for(int i=0; i<total_length; i++) {
		sum += thrust::transform_reduce(d_ptr + hiddenstate_size*h_vocab_indicies_m1[i], 
			d_ptr + hiddenstate_size*h_vocab_indicies_m1[i] + hiddenstate_size, square(), (dType)0, thrust::plus<dType>());
	}
	dType norm = std::sqrt(sum);
	if(norm > norm_threshold) {
		int threads_per_block = 256;
		int num_block = (hiddenstate_size + threads_per_block-1)/threads_per_block;
		dim3 kernel(num_block,256,1);
		scale_W_gradient<<<kernel,threads_per_block>>>(d_grad,d_vocab_indicies_m1,hiddenstate_size,(norm_threshold/norm),total_length);
	}
}



int main() {

	//constants for the sizes of the matrices
	const int hiddenstate_size = 1000;
	const int input_vocab_size = 160000; //input vocab size here really means input vocab size
	const int minibatch_size = 128;
	const int num_trials = 1;
	const precision norm_clip_val = 2;
	const precision learning_rate = (precision)0.4;

	//Make the cublas handle
	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");

	cudaStreamCreate(&s1);
	cudaStreamCreate(&s2);
	cudaStreamCreate(&s3);
	cudaStreamCreate(&s4);
	cudaStreamCreate(&s5);
	cudaStreamCreate(&s6);
	cudaStreamCreate(&s7);
	cudaStreamCreate(&s8);
	cudaStreamCreate(&s9);
	cudaStreamCreate(&s10);
	cudaStreamCreate(&s11);
	cudaStreamCreate(&s12);
	cudaStreamCreate(&s13);
	cudaStreamCreate(&s14);
	cudaStreamCreate(&s15);
	cudaStreamCreate(&s16);
	cudaStreamCreate(&s17);
	cudaStreamCreate(&s18);
	cudaStreamCreate(&s19);
	cudaStreamCreate(&s20);
	cudaStreamCreate(&s21);
	cudaStreamCreate(&s22);
	cudaStreamCreate(&s23);
	cudaStreamCreate(&s24);
	cudaStreamCreate(&s25);
	cudaStreamCreate(&s26);
	cudaStreamCreate(&s27);

	cudaEventCreate(&sparse_forward_start);
	cudaEventCreate(&i_t_part1);
	cudaEventCreate(&i_t_full);
	cudaEventCreate(&f_t_part1);
	cudaEventCreate(&f_t_full);
	cudaEventCreate(&c_prime_t_tanh_part1);
	cudaEventCreate(&c_prime_t_tanh_full);
	cudaEventCreate(&o_t_part1);
	cudaEventCreate(&o_t_full);


	cudaEventCreate(&backprop_init);
	cudaEventCreate(&err_ot_done);
	cudaEventCreate(&err_ft_done);
	cudaEventCreate(&err_tanhcpt_done);
	cudaEventCreate(&err_it_done);
	cudaEventCreate(&htm1_p1_done);
	cudaEventCreate(&htm1_p2_done);
	cudaEventCreate(&htm1_p3_done);
	cudaEventCreate(&htm1_p4_done);

	cudaEventCreate(&W_grad_p1_done);
	cudaEventCreate(&W_grad_p2_done);
	cudaEventCreate(&W_grad_p3_done);
	cudaEventCreate(&W_grad_p4_done);



	//initialize host and device pointers
	full_matrix_setup(&h_d_ERRnTOtp1_ht,&d_d_ERRnTOtp1_ht,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRnTOtp1_ct,&d_d_ERRnTOtp1_ct,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRt_ht,&d_d_ERRt_ht,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_ht,&d_d_ERRnTOt_ht,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_o_t,&d_o_t,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_c_t,&d_c_t,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRt_ct,&d_d_ERRt_ct,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_ct,&d_d_ERRnTOt_ct,hiddenstate_size,minibatch_size);
	full_vector_setup_vocab_01(&h_input_vocab_indices_01,&d_input_vocab_indices_01,minibatch_size);
	full_vector_setup_vocab(&h_input_vocab_indices,&d_input_vocab_indices,minibatch_size,input_vocab_size);
	full_matrix_setup(&h_d_ERRnTOt_ot,&d_d_ERRnTOt_ot,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_f_t,&d_f_t,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_c_t_prev,&d_c_t_prev,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_ft,&d_d_ERRnTOt_ft,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_c_prime_t_tanh,&d_c_prime_t_tanh,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_i_t,&d_i_t,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_tanhcpt,&d_d_ERRnTOt_tanhcpt,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_it,&d_d_ERRnTOt_it,hiddenstate_size,minibatch_size);

	full_matrix_setup(&h_temp1,&d_temp1,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_temp2,&d_temp2,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_temp3,&d_temp3,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_temp4,&d_temp4,hiddenstate_size,minibatch_size);

	full_matrix_setup(&h_W_ho,&d_W_ho,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_W_hf,&d_W_hf,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_W_hi,&d_W_hi,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_W_hc,&d_W_hc,hiddenstate_size,hiddenstate_size);

	full_matrix_setup(&h_W_hi_grad,&d_W_hi_grad,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_W_hf_grad,&d_W_hf_grad,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_W_hc_grad,&d_W_hc_grad,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_W_ho_grad,&d_W_ho_grad,hiddenstate_size,hiddenstate_size);

	full_matrix_setup(&h_d_ERRnTOt_htM1,&d_d_ERRnTOt_htM1,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_ctM1,&d_d_ERRnTOt_ctM1,hiddenstate_size,minibatch_size);

	full_matrix_setup(&h_h_t_prev,&d_h_t_prev,hiddenstate_size,hiddenstate_size);

	full_matrix_setup(&h_M_i_grad,&d_M_i_grad,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_M_f_grad,&d_M_f_grad,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_M_o_grad,&d_M_o_grad,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_M_c_grad,&d_M_c_grad,hiddenstate_size,hiddenstate_size);

	full_matrix_setup(&h_W,&d_W,hiddenstate_size,input_vocab_size);
	full_matrix_setup(&h_sparse_lookup,&d_sparse_lookup,hiddenstate_size,minibatch_size);

	full_matrix_setup(&h_b_i_grad,&d_b_i_grad,hiddenstate_size,1);
	full_matrix_setup(&h_b_f_grad,&d_b_f_grad,hiddenstate_size,1);
	full_matrix_setup(&h_b_c_grad,&d_b_c_grad,hiddenstate_size,1);
	full_matrix_setup(&h_b_o_grad,&d_b_o_grad,hiddenstate_size,1);

	full_vector_setup_ones(&h_ones_minibatch,&d_ones_minibatch,minibatch_size);

	full_matrix_setup(&h_M_i,&d_M_i,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_M_f,&d_M_f,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_M_o,&d_M_o,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_M_c,&d_M_c,hiddenstate_size,hiddenstate_size);

	full_matrix_setup(&h_W_grad,&d_W_grad,hiddenstate_size,input_vocab_size);

	full_matrix_setup(&h_b_i,&d_b_i,hiddenstate_size,1);
	full_matrix_setup(&h_b_f,&d_b_f,hiddenstate_size,1);
	full_matrix_setup(&h_b_c,&d_b_c,hiddenstate_size,1);
	full_matrix_setup(&h_b_o,&d_b_o,hiddenstate_size,1);


	full_matrix_setup(&h_temp5,&d_temp5,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_temp6,&d_temp6,hiddenstate_size,minibatch_size);


	full_matrix_setup(&h_h_t,&d_h_t,hiddenstate_size,minibatch_size);

	full_matrix_setup(&h_temp7,&d_temp7,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_temp8,&d_temp8,hiddenstate_size,minibatch_size);



	//resize eigen matrices
	i_t.resize(hiddenstate_size,minibatch_size);
	f_t.resize(hiddenstate_size,minibatch_size);
	c_t.resize(hiddenstate_size,minibatch_size);
	c_prime_t_tanh.resize(hiddenstate_size,minibatch_size);
	o_t.resize(hiddenstate_size,minibatch_size);
	h_t.resize(hiddenstate_size,minibatch_size);
	h_t_prev.resize(hiddenstate_size,minibatch_size);
	c_t_prev.resize(hiddenstate_size,minibatch_size); 
	vocab_indices_input.resize(minibatch_size,1);
	d_ERRnTOt_ht.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOt_ot.resize(minibatch_size,hiddenstate_size);
	d_ERRt_ct.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOt_ft.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOt_tanhcpt.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOt_it.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOt_htM1.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOt_ctM1.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOt_ct.resize(minibatch_size,hiddenstate_size);
	Z_i.resize(minibatch_size,hiddenstate_size);
	Z_f.resize(minibatch_size,hiddenstate_size);
	Z_c.resize(minibatch_size,hiddenstate_size);
	Z_o.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOtp1_ht.resize(minibatch_size,hiddenstate_size);
	d_ERRnTOtp1_ct.resize(minibatch_size,hiddenstate_size);
	d_ERRt_ht.resize(minibatch_size,hiddenstate_size);

	W_ho.resize(hiddenstate_size,hiddenstate_size);
	W_hf.resize(hiddenstate_size,hiddenstate_size);
	W_hi.resize(hiddenstate_size,hiddenstate_size);
	W_hc.resize(hiddenstate_size,hiddenstate_size);

	W_ho_grad.resize(hiddenstate_size,hiddenstate_size);
	W_hf_grad.resize(hiddenstate_size,hiddenstate_size);
	W_hi_grad.resize(hiddenstate_size,hiddenstate_size);
	W_hc_grad.resize(hiddenstate_size,hiddenstate_size);

	M_i_grad.resize(hiddenstate_size,hiddenstate_size);
	M_f_grad.resize(hiddenstate_size,hiddenstate_size);
	M_o_grad.resize(hiddenstate_size,hiddenstate_size);
	M_c_grad.resize(hiddenstate_size,hiddenstate_size);

	W.resize(hiddenstate_size,input_vocab_size);
	sparse_lookup.resize(hiddenstate_size,minibatch_size);

	b_i_grad.resize(hiddenstate_size,1);
	b_f_grad.resize(hiddenstate_size,1);
	b_c_grad.resize(hiddenstate_size,1);
	b_o_grad.resize(hiddenstate_size,1);

	M_i.resize(hiddenstate_size,hiddenstate_size);
	M_f.resize(hiddenstate_size,hiddenstate_size);
	M_o.resize(hiddenstate_size,hiddenstate_size);
	M_c.resize(hiddenstate_size,hiddenstate_size);

	W_grad.resize(hiddenstate_size,input_vocab_size);

	b_i.resize(hiddenstate_size,1);
	b_f.resize(hiddenstate_size,1);
	b_c.resize(hiddenstate_size,1);
	b_o.resize(hiddenstate_size,1);

	//copy matrices to eigen
	copy_to_eigen(d_ERRnTOtp1_ht.transpose(),h_d_ERRnTOtp1_ht);
	copy_to_eigen(d_ERRnTOtp1_ct.transpose(),h_d_ERRnTOtp1_ct);
	copy_to_eigen(d_ERRt_ht.transpose(),h_d_ERRt_ht);
	copy_to_eigen(d_ERRnTOt_ht.transpose(),h_d_ERRnTOt_ht);
	copy_to_eigen(o_t,h_o_t);
	copy_to_eigen(c_t,h_c_t);
	copy_to_eigen(d_ERRt_ct.transpose(),h_d_ERRt_ct);
	copy_to_eigen(d_ERRnTOt_ct.transpose(),h_d_ERRnTOt_ct);
	copy_to_eigen(d_ERRnTOt_ot.transpose(),h_d_ERRnTOt_ot);
	copy_to_eigen(f_t,h_f_t);
	copy_to_eigen(c_t_prev,h_c_t_prev);
	copy_to_eigen(d_ERRnTOt_ft.transpose(),h_d_ERRnTOt_ft);
	copy_to_eigen(c_prime_t_tanh,h_c_prime_t_tanh);
	copy_to_eigen(i_t,h_i_t);
	copy_to_eigen(d_ERRnTOt_tanhcpt.transpose(),h_d_ERRnTOt_tanhcpt);
	copy_to_eigen(d_ERRnTOt_it.transpose(),h_d_ERRnTOt_it);

	copy_to_eigen(W_ho,h_W_ho);
	copy_to_eigen(W_hf,h_W_hf);
	copy_to_eigen(W_hi,h_W_hi);
	copy_to_eigen(W_hc,h_W_hc);

	copy_to_eigen(W_ho_grad,h_W_ho_grad);
	copy_to_eigen(W_hf_grad,h_W_hf_grad);
	copy_to_eigen(W_hi_grad,h_W_hi_grad);
	copy_to_eigen(W_hc_grad,h_W_hc_grad);

	copy_to_eigen(d_ERRnTOt_htM1.transpose(),h_d_ERRnTOt_htM1);
	copy_to_eigen(d_ERRnTOt_ctM1.transpose(),h_d_ERRnTOt_ctM1);

	copy_to_eigen(h_t_prev,h_h_t_prev);

	copy_to_eigen(M_i_grad,h_M_i_grad);
	copy_to_eigen(M_f_grad,h_M_f_grad);
	copy_to_eigen(M_o_grad,h_M_o_grad);
	copy_to_eigen(M_c_grad,h_M_c_grad);

	copy_to_eigen(W,h_W);
	copy_to_eigen(sparse_lookup,h_sparse_lookup);

	copy_to_eigen(b_i_grad,h_b_i_grad);
	copy_to_eigen(b_f_grad,h_b_f_grad);
	copy_to_eigen(b_c_grad,h_b_c_grad);
	copy_to_eigen(b_o_grad,h_b_o_grad);

	copy_to_eigen(M_i,h_M_i);
	copy_to_eigen(M_f,h_M_f);
	copy_to_eigen(M_o,h_M_o);
	copy_to_eigen(M_c,h_M_c);

	copy_to_eigen(W_grad,h_W_grad);

	copy_to_eigen(b_i,h_b_i);
	copy_to_eigen(b_f,h_b_f);
	copy_to_eigen(b_c,h_b_c);
	copy_to_eigen(b_o,h_b_o);

	copy_to_eigen(h_t,h_h_t);

	//special eigen copying for vocab indices
	for(int i=0; i<minibatch_size; i++) {
		if(h_input_vocab_indices_01[i]==1) {
			vocab_indices_input[i] = h_input_vocab_indices[i];
		}
		else {
			vocab_indices_input[i] = -1;
		}
	}


	//run eigen backprop
	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	std::chrono::duration<double> elapsed_seconds;
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		back_prop_eigen();
	}
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of Eigen backprop: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

    //run CUDA backprop
    start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		cudaProfilerStart();
		back_prop(hiddenstate_size,minibatch_size,handle,input_vocab_size);
		cudaProfilerStop();
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of CUDA backprop: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;



    get_matrix_cuBLAS(h_W_grad,d_W_grad,hiddenstate_size,input_vocab_size);

	if(eigen_check_thres(W_grad,h_W_grad,(precision)0.001)) {
		std::cout << "EIGEN CHECK PASSED for backprop\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED for backprop\n";
	}

	/////////////////////////////////////////forward prop///////////////////////////////////////
	cudaDeviceSynchronize();

	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		//cudaProfilerStart();
		forward_prop(hiddenstate_size,minibatch_size,handle,input_vocab_size);
		//cudaProfilerStop();
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of CUDA forward prop: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		forward_prop_eigen();
	}
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of Eigen forward prop: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

    get_matrix_cuBLAS(h_h_t,d_h_t,hiddenstate_size,minibatch_size);

    if(eigen_check_thres(h_t,h_h_t,(precision)0.001)) {
		std::cout << "EIGEN CHECK PASSED for forward prop\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED for forward prop\n";
	}

	cudaDeviceSynchronize();


	//--------------------------------------------------Gradient clipping and parameter updates-------------------------------------------
	// int rows = 3;
	// int cols = 4;
	// thrust::device_vector<precision> thrust_d_grad(rows * cols,30000);
	// thrust::host_vector<precision> thrust_h_param(rows * cols,20);
	// thrust::device_vector<precision> thrust_d_param(rows * cols,20);
	// precision *d_param = thrust::raw_pointer_cast(&thrust_d_param[0]);
	// precision *d_grad = thrust::raw_pointer_cast(&thrust_d_grad[0]);

	// //eigen
	// Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> test_grad(rows,cols);
	// Eigen::Matrix<precision, Eigen::Dynamic, Eigen::Dynamic> test_param(rows,cols);
	// test_param.setZero();
	// test_grad.setZero();
	// test_grad.array() = 300 + test_grad.array();

	// std::cout << "\n\n----------------------Eigen--------------------\n";
	// test_grad = (1.0/minibatch_size)*test_grad;
	// std::cout << test_grad << "\n\n";
	// computeNorm(test_grad,norm_clip_val);
	// std::cout << test_grad << "\n\n";
	// test_param+= (learning_rate) * test_grad;
	// std::cout << test_param << "\n\n";

	// std::cout << "\n-----------------THRUST-----------------------\n";
	// //thrust
	// zero_gradient(thrust_d_param);
	// scale_gradient(thrust_d_grad,minibatch_size);
	// for(int i=0; i<rows; i++) {
	// 	for(int j=0; j<cols; j++) {
	// 		std::cout << thrust_d_grad[i +j*rows] <<" ";
	// 	}
	// 	std::cout << "\n";
	// }
	// std::cout << "\n";
	// norm_clip(thrust_d_grad,norm_clip_val);
	// for(int i=0; i<rows; i++) {
	// 	for(int j=0; j<cols; j++) {
	// 		std::cout << thrust_d_grad[i +j*rows] <<" ";
	// 	}
	// 	std::cout << "\n";
	// }
	// update_parameters(d_grad,d_param,learning_rate,handle,rows,cols);
	// for(int i=0; i<rows; i++) {
	// 	for(int j=0; j<cols; j++) {
	// 		std::cout << thrust_d_param[i +j*rows] <<" ";
	// 	}
	// 	std::cout << "\n";
	// }
	// thrust_h_param = thrust_d_param;

	// if(eigen_check_thres_thrust(test_param,thrust_h_param,(precision)0.001)) {
	// 	std::cout << "EIGEN CHECK PASSED for gradient clipping\n";
	// }
	// else {
	// 	std::cout << "EIGEN CHECK FAILED for gradient clipping\n";
	// }


	//////////////////////////////////////////////W Gradient experiments//////////////////////////////
	// const int trial_vocab_size = 260000;
	// const int rows_W = hiddenstate_size;
	// const int cols_W = trial_vocab_size;
	// const int length_of_seq = 200;

	// bool *bitmap = new bool[trial_vocab_size];
	// int *vc_indicies = new int[minibatch_size*length_of_seq];
	// int *vc_indicies_new = new int[minibatch_size*length_of_seq];
	// int *d_vocab_indices;
	// allocate_Matrix_GPU(&d_vocab_indices,minibatch_size,length_of_seq);

	// boost::uniform_real<> distribution(0,1);
	// for(int i=0; i<minibatch_size*length_of_seq; i++) {
	// 	if((int)(rand()%4) !=1) {
	// 		vc_indicies[i] = (int)(trial_vocab_size*distribution(gen));
	// 	}
	// 	else {
	// 		vc_indicies[i] = -1;
	// 	}
	// }
	
	// //start timing
	// start_total = std::chrono::system_clock::now();
	// for(int i=0; i<trial_vocab_size; i++) {
	// 	bitmap[i] = false;
	// }
	// for(int i=0; i<minibatch_size*length_of_seq; i++) {

	// 	if(vc_indicies[i]==-1) {
	// 		vc_indicies_new[i] = -1;
	// 	}
	// 	else if(bitmap[vc_indicies[i]]==false) {
	// 		bitmap[vc_indicies[i]]=true;
	// 		vc_indicies_new[i] = vc_indicies[i];
	// 	}
	// 	else  {
	// 		vc_indicies_new[i] = -1;
	// 	}
	// }

	// //Now go and put all -1's at far right and number in far left
	// int len;
	// int left_index = 0;
	// int right_index = minibatch_size*length_of_seq-1;
	// while(left_index < right_index) {
	// 	if(vc_indicies_new[left_index]==-1) {
	// 		if(vc_indicies_new[right_index]!=-1) {
	// 			int temp_swap = vc_indicies_new[left_index];
	// 			vc_indicies_new[left_index] = vc_indicies_new[right_index];
	// 			vc_indicies_new[right_index] = temp_swap;
	// 			left_index++;
	// 			right_index--;
	// 			continue;
	// 		}
	// 		else {
	// 			right_index--;
	// 			continue;
	// 		}
	// 	}
	// 	left_index++;
	// }
	// len = left_index;

	// //Now copy this to the GPU
	// CUDA_ERROR_WRAPPER(cudaMemcpy(d_vocab_indices, vc_indicies_new, len*sizeof(int), cudaMemcpyHostToDevice),"allocation failed at vocab\n");
	// CUDA_ERROR_WRAPPER(cudaMemcpy(d_vocab_indices, vc_indicies_new, len*sizeof(int), cudaMemcpyHostToDevice),"allocation failed at vocab\n");
	// //set_matrix_cuBLAS(vc_indicies_new,d_vocab_indices,minibatch_size,length_of_seq);
	// //set_matrix_cuBLAS(vc_indicies_new,d_vocab_indices,minibatch_size,length_of_seq);
	// //end timing
	// end_total = std::chrono::system_clock::now();
	// elapsed_seconds = end_total-start_total;
 //    std::cout << "\n\n\n";
 //    std::cout << "Runtime of prepping indicies for GPU: " << (elapsed_seconds.count()) << " seconds" << std::endl;


 //    //Slow W gradient
 //    const int rows = hiddenstate_size;
 //    const int cols = trial_vocab_size;
 //    thrust::host_vector<precision> thrust_h_grad(rows * cols,0);
	// thrust::host_vector<precision> thrust_h_param(rows * cols,0);
	// thrust::host_vector<precision> thrust_h_param2(rows * cols,0);
 //   	thrust::device_vector<precision> thrust_d_grad(rows * cols,0);
	// thrust::device_vector<precision> thrust_d_param(rows * cols,0);
	// precision *d_param = thrust::raw_pointer_cast(&thrust_d_param[0]);
	// precision *d_grad = thrust::raw_pointer_cast(&thrust_d_grad[0]);

	// //Prep the W matrix
	// boost::uniform_real<> distribution2(-10,10);
	// for(int i=0; i<minibatch_size*length_of_seq; i++) {
	// 	if(vc_indicies_new[i]!=-1) {
	// 		for(int j=0; j<rows; j++) {
	// 			thrust_h_grad[IDX2C(j,vc_indicies_new[i],rows)] = distribution2(gen);
	// 		}
	// 	}
	// }

	// thrust_d_grad = thrust_h_grad;
	// cudaDeviceSynchronize();
	// //time slow thrust W matrix
	// start_total = std::chrono::system_clock::now();
	// scale_gradient(thrust_d_grad,minibatch_size);
	// norm_clip(thrust_d_grad,norm_clip_val);
	// update_parameters(d_grad,d_param,learning_rate,handle,rows,cols);
	// zero_gradient(thrust_d_grad);
	// cudaDeviceSynchronize();
	// end_total = std::chrono::system_clock::now();
	// elapsed_seconds = end_total-start_total;
 //    std::cout << "\n\n\n";
 //    std::cout << "Runtime of slow W gradient: " << (elapsed_seconds.count()) << " seconds" << std::endl;

 //    //book keeping to compare the two slow and fast methods to be sure they are correct
 //    thrust_h_param = thrust_d_param;
 //    thrust_d_grad = thrust_h_grad;

 //    //time of fast W matrix
 //    start_total = std::chrono::system_clock::now();
 //    //int threads_per_block = 256;
	// //int num_block = (hiddenstate_size + threads_per_block-1)/threads_per_block;
	// //dim3 kernel(num_block,256,1);
 //    //scale_W_gradient<<<kernel,threads_per_block>>>(d_grad,d_vocab_indices,hiddenstate_size,(((precision)1.0)/minibatch_size),len);

	// //norm_clip(thrust_d_grad,norm_clip_val);

	// //norm_clip_W(thrust_d_grad,d_grad,d_vocab_indices,norm_clip_val,len,hiddenstate_size);

	// norm_clip_W_v2(d_grad,d_vocab_indices,vc_indicies_new,norm_clip_val,len,hiddenstate_size);
	// //update_W_gradient<<<kernel,threads_per_block>>>(d_param,d_grad,d_vocab_indices,learning_rate,hiddenstate_size,len);

	// //zero_W_gradient<<<kernel,threads_per_block>>>(d_grad,d_vocab_indices,hiddenstate_size,len);
	// cudaDeviceSynchronize();
	// end_total = std::chrono::system_clock::now();
	// elapsed_seconds = end_total-start_total;
 //    std::cout << "\n\n\n";
 //    std::cout << "Runtime of fast W gradient: " << (elapsed_seconds.count()) << " seconds" << std::endl;

 //    thrust_h_param2 = thrust_d_param;

}

