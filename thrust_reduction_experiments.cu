//Thrust reduction Experiments

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"
#include <thrust/transform_reduce.h>
#include <thrust/execution_policy.h>

typedef float precision;

struct square {
    __host__ __device__
    float operator()(const float& x) const { 
        return x * x;
    }

    __host__ __device__
    double operator()(const double& x) const { 
        return x * x;
    }
};



int main() {

	const int rows = 2;
	const int cols = 3;

	cudaStream_t s1,s2,s3;

	cudaStreamCreate(&s1);
	cudaStreamCreate(&s2);
	cudaStreamCreate(&s3);

	thrust::host_vector<precision> thrust_h_param(rows * cols,0);
	thrust::device_vector<precision> thrust_d_param(rows * cols,0);

	for(int i=0; i<rows; i++) {
		for(int j=0; j<cols; j++) {
			thrust_h_param[IDX2C(i,j,rows)] = i + rows*j;
		}
	}

	thrust_d_param = thrust_h_param;
	print_thrust_matrix(thrust_h_param,rows,cols);

	precision sum = thrust::transform_reduce(thrust::cuda::par.on(s2),thrust_d_param.begin(), 
		thrust_d_param.end(), square(), (precision)0, thrust::plus<precision>());
	std::cout << "sum = " << sum << "\n\n";

	thrust::device_ptr<precision> d_ptr = thrust::device_pointer_cast(thrust::raw_pointer_cast(&thrust_d_param[0]));

	sum = thrust::transform_reduce(d_ptr, 
		d_ptr+2, square(), (precision)0, thrust::plus<precision>());

	std::cout << "sum = " << sum << "\n\n";



}

