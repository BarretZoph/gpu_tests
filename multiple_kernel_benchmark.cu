#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

//#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"


__global__ 
void d_ERRt_ct_kernel(float *d_d_ERRt_ct,float *d_d_ERRnTOt_ht,float *d_o_t,float *d_c_t,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
  		int val = tanhf(d_c_t[index]);
		d_d_ERRt_ct[index] = d_d_ERRnTOt_ht[index] * d_o_t[index] * (1 - val*val);
	}
}

struct tanh_sq_functor {
  double operator() (double x) const { return std::tanh(x)*std::tanh(x); }
};

// template<typename Derived>
// void d_ERRt_ct_eigen(const Eigen::MatrixBase<Derived> &d_ERRt_ct,const Eigen::MatrixBase<Derived> &d_ERRnTOt_ht,
// 	const Eigen::MatrixBase<Derived> &o_t,const Eigen::MatrixBase<Derived> &c_t) {
// 	d_ERRt_ct = d_ERRnTOt_ht.array() * (o_t.array()*(1-(c_t).array().unaryExpr(tanh_sq_functor())));
// }


int main() {

	//constants for the sizes of the matrices
	const int hiddenstate_size = 1000;
	const int minibatch_size = 128;
	const int num_trials = 100;

	//host pointers
	float *h_d_ERRnTOtp1_ht;
	float *h_d_ERRnTOtp1_ct;
	float *h_d_ERRt_ht;
	float *h_d_ERRnTOt_ht;
	float *h_o_t;
	float *h_c_t;
	float *h_d_ERRt_ct;

	//device pointers
	float *d_d_ERRnTOtp1_ht;
	float *d_d_ERRnTOtp1_ct;
	float *d_d_ERRt_ht;
	float *d_d_ERRnTOt_ht;
	float *d_o_t;
	float *d_c_t;
	float *d_d_ERRt_ct;

	//Make the cublas handle
	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");
	
	//initialize host and device pointers
	full_matrix_setup(&h_d_ERRnTOtp1_ht,&d_d_ERRnTOtp1_ht,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_d_ERRt_ct,&d_d_ERRt_ct,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_o_t,&d_o_t,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_c_t,&d_c_t,hiddenstate_size,minibatch_size);

	for(int i=0; i<num_trials; i++) {
		int threads_per_block = 128;
		int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
		dim3 kernel(minibatch_size,num_block,1);
		d_ERRt_ct_kernel<<<kernel,threads_per_block>>>(d_d_ERRt_ct,d_d_ERRnTOt_ht,d_o_t,d_c_t,hiddenstate_size);
		CUDA_GET_LAST_ERROR();
	}

}