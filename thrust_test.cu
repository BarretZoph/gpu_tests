//Thrust test
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <thrust/host_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/iterator/constant_iterator.h>

#include "BZ_CUDA_UTIL.h"

#include <Eigen/Dense>
#include <chrono>

struct tanh_functor {
	__host__ __device__ void operator()(float &x) {
		x = tanhf(x);
	}
};

__global__ 
void tanh_elemwise_128(float *d_result, float *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = tanhf( d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] );
}

int main() {

	int hiddenSize = 1000;
	int miniBatchSize = 128;
	const int num_trials = 100;

	//Thrust stuff
	thrust::host_vector<float> h_result_thrust(hiddenSize*miniBatchSize);
	initialize_thrust_vector(h_result_thrust,hiddenSize*miniBatchSize);
	thrust::device_vector<float> d_result_thrust(hiddenSize*miniBatchSize);

	//cuBLAS stuff
	float *h_matrix;
	float *h_result;
	float *d_matrix;
	float *d_result;

	full_matrix_setup(&h_matrix, &d_matrix, hiddenSize, miniBatchSize);
	full_matrix_setup(&h_result, &d_result, hiddenSize, miniBatchSize);	

	d_result_thrust = h_result_thrust;

	for(int i=0; i<100; i++) {
		thrust::for_each(d_result_thrust.begin(),d_result_thrust.end(),tanh_functor());
	}

	h_result_thrust = d_result_thrust;


	int threadsPerBlock = 128;
	int numBlocks = (hiddenSize + threadsPerBlock - 1) / threadsPerBlock;
	dim3 kernel128(miniBatchSize, numBlocks, 1 );
	for(int i=0; i<num_trials; i++) {
		tanh_elemwise_128<<<kernel128, threadsPerBlock >>>(d_matrix, d_matrix, hiddenSize);
		CUDA_GET_LAST_ERROR();
	}

	get_matrix_cuBLAS(h_result, d_result, hiddenSize, miniBatchSize);

	std::cout << "THRUST: " << std::endl;
	for(int i = 0; i < 3; i++)
	{
		std::cout << h_result_thrust[i] << " ";
	}
	std::cout << "\n\n";

	std::cout << "NORMAL: " << std::endl;
	for(int i = 0; i < 3; i++)
	{
		std::cout << h_result[i] << " ";
	}
	std::cout << "\n\n";


}