//Softmax forward distribution


#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

typedef float precision;

template<typename dType>
__global__ 
void matrix_bias_kernel_128(int hiddenstate_size, dType *d_mat,dType *d_vec,dType *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
	d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] + d_vec[idx];
	}
}

struct exp_functor {
	__host__ __device__ void operator()(float &x) {
		x = expf(x);
	}
	__host__ __device__ void operator()(double &x) {
		x = exp(x);
	}
};

struct exp_functor_eigen {
	template<typename dType>
  dType operator() (dType x) const { return std::exp(x); }
};

//inverse each element in matrix
struct inv_functor {
	template<typename dType>
	__host__ __device__ void operator()(dType &x) {
		x = 1/x;
	}
};

template<typename dType>
__global__ 
void zero_columns_kernel_128(int hiddenstate_size, dType *d_mat,int *d_vec,dType *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
	d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_vec[blockIdx.x];
	}
}


//This kernel adds a matrices rows to a matrices columns, which ones depend on the index
//hiddenstate_size refers to the number of rows in d_mat_final and also d_mat_col
template<typename dType>
__global__
void matrix_row_to_matrix_column_kernel(dType *d_mat_final,dType *d_mat_col,dType *d_mat_row,int *d_indices,int hiddenstate_size,int output_vocab_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_mat_col[IDX2C(idx,blockIdx.x,hiddenstate_size)] + \
		d_mat_row[IDX2C(d_indices[blockIdx.x],idx,output_vocab_size)];
	}
}

//take binary matrix fo ints and convert it to floats
template<typename dType>
__global__ 
void copy_matrix_float_to_int_kernel(int *d_source,dType *d_destination,int size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < size) {
		d_destination[idx] = (dType)d_source[idx];
	}
}


//This kernel adds a matrices columns to a matrices rows, which ones depend on the index
//hiddenstate_size refers to the number of rows in d_mat_final and also d_mat_col
///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__
void matrix_column_to_matrix_row_kernel(float *d_mat_final,float *d_mat_col,float *d_mat_row,int *d_indices,int hiddenstate_size,int output_vocab_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		atomicAdd(&d_mat_final[IDX2C(d_indices[blockIdx.x],idx,output_vocab_size)],d_mat_col[IDX2C(idx,blockIdx.x,hiddenstate_size)]);
	}
}

__global__
void matrix_column_to_matrix_row_kernel(double *d_mat_final,double *d_mat_col,double *d_mat_row,int *d_indices,int hiddenstate_size,int output_vocab_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		atomicAddDouble(&d_mat_final[IDX2C(d_indices[blockIdx.x],idx,output_vocab_size)],d_mat_col[IDX2C(idx,blockIdx.x,hiddenstate_size)]);
	}
}
///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////


template<typename dType>
__global__
void matrix_column_to_matrix_row_kernel_2(dType *d_mat_final,dType *d_mat_col,dType *d_mat_row,int *d_indices,int hiddenstate_size,int output_vocab_size,int minibatch_size) {
	
	for(int i=0; i<minibatch_size; i++) {
		if(d_indices[i]==blockIdx.x) {
			int idx = threadIdx.x + blockIdx.y*blockDim.x;
			if(idx < hiddenstate_size) {
				d_mat_final[IDX2C(blockIdx.x,idx,output_vocab_size)] += d_mat_col[IDX2C(idx,i,hiddenstate_size)];
			}
		}
	}
}


//add ones to b_d bias unit
///////////////////////////////////////////DOUBLE DECLARATION BEGIN/////////////////////////////////////////////
__global__
void add_ones_b_d_grad(float *d_b_d_grad,int *d_output_vocab_indices_01,int *d_output_vocab_indices,int minibatch_size) {
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
	if(idx < minibatch_size && d_output_vocab_indices_01[idx]==1) {
		atomicAdd(&d_b_d_grad[d_output_vocab_indices[idx]],1);
	}
}

__global__
void add_ones_b_d_grad(double *d_b_d_grad,int *d_output_vocab_indices_01,int *d_output_vocab_indices,int minibatch_size) {
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
	if(idx < minibatch_size && d_output_vocab_indices_01[idx]==1) {
		atomicAddDouble(&d_b_d_grad[d_output_vocab_indices[idx]],1);
	}
}
///////////////////////////////////////////DOUBLE DECLARATION END/////////////////////////////////////////////


//get device pointers to thrust vectors for using cuBLAS and custom kernels
precision *d_outputdist;
precision *d_normalization;


//Parameters
//non thrust
//host pointers
precision *h_D;
precision *h_h_t;
precision *h_b_d;
precision *h_d_ERRt_ht;
precision *h_ones;
int *h_output_vocab_indices;
int *h_output_vocab_indices_01;
precision *h_D_grad;
precision *h_output_vocab_indices_01_float;
precision *h_b_d_grad;

//device pointers
precision *d_D;
precision *d_h_t;
precision *d_b_d;
precision *d_d_ERRt_ht;
precision *d_ones;
int *d_output_vocab_indices;
int *d_output_vocab_indices_01;
precision *d_D_grad;
precision *d_output_vocab_indices_01_float;
precision *d_b_d_grad;

//Stream stuff
cudaStream_t s1,s2,s3;




//for the softmax, note nothing is zeroed out for this operation
template<typename dType>
void get_distribution(int output_vocab_size,int hiddenstate_size,int minibatch_size,cublasHandle_t handle,
	thrust::device_vector<dType> &thrust_d_outputdist,thrust::device_vector<dType> &thrust_d_normalization) 
{
	//multiply the D matrix with the hidden state matrix
	precision alpha = 1;
	precision beta = 0;
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle, CUBLAS_OP_N, CUBLAS_OP_N,
	 output_vocab_size, minibatch_size, hiddenstate_size, &alpha, d_D, output_vocab_size,
	  d_h_t, hiddenstate_size, &beta, d_outputdist, output_vocab_size),"get_distribution cuBLAS call failed\n");

	// for(int i=0; i<output_vocab_size; i++) {
	// 	for(int j=0; j<minibatch_size; j++) {
	// 		std::cout << thrust_d_outputdist[IDX2C(i,j,output_vocab_size)] << " ";
	// 	}
	// 	std::cout << "\n";
	// }
	// std::cout << "\n";

	//add the bias vector to the matrix
	int threads_per_block = 128;
	int num_block = (output_vocab_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(minibatch_size,num_block,1);
	matrix_bias_kernel_128<<< kernel_dim,threads_per_block >>>(output_vocab_size,d_outputdist,d_b_d,d_outputdist);
	CUDA_GET_LAST_ERROR();

	//exp every element in the outputDist matrix
	thrust::for_each(thrust_d_outputdist.begin(),thrust_d_outputdist.end(),exp_functor());

	//get the normalization vector
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_T,output_vocab_size,minibatch_size,&alpha,d_outputdist,output_vocab_size,
		d_ones,1,&beta,d_normalization,1),"cuBLAS normaliztion failed\n");

	//invert the values in the normalization matrix
	thrust::for_each(thrust_d_normalization.begin(),thrust_d_normalization.end(),inv_functor());

	//renormalize outputdist with the normalization vector
	CUBLAS_ERROR_WRAPPER(cublas_dgmm_wrapper(handle,CUBLAS_SIDE_RIGHT,output_vocab_size,minibatch_size,d_outputdist,output_vocab_size,
		d_normalization,1,d_outputdist,output_vocab_size),"cuBLAS normalization part 2 failed\n");

}

//get the error for the softmax with respect to h_t
//output vocab indicies should contain no -1's
//output vocab indicies should contain all 1's except for zeros where the column should be zeroed out
void get_h_t_gradient(int output_vocab_size,int hiddenstate_size,int minibatch_size,cublasHandle_t handle) {

	precision alpha = -1;
	precision beta = 0;
	//multiply outputdist by D
	cublasSetStream(handle,s1);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,output_vocab_size,
		&alpha,d_D,output_vocab_size,d_outputdist,output_vocab_size,&beta,d_d_ERRt_ht,hiddenstate_size),"cuBLAS h_t gradient failed");

	//debugging
	//get_matrix_cuBLAS(h_d_ERRt_ht,d_ERRt_ht,hiddenstate_size,minibatch_size);
	//print_matrix(h_d_ERRt_ht,hiddenstate_size,minibatch_size);

	//add in the D rows
	int threads_per_block = 128;
	int num_block = (output_vocab_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(minibatch_size,num_block,1);
	matrix_row_to_matrix_column_kernel<<< kernel_dim,threads_per_block,0,s1 >>>(d_d_ERRt_ht,d_d_ERRt_ht,d_D,d_output_vocab_indices,hiddenstate_size,output_vocab_size);

	//zero out columns
	int num_block_2 = (hiddenstate_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim_2(minibatch_size,num_block_2,1);
	zero_columns_kernel_128<<<kernel_dim_2,threads_per_block,0,s1 >>>(hiddenstate_size,d_d_ERRt_ht,d_output_vocab_indices_01,d_d_ERRt_ht);
}

//output vocab indices cannot contain any -1's
void compute_D_gradient(int hiddenstate_size,int minibatch_size,int output_vocab_size,cublasHandle_t handle) {
	//zero out h_t
	int threads_per_block = 128;
	int num_block = (hiddenstate_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(minibatch_size,num_block,1);
	zero_columns_kernel_128<<<kernel_dim,threads_per_block,0,s2 >>>(hiddenstate_size,d_h_t,d_output_vocab_indices_01,d_h_t);
	CUDA_GET_LAST_ERROR();

	//multiply output dist and h_t
	precision alpha = -1;
	precision beta = 1;
	cublasSetStream(handle,s2);
	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,output_vocab_size,hiddenstate_size,minibatch_size,&alpha,d_outputdist,output_vocab_size,
		d_h_t,hiddenstate_size,&beta,d_D_grad,output_vocab_size),"computing softmax D gradient failed in cuBLAS\n");

	// //debugging
	//get_matrix_cuBLAS(h_D_grad,d_D_grad,output_vocab_size,hiddenstate_size);
	//print_matrix(h_D_grad,output_vocab_size,hiddenstate_size);

	//add columns of h_t to D_grad
	matrix_column_to_matrix_row_kernel<<< kernel_dim,threads_per_block,0,s2 >>>(d_D_grad,d_h_t,d_D_grad,d_output_vocab_indices,hiddenstate_size,output_vocab_size);
	CUDA_GET_LAST_ERROR();

	//testing to see if faster (its not)
	// int num_block_2 = (hiddenstate_size + threads_per_block-1)/threads_per_block;
	// dim3 kernel_dim_2(output_vocab_size,num_block_2,1);
	// matrix_column_to_matrix_row_kernel_2<<< kernel_dim_2,threads_per_block >>>(d_D_grad,d_h_t,d_D_grad,d_output_vocab_indices,hiddenstate_size,output_vocab_size,minibatch_size);
	// CUDA_GET_LAST_ERROR();
	//get_matrix_cuBLAS(h_D_grad,d_D_grad,output_vocab_size,hiddenstate_size);
	//print_matrix(h_D_grad,output_vocab_size,hiddenstate_size);

}

void prep_b_d_vocab_indices(int minibatch_size) {

	int threads_per_block = 128;
	int num_block = (minibatch_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(1,num_block,1);
	copy_matrix_float_to_int_kernel<<< kernel_dim,threads_per_block,0,s3 >>>(d_output_vocab_indices_01,d_output_vocab_indices_01_float,minibatch_size);
	CUDA_GET_LAST_ERROR();
}


void compute_b_d_gradient(int minibatch_size,cublasHandle_t handle,int output_vocab_size) {

	//copy ints of 0's and 1's for floats of 0's and 1's for using cuBLAS
	prep_b_d_vocab_indices(minibatch_size);

	//multiply
	precision alpha = -1;
	precision beta = 1;
	cublasSetStream(handle,s3);
	CUBLAS_ERROR_WRAPPER(cublas_gemv_wrapper(handle,CUBLAS_OP_N,output_vocab_size,minibatch_size,&alpha,d_outputdist,output_vocab_size,
		d_output_vocab_indices_01_float,1,&beta,d_b_d_grad,1),"cuBLAS compute b_d_gradient failed");

	//add ones
	int threads_per_block = 128;
	int num_block = (minibatch_size + threads_per_block-1)/threads_per_block;
	dim3 kernel_dim(1,num_block,1);
	add_ones_b_d_grad<<< kernel_dim,threads_per_block,0,s3>>>(d_b_d_grad,d_output_vocab_indices_01,d_output_vocab_indices,minibatch_size);
}

template<typename dType>
void softmax_backward(int output_vocab_size,int hiddenstate_size,int minibatch_size,cublasHandle_t handle,
	thrust::device_vector<dType> &thrust_d_outputdist,thrust::device_vector<dType> &thrust_d_normalization) 
{	
	//get the output distribution
	get_distribution(output_vocab_size,hiddenstate_size,minibatch_size,handle,thrust_d_outputdist,thrust_d_normalization);
	cudaDeviceSynchronize();

	//Now get error for h_t, compute gradient for D and b_d
	get_h_t_gradient(output_vocab_size,hiddenstate_size,minibatch_size,handle);

	//Get gradient for D
	compute_D_gradient(hiddenstate_size,minibatch_size,output_vocab_size,handle);

	//Get gradient for b_d
	compute_b_d_gradient(minibatch_size,handle,output_vocab_size);

	//synch the device from the other streams
	cudaDeviceSynchronize();

}


template<typename Derived,typename Derived2,typename Derived3>
void eigen_compute_b_d_gradient(const Eigen::MatrixBase<Derived> &outputDist,
	const Eigen::MatrixBase<Derived2> &vocab_indicies,const Eigen::MatrixBase<Derived3> &b_d_grad_const) 
{
	UNCONST(Derived3,b_d_grad_const,b_d_grad);
	for(int i=0; i<vocab_indicies.rows(); i++) {
		if(vocab_indicies(i)!=-1) {
			b_d_grad += -1*outputDist.col(i);
		}
	}

	for(int i=0; i<outputDist.cols(); i++) {
		if(vocab_indicies(i)!=-1) {
			b_d_grad(vocab_indicies(i)) += 1;
		}
	}
}


template<typename Derived,typename Derived2>
void eigen_compute_D_gradient(const Eigen::MatrixBase<Derived> &d_h_t_const,const Eigen::MatrixBase<Derived> &D_grad_const,
	const Eigen::MatrixBase<Derived2> &vocab_indicies,const Eigen::MatrixBase<Derived> &outputDist) {
	UNCONST(Derived,d_h_t_const,d_h_t);
	UNCONST(Derived,D_grad_const,D_grad);

	//Zero out the hidden state columns where vocab
	for(int i=0; i<vocab_indicies.rows(); i++) {
		if(vocab_indicies(i)==-1) {
			d_h_t.col(i).setZero();
		}
	}

	D_grad.noalias() += -1*(outputDist*d_h_t.transpose());

	//print_eigen_matrix(D_grad);

	for(int i=0; i<outputDist.cols(); i++) {
		if(vocab_indicies(i)!=-1) {
			D_grad.row(vocab_indicies(i))+= d_h_t.col(i).transpose();
		}
	}
	//print_eigen_matrix(D_grad);
	//print_eigen_matrix(vocab_indicies);
	//print_eigen_matrix(d_h_t);
}

template<typename Derived,typename Derived2,typename Derived3>
void eigen_get_distribution(const Eigen::MatrixBase<Derived> &h_outputdist_const,const Eigen::MatrixBase<Derived2> &h_normalization_const,
	const Eigen::MatrixBase<Derived3> &h_b_d,const Eigen::MatrixBase<Derived> &h_h_t,const Eigen::MatrixBase<Derived> &h_D) {

	UNCONST(Derived,h_outputdist_const,h_outputdist);
	UNCONST(Derived2,h_normalization_const,h_normalization);

	h_normalization.setZero();
	//h_outputdist = ( (h_D*h_h_t).colwise() + h_b_d).array().unaryExpr(exp_functor_eigen());
	h_outputdist = h_D*h_h_t;
	//print_eigen_matrix(h_outputdist);
	h_outputdist = h_outputdist.colwise() + h_b_d;
	h_outputdist = h_outputdist.array().unaryExpr(exp_functor_eigen());
	//print_eigen_matrix(h_outputdist);
	

	for(int i=0; i<h_outputdist.rows(); i++) {
		h_normalization += h_outputdist.row(i);
	}

	//print_eigen_matrix(h_normalization);

	for(int i=0; i<h_outputdist.rows(); i++) {
		h_outputdist.row(i) = (h_outputdist.row(i).array()/h_normalization.array()).matrix();
	}
}


template<typename Derived,typename Derived2>
void eigen_compute_errht(const Eigen::MatrixBase<Derived> &h_outputdist,
	const Eigen::MatrixBase<Derived> &h_d_Errt_ht_const,const Eigen::MatrixBase<Derived> &h_D,
	const Eigen::MatrixBase<Derived2> &h_vocab_indicies) {

	UNCONST(Derived,h_d_Errt_ht_const,h_d_Errt_ht);

	h_d_Errt_ht.setZero();

	//Compute second part of gradient
	for(int i=0; i<h_D.rows(); i++) {
		for(int j=0; j<h_d_Errt_ht.rows(); j++) {
			if(h_vocab_indicies(j)!=-1) {
				h_d_Errt_ht.row(j)+= -(h_outputdist(i,j))*(h_D.row(i));
			}
		}
	}

	//print_eigen_matrix(h_d_Errt_ht.transpose());

	for(int i=0; i<h_d_Errt_ht.rows(); i++) {
		if(h_vocab_indicies(i)!=-1) {
			h_d_Errt_ht.row(i) += h_D.row(h_vocab_indicies(i));
		}
	}
}



int main() {
	const int output_vocab_size = 40000;
	const int hiddenstate_size = 1000;
	const int minibatch_size = 128;
	const int num_trials = 1;


	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");

	//thrust host vectors
	thrust::host_vector<precision > thrust_h_outputdist(output_vocab_size * minibatch_size);
	thrust::host_vector<precision > thrust_h_normalization(1 * minibatch_size);


	//thrust device vectors
	thrust::device_vector<precision > thrust_d_outputdist(output_vocab_size * minibatch_size);
	thrust::device_vector<precision > thrust_d_normalization(1 * minibatch_size);

    //allocate the host thrust vectors
    initialize_thrust_vector(thrust_h_outputdist,output_vocab_size * minibatch_size);
    initialize_thrust_vector(thrust_h_normalization,1 * minibatch_size);

     //copy host vectors to device vectors
    thrust_d_outputdist = thrust_h_outputdist;
    thrust_d_normalization = thrust_h_normalization;

    //get device pointers to thrust vectors for using cuBLAS and custom kernels
   	d_outputdist = thrust::raw_pointer_cast(&thrust_d_outputdist[0]);
    d_normalization = thrust::raw_pointer_cast(&thrust_d_normalization[0]);

    //Streams
    cudaStreamCreate(&s1);
	cudaStreamCreate(&s2);
	cudaStreamCreate(&s3);

	//set up CUDA memory
	full_matrix_setup(&h_D,&d_D,output_vocab_size,hiddenstate_size);
	full_matrix_setup(&h_h_t,&d_h_t,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_b_d,&d_b_d,output_vocab_size,1);
	full_matrix_setup(&h_d_ERRt_ht,&d_d_ERRt_ht,hiddenstate_size,minibatch_size);
	full_vector_setup_ones(&h_ones,&d_ones,output_vocab_size);
	full_matrix_setup(&h_D_grad,&d_D_grad,output_vocab_size,hiddenstate_size);
	full_vector_setup_vocab(&h_output_vocab_indices,&d_output_vocab_indices,minibatch_size,output_vocab_size);
	full_vector_setup_vocab_01(&h_output_vocab_indices_01,&d_output_vocab_indices_01,minibatch_size);
	full_vector_setup(&h_output_vocab_indices_01_float,&d_output_vocab_indices_01_float,minibatch_size);
	full_vector_setup(&h_b_d_grad,&d_b_d_grad,output_vocab_size);


	//eigen vocab initialization
	Eigen::Matrix<int, Eigen::Dynamic, 1> h_eigen_output_vocab_indices(minibatch_size,1);
	for(int i=0; i<minibatch_size; i++) {
		if(h_output_vocab_indices_01[i]==1) {
			h_eigen_output_vocab_indices[i] = h_output_vocab_indices[i];
		}
		else {
			h_eigen_output_vocab_indices[i] = -1;
		}
	}

    //eigen matrices
    Eigen::Matrix<precision , Eigen::Dynamic, Eigen::Dynamic> h_eigen_outputdist(output_vocab_size,minibatch_size);
	Eigen::Matrix<precision , 1, Eigen::Dynamic> h_eigen_normalization(1,minibatch_size);
	Eigen::Matrix<precision , Eigen::Dynamic, Eigen::Dynamic> h_eigen_D(output_vocab_size,hiddenstate_size);
	Eigen::Matrix<precision , Eigen::Dynamic, Eigen::Dynamic> h_eigen_h_t(hiddenstate_size,minibatch_size);
	Eigen::Matrix<precision , Eigen::Dynamic, 1> h_eigen_b_d(output_vocab_size,1);
	Eigen::Matrix<precision , Eigen::Dynamic, 1> h_eigen_ones(output_vocab_size,1);
	Eigen::Matrix<precision , Eigen::Dynamic, 1> h_eigen_h_b_d_grad(output_vocab_size,1);
	Eigen::Matrix<precision , Eigen::Dynamic, Eigen::Dynamic> h_eigen_h_d_ERRt_ht(minibatch_size,hiddenstate_size);
	Eigen::Matrix<precision , Eigen::Dynamic, Eigen::Dynamic> h_eigen_h_D_grad(output_vocab_size,hiddenstate_size);


    //copy GPU matrices to eigen
	copy_to_eigen_thrust(h_eigen_outputdist,thrust_h_outputdist);
	copy_to_eigen_thrust(h_eigen_normalization,thrust_h_normalization);
	copy_to_eigen(h_eigen_D,h_D);
	copy_to_eigen(h_eigen_h_t,h_h_t);
	copy_to_eigen(h_eigen_b_d,h_b_d);
	copy_to_eigen(h_eigen_ones,h_ones);

	copy_to_eigen(h_eigen_h_b_d_grad,h_b_d_grad);
	copy_to_eigen(h_eigen_h_D_grad,h_D_grad);
	copy_to_eigen(h_eigen_h_d_ERRt_ht,h_d_ERRt_ht);


	///////////////////////////////////CUDA backprop for softmax////////////////////////////////
	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	for(int i=0; i< num_trials; i++) {
		cudaProfilerStart();
		softmax_backward(output_vocab_size,hiddenstate_size,minibatch_size,handle,thrust_d_outputdist,thrust_d_normalization);
		cudaProfilerStop();
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of GPU: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


	//run all the eigen code
    for(int i=0; i<1; i++) {
    	eigen_get_distribution(h_eigen_outputdist,h_eigen_normalization,
		h_eigen_b_d,h_eigen_h_t,h_eigen_D);
		eigen_compute_errht(h_eigen_outputdist,h_eigen_h_d_ERRt_ht,h_eigen_D,h_eigen_output_vocab_indices);
		eigen_compute_D_gradient(h_eigen_h_t,h_eigen_h_D_grad,h_eigen_output_vocab_indices,h_eigen_outputdist);
		eigen_compute_b_d_gradient(h_eigen_outputdist,h_eigen_output_vocab_indices,h_eigen_h_b_d_grad);
    }

    //////////////////////////////////////////////////Do all Eigen checks////////////////////////////////////////////

    thrust_h_outputdist = thrust_d_outputdist;
    cudaDeviceSynchronize();
	if(eigen_check_thres_thrust(h_eigen_outputdist,thrust_h_outputdist,(precision)0.001)) {
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}
	
	//compare
	get_matrix_cuBLAS(h_d_ERRt_ht,d_d_ERRt_ht,hiddenstate_size,minibatch_size);
	cudaDeviceSynchronize();
	if(eigen_check_thres(h_eigen_h_d_ERRt_ht.transpose(),h_d_ERRt_ht,(precision)0.001)) {
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}

	//compare GPU to eigen
	get_matrix_cuBLAS(h_D_grad,d_D_grad,output_vocab_size,hiddenstate_size);
	cudaDeviceSynchronize();
	if(eigen_check_thres(h_eigen_h_D_grad,h_D_grad,(precision)0.001)) {
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}

    //compare GPU to eigen
	get_vector_cuBLAS(h_b_d_grad,d_b_d_grad,output_vocab_size);
	cudaDeviceSynchronize();
	if(eigen_check_thres(h_eigen_h_b_d_grad,h_b_d_grad,(precision)0.001)) {
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}

	cudaDeviceSynchronize();
}

