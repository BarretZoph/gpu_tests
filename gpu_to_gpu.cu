#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

// #include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"


void set_zero(float *ptr,int size) {
	for(int i=0; i<size; i++) ptr[i] =0;
}

bool check_correct(float *ptr,int size) {
	for(int i=0; i<size; i++){
		if(ptr[i]!=i) {
			return false;
		}
	}
	return true;
}

void synch_all() {
	cudaSetDevice(0);
	cudaDeviceSynchronize();

	cudaSetDevice(1);
	cudaDeviceSynchronize();
}


int main(int argc, char * argv[])
{

	const int rows = 2000;
	const int cols = 128;
	const int num_trials = 1000;
	const int num_elem = rows*cols;
	const int size = num_elem*sizeof(float);

	float * h_zeros;



	float * d_unpinned_mat1_device0;
	float * d_unpinned_mat1_device1;
	float * h_unpinned_mat1;

	h_unpinned_mat1 = (float*)malloc(size); 
	h_zeros = (float*)malloc(size);

	for (int i = 0; i < num_elem; ++i) h_unpinned_mat1[i] = i;  
	memset(h_zeros,0,size);

	CUDA_ERROR_WRAPPER( cudaSetDevice(0), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMalloc((void**)&d_unpinned_mat1_device0, size) ,"device unpinned device 0 malloc failed\n");
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device0, h_unpinned_mat1, size, cudaMemcpyHostToDevice) ,"device unpinned device 0 memcpy failed\n");

	CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 1");
	CUDA_ERROR_WRAPPER( cudaMalloc((void**)&d_unpinned_mat1_device1, size), "Could not cudamalloc unpinned_mat1_device1" );
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device1, h_zeros, size, cudaMemcpyHostToDevice), "could not copy d_unpinned_mat1_device1 and h_zero" );


	set_zero(h_unpinned_mat1,num_elem);
	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	synch_all();
	start_total = std::chrono::system_clock::now();
	
	for(int i = 0; i < num_trials; i++)
	{
		CUDA_ERROR_WRAPPER( cudaSetDevice(0), "Could not set to Device 0");
		CUDA_ERROR_WRAPPER( cudaMemcpy(h_unpinned_mat1, d_unpinned_mat1_device0, size, cudaMemcpyDeviceToHost), "could not memcpy h_unpinned_mat1 to d_unpinned_mat1_device0" );

		CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 1" );
		CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device1, h_unpinned_mat1, size, cudaMemcpyHostToDevice), "could not memcpy d_unpinned_mat1_device1 to h_unpinned_mat1" );
	}
	synch_all();
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of GPU To CPU to GPU unpinned is: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;



    set_zero(h_unpinned_mat1,num_elem);
    CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMemcpy(h_unpinned_mat1, d_unpinned_mat1_device1, size, cudaMemcpyDeviceToHost), "could not memcpy h_unpinned_mat1 to d_unpinned_mat1_device1" );
	if(check_correct(h_unpinned_mat1,num_elem)) {
		std::cout << "Unpinned worked correctly\n";
	}
	else {
		std::cout << "unpinned failed\n";
	}



    float * h_pinned_mat1;
 

    CUDA_ERROR_WRAPPER( cudaSetDevice(0), "Could not set to Device 0");
    CUDA_ERROR_WRAPPER( cudaMallocHost((void**)&h_pinned_mat1, size), "Could not cuda malloc host h_pinned_mat1" );

    for (int i = 0; i < num_elem; ++i) h_pinned_mat1[i] = i;  
	memset(h_zeros,0,size);

	CUDA_ERROR_WRAPPER( cudaSetDevice(0), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device0, h_pinned_mat1, size, cudaMemcpyHostToDevice), "could not memcpy d_unpinned_mat1_device0 to h_pinned_mat1" );

	CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 1");
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device1, h_zeros, size, cudaMemcpyHostToDevice), "could not memcpy d_unpinned_mat1_device1 to h_zeros" );
	synch_all();
	start_total = std::chrono::system_clock::now();
	
	for(int i = 0; i < num_trials; i++)
	{
		CUDA_ERROR_WRAPPER( cudaSetDevice(0), "Could not set to Device 0");
		CUDA_ERROR_WRAPPER( cudaMemcpy(h_pinned_mat1, d_unpinned_mat1_device0, size, cudaMemcpyDeviceToHost), "could not cuda memcpy h_pinned_mat1 to d_unpinned_mat1_device0" );

		CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 1" );
		CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device1, h_pinned_mat1, size, cudaMemcpyHostToDevice), "could not cuda memcpy d_unpinned_mat1_device1 to h_pinned_mat1" );
	}

	synch_all();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of GPU To CPU to GPU pinned is: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


    set_zero(h_pinned_mat1,num_elem);
    CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMemcpy(h_pinned_mat1, d_unpinned_mat1_device1, size, cudaMemcpyDeviceToHost), "could not memcpy h_pinned_mat1 to d_unpinned_mat1_device1" );
	if(check_correct(h_pinned_mat1,num_elem)) {
		std::cout << "pinned worked correctly\n";
	}
	else {
		std::cout << "pinned failed\n";
	}


    //---------------------------------------------DMA-----------------------------------------


    cudaError_t status =  cudaDeviceEnablePeerAccess( 1, 0 );
	if (status == cudaSuccess)
		  printf("cudaSuccess: 1 to 0");
	else if(status == cudaErrorInvalidDevice)
		printf("cudaErrorInvalidDevice: 1 to 0");
	else if(status == cudaErrorInvalidDevice)
		printf("cudaErrorPeerAccessAlreadyEnabled: 1 to 0");
	else if(status == cudaErrorInvalidDevice)
		printf("cudaErrorInvalidValue: 1 to 0");

    status =  cudaDeviceEnablePeerAccess( 0, 1 );
	if (status == cudaSuccess)
		  printf("cudaSuccess: 0 to 1");
	else if(status == cudaErrorInvalidDevice)
		printf("cudaErrorInvalidDevice: 0 to 1");
	else if(status == cudaErrorInvalidDevice)
		printf("cudaErrorPeerAccessAlreadyEnabled: 0 to 1");
	else if(status == cudaErrorInvalidDevice)
		printf("cudaErrorInvalidValue: 0 to 1");

	for (int i = 0; i < num_elem; ++i) h_pinned_mat1[i] = i;  
	memset(h_zeros,0,size);

	CUDA_ERROR_WRAPPER( cudaSetDevice(0), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device0, h_pinned_mat1, size, cudaMemcpyHostToDevice), "could not cuda memcpy from d_unpinned_mat1_device0 to h_pinned_mat1" );

	CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 1");
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device1, h_zeros, size, cudaMemcpyHostToDevice), "could not cuda memcpy d_unpinned_mat1_device1 to h_zeros" );
	synch_all();
    start_total = std::chrono::system_clock::now();
	for(int i = 0; i < num_trials; i++)
	{
		CUDA_ERROR_WRAPPER( cudaMemcpyPeer(d_unpinned_mat1_device1, 1, d_unpinned_mat1_device0, 0, size), "could not cuda memcpy peer for DMA transfer" );
	}
	synch_all();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of GPU To GPU DMA trasfer is: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

    set_zero(h_pinned_mat1,num_elem);
    CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMemcpy(h_pinned_mat1, d_unpinned_mat1_device1, size, cudaMemcpyDeviceToHost), "could not cuda memcpy h_pinned_mat1 to d_unpinned_mat1_device1" );
	if(check_correct(h_pinned_mat1,num_elem)) {
		std::cout << "DMA worked correctly\n";
	}
	else {
		std::cout << "DMA failed\n";
	}

//     cudaMemcpyPeerAsync( void* dst_addr, int dst_dev,
// void* src_addr, int src_dev,
// size_t num_bytes, cudaStream_t stream )

	for (int i = 0; i < num_elem; ++i) h_pinned_mat1[i] = i;  
	memset(h_zeros,0,size);

	CUDA_ERROR_WRAPPER( cudaSetDevice(0), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device0, h_pinned_mat1, size, cudaMemcpyHostToDevice), "could not cuda memcpy from d_unpinned_mat1_device0 to h_pinned_mat1" );

	CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 1");
	CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device1, h_zeros, size, cudaMemcpyHostToDevice), "could not cuda memcpy d_unpinned_mat1_device1 to h_zeros" );
	synch_all();
    start_total = std::chrono::system_clock::now();
    cudaSetDevice(0);
	for(int i = 0; i < num_trials; i++)
	{
		CUDA_ERROR_WRAPPER( cudaMemcpy(d_unpinned_mat1_device1, d_unpinned_mat1_device0, size, cudaMemcpyDefault), "could not use UVA to transfer!" );
	}
	synch_all();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of GPU To GPU UVA trasfer is: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

    set_zero(h_pinned_mat1,num_elem);
    CUDA_ERROR_WRAPPER( cudaSetDevice(1), "Could not set to Device 0");
	CUDA_ERROR_WRAPPER( cudaMemcpy(h_pinned_mat1, d_unpinned_mat1_device1, size, cudaMemcpyDeviceToHost), "could not cuda memcpy h_pinned_mat1 to d_unpinned_mat1_device1" );
	if(check_correct(h_pinned_mat1,num_elem)) {
		std::cout << "UVA worked correctly\n";
	}
	else {
		std::cout << "UVA failed\n";
	}


}




