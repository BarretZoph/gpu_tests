#ifndef CONV_CHAR_H
#define CONV_CHAR_H

#include "highway_network.h"
#include "charCNN_node.h"

template<typename dType>
class conv_char_layer {
public:

	//model info
	int longest_word = 8;
	int char_emb_size = 4;
	//int word_emb_size = 100;
	int minibatch_size = 3;
	int num_unique_chars = 15;
	int filter_size = 6;
	int num_filters = 7;
	int longest_sent = 10;

	int num_highway_networks = 4;
	std::vector<highway_network_layer<dType>*> highway_layers;


	//gpu info
	int device_number = 0;
	cudnnHandle_t cudnnHandle;
	cudnnDataType_t cudnn_dtype; //datatype for cudnn
	const cudnnTensorFormat_t cudnn_tensor_format = CUDNN_TENSOR_NCHW; //type of tensor


	//params
	dType *d_Q; //character embeddings
	dType *d_C; //character embeddings for specific word memory
	dType *d_H; //storage for filter
	// dType *d_output_conv;
	// dType *d_output_pooling;
	dType *d_b; //bias for convolution

	dType *d_output_conv_err;
	dType *d_output_pooling_err;
	dType *d_H_grad; //storage for filter gradient
	dType *d_C_err; //error for character embeddings
	dType *d_Q_grad; //character embeddings
	dType *d_b_grad; //gradient of bias for convolution

	int *d_vocab_indicies;

	cudnnTensorDescriptor_t tensor_C; //character embeddings tensor descriptor
	cudnnTensorDescriptor_t tensor_b; //bias
	// cudnnTensorDescriptor_t tensor_output_conv; //output from character convolution before max pooling
	// cudnnTensorDescriptor_t tensor_output_pooling; //output from max pooling
	cudnnFilterDescriptor_t filter_H; //cudnn filter for going over character embeddings

	cudnnTensorDescriptor_t tensor_output_conv_err; //output from character convolution before max pooling
	cudnnTensorDescriptor_t tensor_output_pooling_err; //output from max pooling
	cudnnFilterDescriptor_t filter_H_grad; //grad for filters
	cudnnTensorDescriptor_t tensor_C_grad; //grad for filters
	cudnnTensorDescriptor_t tensor_b_grad; //bias


	dType *d_workspace_conv_forward; //for cudnn algorithms
	size_t workspace_conv_forward_size;

	dType *d_workspace_conv_backward_data; //for cudnn algorithms
	size_t workspace_conv_backward_data_size;
	dType *d_workspace_conv_backward_filter; //for cudnn algorithms
	size_t workspace_conv_backward_filter_size;

	cudnnConvolutionFwdAlgo_t cudnn_conv_algo = CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM;
	cudnnConvolutionBwdDataAlgo_t cudnn_conv_back_data_algo = CUDNN_CONVOLUTION_BWD_DATA_ALGO_0;
	cudnnConvolutionBwdFilterAlgo_t cudnn_conv_back_filter_algo = CUDNN_CONVOLUTION_BWD_FILTER_ALGO_0;
	cudnnPoolingDescriptor_t cudnn_poolingDesc; //for max pooling

	cudnnConvolutionDescriptor_t cudnn_conv_info;

	std::vector<charCNN_node<dType> *> nodes;

	void init();
	void fill_char_embeddings();
	dType forward(int index);
	void backward(int index);
	void clear_gradients();
};


template<typename dType>
void conv_char_layer<dType>::clear_gradients() {

	for(int i=0; i<highway_layers.size(); i++) {
		highway_layers[i]->clear_gradients();
	}

	cudaMemset(d_H_grad,0,char_emb_size*num_filters*filter_size*sizeof(dType));
	cudaMemset(d_Q_grad,0,char_emb_size*num_unique_chars*sizeof(dType));
	cudaMemset(d_b_grad,0,num_filters*sizeof(dType));
}


template<typename dType>
void conv_char_layer<dType>::init() {

	//create cuDNN handle
	checkCUDNN(cudnnCreate(&cudnnHandle));

	//set datatype
	if(sizeof(dType) == sizeof(float)) {
		cudnn_dtype = CUDNN_DATA_FLOAT;
	}
	else {
		cudnn_dtype = CUDNN_DATA_DOUBLE;
	}	

	std::cout << "cuDNN version being used: " << cudnnGetVersion() << "\n";

	for(int i=0; i<longest_sent; i++) {
		nodes.push_back( new charCNN_node<dType>(cudnn_tensor_format,cudnn_dtype,
			minibatch_size,num_filters,longest_word,filter_size) );
	}

	//set up cudnn convolution info
	checkCUDNN(cudnnCreateConvolutionDescriptor(&cudnn_conv_info));
	checkCUDNN(cudnnSetConvolution2dDescriptor( cudnn_conv_info,
		0, //pad_h
		0, //pad_w
		1, //u
		1, //v
		1, //upscalex
		1, //upscaley
		CUDNN_CONVOLUTION ));

	//set up highway networks
	for(int i = 0; i<num_highway_networks; i++) {
		highway_layers.push_back( new highway_network_layer<dType>() );
		highway_layers[i]->init(num_filters,minibatch_size,1);
	}

	//Allocate Q embedding
	dType *h_temp;
	full_matrix_setup(&h_temp,&d_Q,char_emb_size,num_unique_chars);
	full_matrix_setup(&h_temp,&d_C,char_emb_size,longest_word*minibatch_size); //this is actually a tensor
	full_matrix_setup(&h_temp,&d_H,char_emb_size,num_filters*filter_size); //this is actually a tensor
	//full_matrix_setup(&h_temp,&d_output_conv,num_filters*(longest_word - filter_size + 1),minibatch_size); //this is actually a tensor
	full_matrix_setup(&h_temp,&d_b,num_filters,1);
	//full_matrix_setup(&h_temp,&d_output_pooling,num_filters,minibatch_size); //this is actually a tensor
	
	full_matrix_setup(&h_temp,&d_Q_grad,char_emb_size,num_unique_chars);
	full_matrix_setup(&h_temp,&d_C_err,char_emb_size,longest_word*minibatch_size); //this is actually a tensor
	full_matrix_setup(&h_temp,&d_H_grad,char_emb_size,num_filters*filter_size); //this is actually a tensor
	full_matrix_setup(&h_temp,&d_output_conv_err,num_filters*(longest_word - filter_size + 1),minibatch_size); //this is actually a tensor
	full_matrix_setup(&h_temp,&d_b_grad,num_filters,1);
	full_matrix_setup(&h_temp,&d_output_pooling_err,num_filters,minibatch_size); //this is actually a tensor

	int *h_vec;
	full_vector_setup_vocab(&h_vec,&d_vocab_indicies,longest_word*minibatch_size,num_unique_chars);

	//allocation for tensor C
	checkCUDNN(cudnnCreateTensorDescriptor(&tensor_C));
	checkCUDNN(cudnnSetTensor4dDescriptor( tensor_C,
    	cudnn_tensor_format,
        cudnn_dtype,
        minibatch_size,  //n
        1,  //c
		longest_word,  //h
		char_emb_size ));  //w

	//allocate tensor for the bias
	checkCUDNN(cudnnCreateTensorDescriptor(&tensor_b));
	checkCUDNN(cudnnSetTensor4dDescriptor( tensor_b,
    	cudnn_tensor_format,
        cudnn_dtype,
        1,  //n
        num_filters,  //c
		1,  //h
		1 ));  //w

	// //allocation for tensor for output_conv
	// checkCUDNN(cudnnCreateTensorDescriptor(&tensor_output_conv));
	// checkCUDNN(cudnnSetTensor4dDescriptor( tensor_output_conv,
 //    	cudnn_tensor_format,
 //        cudnn_dtype,
 //        minibatch_size,  //n
 //        num_filters,  //c
	// 	longest_word - filter_size + 1,  //h
	// 	1 ));  //w


	// //allocation for tensor for output of pooling
	// checkCUDNN(cudnnCreateTensorDescriptor(&tensor_output_pooling));
	// checkCUDNN(cudnnSetTensor4dDescriptor( tensor_output_pooling,
 //    	cudnn_tensor_format,
 //        cudnn_dtype,
 //        minibatch_size,  //n
 //        num_filters,  //c
	// 	1,  //h
	// 	1 ));  //w


	//allocation for tensor for output of pooling gradient
	checkCUDNN(cudnnCreateTensorDescriptor(&tensor_output_pooling_err));
	checkCUDNN(cudnnSetTensor4dDescriptor( tensor_output_pooling_err,
    	cudnn_tensor_format,
        cudnn_dtype,
        minibatch_size,  //n
        num_filters,  //c
		1,  //h
		1 ));  //w


	//allocate the tensor for input embedding gradient
	checkCUDNN(cudnnCreateTensorDescriptor(&tensor_C_grad));
	checkCUDNN(cudnnSetTensor4dDescriptor( tensor_C_grad,
    	cudnn_tensor_format,
        cudnn_dtype,
        minibatch_size,  //n
        1,  //c
		longest_word,  //h
		char_emb_size ));  //w


	//allocate the tensor for filter gradients
	checkCUDNN(cudnnCreateFilterDescriptor(&filter_H_grad));
	checkCUDNN(cudnnSetFilter4dDescriptor( filter_H_grad,
        cudnn_dtype,
        num_filters, //k
        1, //c
        filter_size, //h
		char_emb_size ));  //w

	//allocate tensor for the bias
	checkCUDNN(cudnnCreateTensorDescriptor(&tensor_b_grad));
	checkCUDNN(cudnnSetTensor4dDescriptor( tensor_b_grad,
    	cudnn_tensor_format,
        cudnn_dtype,
        1,  //n
        num_filters,  //c
		1,  //h
		1 ));  //w


	//allocate the tensor for output of conv gradient
	checkCUDNN(cudnnCreateTensorDescriptor(&tensor_output_conv_err));
	checkCUDNN(cudnnSetTensor4dDescriptor( tensor_output_conv_err,
    	cudnn_tensor_format,
        cudnn_dtype,
        minibatch_size,  //n
        num_filters,  //c
		longest_word - filter_size + 1,  //h
		1 ));  //w



	//allocate the filters
	checkCUDNN(cudnnCreateFilterDescriptor(&filter_H));
	checkCUDNN(cudnnSetFilter4dDescriptor( filter_H,
        cudnn_dtype,
        num_filters, //k
        1, //c
        filter_size, //h
		char_emb_size ));  //w

	//get workspace size for conv forward
	checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize( cudnnHandle,
		tensor_C,
		filter_H,
		cudnn_conv_info,
		nodes[0]->tensor_output_conv,
		cudnn_conv_algo,
		&workspace_conv_forward_size));

	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_workspace_conv_forward,workspace_conv_forward_size),"GPU memory allocation failed\n");
	cudaDeviceSynchronize();
	std::cout << "Size of conv forward workspace: " << workspace_conv_forward_size << "\n";


	int temp_n = -1;
	int temp_c = -1;
	int temp_h = -1;
	int temp_w = -1;
	checkCUDNN(cudnnGetConvolution2dForwardOutputDim( cudnn_conv_info,
		tensor_C,
		filter_H,
		&temp_n,
		&temp_c,
		&temp_h,
		&temp_w));

	std::cout << "Printing convolution forward 2D output dimensions\n";
	std::cout << "n = " << temp_n << "\n";
	std::cout << "c = " << temp_c << "\n";
	std::cout << "h = " << temp_h << "\n";
	std::cout << "w = " << temp_w << "\n";
	std::cout << "Printing my dimensions\n";
	std::cout << "n = " << minibatch_size << "\n";
	std::cout << "c = " << num_filters << "\n";
	std::cout << "h = " << longest_word - filter_size + 1 << "\n";
	std::cout << "w = " << 1 << "\n";

	if(temp_n!=minibatch_size) {
		std::cout << "ERROR: incorrect sizes for conv forward\n";
		exit (EXIT_FAILURE);
	}
	if(temp_c!=num_filters) {
		std::cout << "ERROR: incorrect sizes for conv forward\n";
		exit (EXIT_FAILURE);
	}
	if(temp_h!= (longest_word - filter_size + 1) ) {
		std::cout << "ERROR: incorrect sizes for conv forward\n";
		exit (EXIT_FAILURE);
	}
	if(temp_w!=1) {
		std::cout << "ERROR: incorrect sizes for conv forward\n";
		exit (EXIT_FAILURE);
	}


	// set pooling descriptor
	checkCUDNN(cudnnCreatePoolingDescriptor(&cudnn_poolingDesc));
	checkCUDNN(cudnnSetPooling2dDescriptor( cudnn_poolingDesc,
		CUDNN_POOLING_MAX,
		longest_word - filter_size + 1,
		1,
		0,
		0,
		longest_word - filter_size + 1,
		1 ));


	//check the output dimension for pooling forward
	
	checkCUDNN(cudnnGetPooling2dForwardOutputDim( cudnn_poolingDesc,
		nodes[0]->tensor_output_conv,
		&temp_n,
		&temp_c,
		&temp_h,
		&temp_w));


	std::cout << "Printing convolution forward 2D output dimensions\n";
	std::cout << "n = " << temp_n << "\n";
	std::cout << "c = " << temp_c << "\n";
	std::cout << "h = " << temp_h << "\n";
	std::cout << "w = " << temp_w << "\n";
	std::cout << "Printing my dimensions\n";
	std::cout << "n = " << minibatch_size << "\n";
	std::cout << "c = " << num_filters << "\n";
	std::cout << "h = " << 1 << "\n";
	std::cout << "w = " << 1 << "\n";

	if(temp_n!=minibatch_size) {
		std::cout << "ERROR: incorrect sizes for pool forward\n";
		exit (EXIT_FAILURE);
	}
	if(temp_c!=num_filters) {
		std::cout << "ERROR: incorrect sizes for pool forward\n";
		exit (EXIT_FAILURE);
	}
	if(temp_h!=1) {
		std::cout << "ERROR: incorrect sizes for pool forward\n";
		exit (EXIT_FAILURE);
	}
	if(temp_w!=1) {
		std::cout << "ERROR: incorrect sizes for pool forward\n";
		exit (EXIT_FAILURE);
	}

	//----------------------------- get workspaces for convolution backward -----------------------------

	//get workspace for conv backward data
	checkCUDNN(cudnnGetConvolutionBackwardDataWorkspaceSize( cudnnHandle,
	    filter_H,
	    tensor_output_conv_err,
		cudnn_conv_info,
	    tensor_C_grad,
		cudnn_conv_back_data_algo,
		&workspace_conv_backward_data_size));
	std::cout << "workspace size for gradient of convolution backward data " << workspace_conv_backward_data_size << "\n";
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_workspace_conv_backward_data,workspace_conv_backward_data_size),"GPU memory allocation failed\n");


	//get workspace for conv backward filter
	checkCUDNN(cudnnGetConvolutionBackwardFilterWorkspaceSize( cudnnHandle,
		tensor_C,
	    tensor_output_conv_err,
		cudnn_conv_info,
		filter_H_grad,
	    cudnn_conv_back_filter_algo,
	  	&workspace_conv_backward_filter_size ));
	std::cout << "workspace size for gradient of convolution backward filter " << workspace_conv_backward_filter_size << "\n";
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_workspace_conv_backward_filter,workspace_conv_backward_filter_size),"GPU memory allocation failed\n");


	clear_gradients();
}	


//cuda kernels
template<typename dType>
__global__
void tanh_kernel(dType *d_final,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_final[i] = cuda_tanh_wrapper(d_final[i]);
	}
}

//cuda kernels
template<typename dType>
__global__
void tanh_error_kernel(dType *d_final_error,dType *d_tanh_val,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_final_error[i] = d_final_error[i]*(1 - d_tanh_val[i]*d_tanh_val[i]);
	}
}


__device__
inline int tensor_index(int n,int c,int h,int w,int n_dim,int c_dim,int h_dim,int w_dim) {
	return w + (h * w_dim) + (c * w_dim * h_dim) + (n * w_dim * h_dim * c_dim);
}


/*
	-vocab indicies are in the format of word len, word len, word len, 
	-size is word_len*minibatch_size
	-the -1 is the NULL character that sets the embedding to zero
*/
template<typename dType>
__global__
void load_char_emb_kernel(dType *d_C,dType *d_Q,int *d_vocab_indicies,int char_emb_size,int minibatch_size,int longest_word) {

	int i_start = threadIdx.x;
	int i_end = char_emb_size*longest_word;
	int i_step = blockDim.x;

	for(int minibatch_index = blockIdx.x; minibatch_index < minibatch_size; minibatch_index+=gridDim.x) {

		for(int i=i_start; i<i_end; i+=i_step) {
			int char_emb_index = i%char_emb_size;
			int curr_word_pos = i/char_emb_size;
			int word_index = d_vocab_indicies[IDX2C(curr_word_pos,minibatch_index,longest_word)];

			if(word_index==-1) {
				d_C[tensor_index(minibatch_index,0,curr_word_pos,char_emb_index,minibatch_size,1,longest_word,char_emb_size)] = 0;
			}
			else {
				d_C[tensor_index(minibatch_index,0,curr_word_pos,char_emb_index,minibatch_size,1,longest_word,char_emb_size)] = \
					d_Q[IDX2C(char_emb_index,word_index,char_emb_size)];
			}
		}
	}
}


template<typename dType>
__global__
void update_char_embeddings(dType *d_C_err,dType *d_Q_grad,int *d_vocab_indicies,int char_emb_size,int minibatch_size,int longest_word) {

	int i_start = threadIdx.x;
	int i_end = char_emb_size*longest_word;
	int i_step = blockDim.x;

	for(int minibatch_index = blockIdx.x; minibatch_index < minibatch_size; minibatch_index+=gridDim.x) {
		for(int i=i_start; i<i_end; i+=i_step) {
			int char_emb_index = i%char_emb_size;
			int curr_word_pos = i/char_emb_size;
			int word_index = d_vocab_indicies[IDX2C(curr_word_pos,minibatch_index,longest_word)];

			if(word_index!=-1) {
				dType temp_val = d_C_err[tensor_index(minibatch_index,0,curr_word_pos,char_emb_index,minibatch_size,1,longest_word,char_emb_size)]; 
				atomicAdd(&(d_Q_grad[IDX2C(char_emb_index,word_index,char_emb_size)]),(temp_val));
			}
		}
	}
}

/*
	------------------ Inputs to function -----------------
	length = length of sentences/words (zero padded to be of same length)
	input_size = embedding_size
	feature_maps = table of feature maps (for each kernel width)
	kernels = table of kernel widths
	num_kernels = number of kernel widths


*/
template<typename dType>
dType conv_char_layer<dType>::forward(int index) {

	//load in the correct character embeddings
	load_char_emb_kernel<<<256,256>>>(d_C,d_Q,d_vocab_indicies,char_emb_size,minibatch_size,longest_word);
	cudaDeviceSynchronize();

	//run the convolution
	//alpha = 1
	//beta = 0
	dType alpha =1;
	dType beta = 0;
	checkCUDNN(cudnnConvolutionForward( cudnnHandle,
		&alpha,
		tensor_C,
		d_C,
		filter_H,
		d_H,
		cudnn_conv_info,
		cudnn_conv_algo,
		d_workspace_conv_forward,
		workspace_conv_forward_size,
		&beta,
		nodes[0]->tensor_output_conv,
		nodes[0]->d_output_conv ));


	//add in bias
	alpha = 1;
	beta = 1;
	checkCUDNN(cudnnAddTensor( cudnnHandle,
		&alpha,
		tensor_b,
		d_b,
		&beta,
		nodes[0]->tensor_output_conv,
		nodes[0]->d_output_conv ));

	cudaDeviceSynchronize();
	//send through tanh
	tanh_kernel<<<256,256>>>(nodes[0]->d_output_conv,minibatch_size*num_filters*(longest_word - filter_size + 1));
	cudaDeviceSynchronize();

	//run pooling forward
	alpha = 1;
	beta = 0;
	checkCUDNN(cudnnPoolingForward( cudnnHandle,
		cudnn_poolingDesc,
		&alpha,
		nodes[0]->tensor_output_conv, 
		nodes[0]->d_output_conv,
		&beta,
		nodes[0]->tensor_output_pooling,
		nodes[0]->d_output_pooling ));

	cudaDeviceSynchronize();
	//now send this to LSTM or highway network
	//cudaMemcpy(highway_layers[0]->nodes[0]->d_y,nodes[0]->d_output_pooling,num_filters*minibatch_size*sizeof(dType), cudaMemcpyDefault);
	cudaDeviceSynchronize();
	dType loss = 0;
	//call highway networks here
	for(int i=0; i<highway_layers.size(); i++) {
		dType *d_y_temp;
		if(i==0) {
			d_y_temp = nodes[0]->d_output_pooling;
		}
		else {
			d_y_temp = highway_layers[i-1]->nodes[0]->d_z;
		}
		loss = highway_layers[i]->forward(0,d_y_temp);
	}
	return loss;
}


template<typename dType>
void conv_char_layer<dType>::backward(int index) {

	/*	
		- compute gradients for highway networks
		- compute gradient from max pooling
		- compute gradients for filters
		- compute gradients for convolution bias
		- compute gradients with respect to embeddings
	*/

	//call highway networks here
	for(int i=highway_layers.size()-1; i>=0 ; i--) {
		dType *d_Err_z_temp=NULL;
		if(i != (highway_layers.size()-1)) {
			d_Err_z_temp = highway_layers[i+1]->d_Err_y;
		}
		highway_layers[i]->backward(0,d_Err_z_temp);
	}

	cudaDeviceSynchronize();	
	cudaMemcpy(d_output_pooling_err,highway_layers[0]->d_Err_y,num_filters*minibatch_size*sizeof(dType), cudaMemcpyDefault);
	cudaDeviceSynchronize();

	//run pooling backward
	dType alpha = 1;
	dType beta = 0;
	checkCUDNN(cudnnPoolingBackward( cudnnHandle,
		cudnn_poolingDesc,
		&alpha,
		nodes[0]->tensor_output_pooling,
		nodes[0]->d_output_pooling,
		tensor_output_pooling_err,
		d_output_pooling_err,
		nodes[0]->tensor_output_conv,
		nodes[0]->d_output_conv,
		&beta,
		tensor_output_conv_err,
		d_output_conv_err ));


	cudaDeviceSynchronize();
	//run error through tanh backward
	tanh_error_kernel<<<256,256>>>(d_output_conv_err,nodes[0]->d_output_conv,minibatch_size*num_filters*(longest_word - filter_size + 1));

	cudaDeviceSynchronize();

	//run conv filter backward
	alpha = 1;
	beta = 1;
	checkCUDNN(cudnnConvolutionBackwardFilter(cudnnHandle,
		&alpha,
		tensor_C,
		d_C,
		tensor_output_conv_err,
		d_output_conv_err,
		cudnn_conv_info,
		cudnn_conv_back_filter_algo,
		d_workspace_conv_backward_filter,
		workspace_conv_backward_filter_size,
		&beta,
		filter_H_grad,
		d_H_grad ));


	//run conv data backward
	alpha = 1;
	beta = 0;
	checkCUDNN(cudnnConvolutionBackwardData(cudnnHandle,
		&alpha,
		filter_H,
		d_H,
		tensor_output_conv_err,
		d_output_conv_err,
		cudnn_conv_info,
		cudnn_conv_back_data_algo,
		d_workspace_conv_backward_data,
		workspace_conv_backward_data_size,
		&beta,
		tensor_C_grad,
		d_C_err ));


	//run conv bias backward
	alpha = 1;
	beta = 1;
	checkCUDNN(cudnnConvolutionBackwardBias( cudnnHandle,
		&alpha,
		tensor_output_conv_err,
		d_output_conv_err,
		&beta,
		tensor_b_grad,
		d_b_grad));

	//now send gradients to correct character embedding
	cudaDeviceSynchronize();
	update_char_embeddings<<<256,256>>>(d_C_err,d_Q_grad,d_vocab_indicies,char_emb_size,minibatch_size,longest_word);
}



#endif