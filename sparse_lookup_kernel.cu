//Kernel for the sparse lookups

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

//W is the embedding matrix
//d_lookup is the (hidden state size)x(minibatch size) matrix
//Note that W is stored in column major
//Row size and col size
__global__ 
void sparse_lookup(float *d_lookup, float *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size,int vocab_size)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	d_lookup[IDX2C(threadIdx.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
}

//1024 threads per block with the number of blocks being the minibatch size
__global__ 
void sparse_lookup_1(float *d_lookup, float *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size,int vocab_size)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if(threadIdx.x < hiddenstate_size)
		d_lookup[IDX2C(threadIdx.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
}

//Grid is (size of minibatch x 8) since each block is 128x1 responsible for doing 1/4th of the transfer of a column
__global__ 
void sparse_lookup_2(float *d_lookup, float *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size,int vocab_size)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if(threadIdx.x+blockIdx.y*blockDim.x < hiddenstate_size)
		d_lookup[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
}


//Grid is (size of minibatch x 2) since each block is 128x1 responsible for doing 1/4th of the transfer of a column
__global__ 
void sparse_lookup_3(float *d_lookup, float *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size,int vocab_size)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if(threadIdx.x+blockIdx.y*blockDim.x < hiddenstate_size)
		d_lookup[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
}

//Grid is (size of minibatch x 2) since each block is 128x1 responsible for doing 1/4th of the transfer of a column
__global__ 
void sparse_lookup_4(float *d_lookup, float *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size,int vocab_size)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if(threadIdx.x+blockIdx.y*blockDim.x < hiddenstate_size)
		d_lookup[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x+blockIdx.y*blockDim.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
}


//This is eigen code for doing sparse lookups
template<typename Derived,typename Derived2>
void compute_temp_mat(const Eigen::MatrixBase<Derived> &W_mat,const Eigen::MatrixBase<Derived> &h_lookup_const,
	const Eigen::MatrixBase<Derived2> &vocab_indices_input) 
{
	UNCONST(Derived,h_lookup_const,h_lookup);
	for(int i=0; i<vocab_indices_input.rows(); i++) {
		h_lookup.col(i) = W_mat.col(vocab_indices_input(i));
	}
}




int main() {
	const int minibatch_size = 128;
	const int hiddenstate_size = 1000;
	const int vocab_size = 160000;
	const int num_trials =10;

	int *h_vocab_indices;
	int *d_vocab_indices;

	float *h_W;
	float *d_W;

	float *h_lookup;
	float *d_lookup;

	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_lookup(hiddenstate_size,minibatch_size);
	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_W(hiddenstate_size,vocab_size);
	Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> h_eigen_vocab_indices(minibatch_size,1);

	//initialize cuBLAS
	std::cout << "\n";
	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");
	
	full_vector_setup_vocab(&h_vocab_indices,&d_vocab_indices,minibatch_size,vocab_size);
	full_matrix_setup(&h_W,&d_W,hiddenstate_size,vocab_size);
	full_matrix_setup(&h_lookup,&d_lookup,hiddenstate_size,minibatch_size);

	//Copy the matrices to eigen
	copy_to_eigen(h_eigen_lookup,h_lookup);
	copy_to_eigen(h_eigen_vocab_indices,h_vocab_indices);
	copy_to_eigen(h_eigen_W,h_W);


	for(int i=0; i<num_trials; i++) {
		sparse_lookup<<<minibatch_size,hiddenstate_size>>>(d_lookup,d_W,d_vocab_indices,minibatch_size,hiddenstate_size,vocab_size);
		CUDA_GET_LAST_ERROR();
	}

	for(int i=0; i<num_trials; i++) {
		sparse_lookup_1<<<minibatch_size,1024>>>(d_lookup,d_W,d_vocab_indices,minibatch_size,hiddenstate_size,vocab_size);
		CUDA_GET_LAST_ERROR();
	}

	for(int i=0; i<num_trials; i++) {
		const dim3 kernel2(minibatch_size,8,1);
		sparse_lookup_2<<<kernel2,128>>>(d_lookup,d_W,d_vocab_indices,minibatch_size,hiddenstate_size,vocab_size);
		CUDA_GET_LAST_ERROR();
	}

	for(int i=0; i<num_trials; i++) {
		const dim3 kernel3(minibatch_size,4,1);
		sparse_lookup_3<<<kernel3,256>>>(d_lookup,d_W,d_vocab_indices,minibatch_size,hiddenstate_size,vocab_size);
		CUDA_GET_LAST_ERROR();
	}

	for(int i=0; i<num_trials; i++) {
		const dim3 kernel4(minibatch_size,2,1);
		sparse_lookup_4<<<kernel4,512>>>(d_lookup,d_W,d_vocab_indices,minibatch_size,hiddenstate_size,vocab_size);
		CUDA_GET_LAST_ERROR();
	}

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	//Eigen code timing
	for(int i=0; i<num_trials; i++) {
		compute_temp_mat(h_eigen_W,h_eigen_lookup,h_eigen_vocab_indices);
	}
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of Eigen: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	get_matrix_cuBLAS(h_lookup,d_lookup,hiddenstate_size,minibatch_size);

	if(eigen_check(h_eigen_lookup,h_lookup)) {
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}
}


