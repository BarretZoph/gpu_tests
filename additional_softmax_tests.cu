//Softmax forward distribution


#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"


#define TILE_DIM 16

__global__ void MatMul(float* A, float* B, float* C, int ARows, int ACols, int BRows, int BCols, int CRows, int CCols) {

    float CValue = 0;

    int Row = blockIdx.y*TILE_DIM + threadIdx.y;
    int Col = blockIdx.x*TILE_DIM + threadIdx.x;

    __shared__ float As[TILE_DIM][TILE_DIM];
    __shared__ float Bs[TILE_DIM][TILE_DIM];

    for (int k = 0; k < (TILE_DIM + ACols - 1)/TILE_DIM; k++) {

         if (k*TILE_DIM + threadIdx.x < ACols && Row < ARows)   As[threadIdx.y][threadIdx.x] = A[Row*ACols + k*TILE_DIM + threadIdx.x];
         else                                                   As[threadIdx.y][threadIdx.x] = 0.0;

         if (k*TILE_DIM + threadIdx.y < BRows && Col < BCols)   Bs[threadIdx.y][threadIdx.x] = B[(k*TILE_DIM + threadIdx.y)*BCols + Col];
         else                                                   Bs[threadIdx.y][threadIdx.x] = 0.0;

         __syncthreads();

         for (int n = 0; n < TILE_DIM; ++n) CValue += As[threadIdx.y][n] * Bs[n][threadIdx.x];

         __syncthreads();
    }

    if (Row < CRows && Col < CCols) C[((blockIdx.y * blockDim.y + threadIdx.y)*CCols)+(blockIdx.x*blockDim.x)+threadIdx.x]= -CValue;
}

int main() {

	const int output_vocab_size = 20000;
	const int hiddenstate_size = 1000;
	const int minibatch_size = 128;
	const int num_trials = 100;

	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");


	float *h_D_normal;
	float *h_D_transpose;
	float *h_output_dist_normal;
	float *h_output_dist_transpose;
	float *h_Err_normal;
	float *h_Err_normal_2;
	float *h_Err_transpose;
	float *h_D_grad_normal;
	float *h_D_grad_transpose;
	float *h_h_t_normal;
	float *h_h_t_transpose;

	float *d_D_normal;
	float *d_D_transpose;
	float *d_output_dist_normal;
	float *d_output_dist_transpose;
	float *d_Err_normal;
	float *d_Err_normal_2;
	float *d_Err_transpose;
	float *d_D_grad_normal;
	float *d_D_grad_transpose;
	float *d_h_t_normal;
	float *d_h_t_transpose;


	full_matrix_setup(&h_D_normal,&d_D_normal,output_vocab_size,hiddenstate_size);
	full_matrix_setup(&h_D_transpose,&d_D_transpose,hiddenstate_size,output_vocab_size);
	full_matrix_setup(&h_output_dist_normal,&d_output_dist_normal,output_vocab_size,minibatch_size);
	full_matrix_setup(&h_output_dist_transpose,&d_output_dist_transpose,minibatch_size,output_vocab_size);
	full_matrix_setup(&h_Err_normal,&d_Err_normal,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_Err_normal_2,&d_Err_normal_2,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_Err_transpose,&d_Err_transpose,minibatch_size,hiddenstate_size);
	full_matrix_setup(&h_D_grad_normal,&d_D_grad_normal,output_vocab_size,hiddenstate_size);
	full_matrix_setup(&h_D_grad_transpose,&d_D_grad_transpose,hiddenstate_size,output_vocab_size);
	full_matrix_setup(&h_h_t_normal,&d_h_t_normal,hiddenstate_size,minibatch_size);
	full_matrix_setup(&h_h_t_transpose,&d_h_t_transpose,minibatch_size,hiddenstate_size);

	cudaDeviceSynchronize();
	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	std::chrono::duration<double> elapsed_seconds;

	//test 1
	//-D^T * outputdist
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		float alpha = -1;
		float beta = 0;
		CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_T,CUBLAS_OP_N,hiddenstate_size,minibatch_size,output_vocab_size,
			&alpha,d_D_normal,output_vocab_size,d_output_dist_normal,output_vocab_size,&beta,d_Err_normal,hiddenstate_size),"cuBLAS h_t gradient failed");
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
	std::cout << "Average Runtime of test 1 GPU: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	//test2
	//D*outputdist (D is stored as transpose)
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		float alpha = -1;
		float beta = 0;
		CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,hiddenstate_size,minibatch_size,output_vocab_size,
			&alpha,d_D_transpose,hiddenstate_size,d_output_dist_normal,output_vocab_size,&beta,d_Err_normal,hiddenstate_size),"cuBLAS h_t gradient failed");
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
	std::cout << "Average Runtime of test 2 GPU: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	//test3
	//outputdist*D (outputdist is stored as transpose)
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		float alpha = -1;
		float beta = 0;
		CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_N,minibatch_size,hiddenstate_size,output_vocab_size,
			&alpha,d_output_dist_transpose,minibatch_size,d_D_normal,output_vocab_size,&beta,d_Err_transpose,minibatch_size),"cuBLAS h_t gradient failed");
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
	std::cout << "Average Runtime of test 3 GPU: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	//test4
	//D*outputdist (D is stored as transpose), but custom made matrix multiplication
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		dim3 block(TILE_DIM,TILE_DIM);
		dim3 grid(minibatch_size / TILE_DIM + 1, hiddenstate_size / TILE_DIM + 1);
		MatMul<<<grid,block>>>(d_D_transpose, d_output_dist_normal, d_Err_normal_2, hiddenstate_size, output_vocab_size, output_vocab_size, minibatch_size, hiddenstate_size, minibatch_size);
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
	std::cout << "Average Runtime of test 4 GPU: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	get_matrix_cuBLAS(h_Err_normal,d_Err_normal,hiddenstate_size,minibatch_size);
	get_matrix_cuBLAS(h_Err_normal_2,d_Err_normal_2,hiddenstate_size,minibatch_size);

	// int counter=0;
	// for(int i=0; i<hiddenstate_size; i++) {
	// 	for(int j=0; j<minibatch_size; j++) {
	// 		if(h_Err_normal[IDX2C(i,j,hiddenstate_size)] != h_Err_normal_2[IDX2C(i,j,hiddenstate_size)]) {
	// 			counter++;
	// 			std::cout << "Not equal, the difference is: " << h_Err_normal[IDX2C(i,j,hiddenstate_size)]- h_Err_normal_2[IDX2C(i,j,hiddenstate_size)] << "\n";
	// 		}
	// 	}
	// }
	// std::cout << "Total wrong " << counter << "  out of " << hiddenstate_size*minibatch_size << "\n";

	//magma stuff

}