#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

//thrust streams
//#include <thrust/execution_policy.h>
#include <thrust/system/cuda/execution_policy.h>

struct scale_functor {
	const int minibatch_size;

	scale_functor(int _minibatch_size) : minibatch_size(_minibatch_size) {}

	__host__ __device__ void operator()(float &x) {
		x = (1.0f/minibatch_size)*x;
	}
	__host__ __device__ void operator()(double &x) {
		x = (1.0/minibatch_size)*x;
	}
};

__global__
void custom_kernel(float *d_mat,int size,int minibatch_size) {
	for (int i = blockIdx.x * blockDim.x + threadIdx.x; 
         i < size; 
         i += blockDim.x * gridDim.x) 
	{
		d_mat[i] = (1.0f/minibatch_size)*d_mat[i];
	}
}


int main() { 

	cudaStream_t s1,s2,s3;
	const int rows = 1000;
	const int cols = 1000;
	const int num_trials = 100;
	const int minibatch_size = 128;

	cudaStreamCreate(&s1);
	cudaStreamCreate(&s2);
	cudaStreamCreate(&s3);

	thrust::device_vector<float> thrust_d_1(rows * cols,1.2);
	thrust::device_vector<float> thrust_d_2(rows * cols,2.2);
	thrust::device_vector<float> thrust_d_3(rows * cols,3.2);

	float *d_mat1 = thrust::raw_pointer_cast(&thrust_d_1[0]);
	float *d_mat2 = thrust::raw_pointer_cast(&thrust_d_2[0]);
	float *d_mat3 = thrust::raw_pointer_cast(&thrust_d_3[0]);

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	std::chrono::duration<double> elapsed_seconds;

	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		scale_functor unary_op(minibatch_size);
		thrust::for_each(thrust_d_1.begin(),thrust_d_1.end(),unary_op);
		thrust::for_each(thrust_d_2.begin(),thrust_d_2.end(),unary_op);
		//thrust::for_each(thrust_d_3.begin(),thrust_d_3.end(),unary_op);
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test1: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


    start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		scale_functor unary_op(minibatch_size);
		scale_functor unary_op2(minibatch_size);
		scale_functor unary_op3(minibatch_size);
		thrust::for_each(thrust::cuda::par.on(s1),thrust_d_1.begin(),thrust_d_1.end(),unary_op);
		thrust::for_each(thrust::cuda::par.on(s2),thrust_d_2.begin(),thrust_d_2.end(),unary_op2);
		//thrust::for_each(thrust::cuda::par.on(s3),thrust_d_3.begin(),thrust_d_3.end(),unary_op3);

	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test2: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


     start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		custom_kernel<<<13,256,0,s1>>>(d_mat1,rows*cols,minibatch_size);
		custom_kernel<<<13,256,0,s2>>>(d_mat1,rows*cols,minibatch_size);
		//custom_kernel<<<13,256,0,s3>>>(d_mat1,rows*cols,minibatch_size);
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test3: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


    std::cout << "Thrust version: " << THRUST_VERSION << "\n";

    cudaSetDevice(0);
    thrust::device_vector<float> thrust_d_1_GPU1(rows * cols,1.2);
    cudaSetDevice(1);
	thrust::device_vector<float> thrust_d_2_GPU2(rows * cols,2.2);


	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		scale_functor unary_op(minibatch_size);
		scale_functor unary_op2(minibatch_size);
		cudaSetDevice(0);
		thrust::for_each(thrust_d_1_GPU1.begin(),thrust_d_1_GPU1.end(),unary_op);
		cudaSetDevice(1);
		thrust::for_each(thrust_d_2_GPU2.begin(),thrust_d_2_GPU2.end(),unary_op2);

	}
	cudaSetDevice(0);
	cudaDeviceSynchronize();
	cudaSetDevice(1);
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test4: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


}

