//code for convolutional character model
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>
#include <cudnn.h>

#include "math_constants.h"
#include <Eigen/Dense>
#include "BZ_CUDA_UTIL.h"

/*
	Error checking DEFINE states for cuDNN
	code is from NVIDIA example
*/

#define FatalError(s) {                                                \
    std::stringstream _where, _message;                                \
    _where << __FILE__ << ':' << __LINE__;                             \
    _message << std::string(s) + "\n" << __FILE__ << ':' << __LINE__;\
    std::cerr << _message.str() << "\nAborting...\n";                  \
    cudaDeviceReset();                                                 \
    exit(EXIT_FAILURE);                                                \
}


#define checkCUDNN(status) {                                           \
    std::stringstream _error;                                          \
    if (status != CUDNN_STATUS_SUCCESS) {                              \
      _error << "CUDNN failure\nError: " << cudnnGetErrorString(status); \
      FatalError(_error.str());                                        \
    }                                                                  \
}

#include "conv_char.h"

typedef double precision;

/*
	General Notes
	- need a max character length variable
	- append <start> and <end> character symbols to each word
	
	----------------- Parameters being used ---------------

	Q (embedding size x number of unique characters)
	C (LSTM size x longest word x minibatch size) this gets filled with zeros for not present characters

	----------------- Stages of algorithm -----------------

	1. A each timestep fill C with correct embeddings
	2. Run convolution forward
	3. Run Max Pooling
	4. Send this through highway networks
*/


template<typename dType>
void check_gradient_GPU(dType *d_mat,dType *d_grad,int rows,int cols,conv_char_layer<dType> *layer) {
	dType epsilon = 1e-5;
	cudaDeviceSynchronize();
	thrust::device_ptr<dType> d_thrust_mat = thrust::device_pointer_cast(d_mat);
	thrust::device_ptr<dType> d_thrust_grad = thrust::device_pointer_cast(d_grad);
	for(int i=0; i<rows; i++) {
		for(int j=0; j<cols; j++) {
			dType loss =0;
			d_thrust_mat[IDX2C(i,j,rows)]+= epsilon;
			loss = layer->forward(0);
			cudaDeviceSynchronize();
			d_thrust_mat[IDX2C(i,j,rows)]+= -2*epsilon;
			loss -=layer->forward(0);
			cudaDeviceSynchronize();
			d_thrust_mat[IDX2C(i,j,rows)]+= epsilon;
			std::cout << "Gradient difference: " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon)) << "     my gradient: " << d_thrust_grad[IDX2C(i,j,rows)] << "\n";
			if( (std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))) > 1/(dType)1000.0 ||  (std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))/(std::abs(d_thrust_grad[IDX2C(i,j,rows)]) + std::abs(loss/(2*epsilon)))) > 1/1000.0  ) {
				std::cout << "Row: " << i << "  Col: " << j << "\n";
				std::cout << "Gradient for gradient check: " << loss/(2*epsilon) << "\n";
				std::cout << "My gradient: " << d_thrust_grad[IDX2C(i,j,rows)] << "\n";
				std::cout << "Gradient difference: " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon)) << "\n";
				std::cout << "Gradient difference (Equation 2): " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))/(std::abs(d_thrust_grad[IDX2C(i,j,rows)]) + std::abs(loss/(2*epsilon)) ) << "\n\n";
			}
		}
	}
}

int main() {
		

	// const int num_trials = 500;
	// std::chrono::time_point<std::chrono::system_clock> start_total,end_total;

	// //time operations
	// cudaDeviceSynchronize();
	// start_total = std::chrono::system_clock::now();
	// for(int i=0; i<num_trials; i++) {
	// 	ccl->forward();
	// 	ccl->backward();
	// }
	// cudaDeviceSynchronize();
	// end_total = std::chrono::system_clock::now();
	// std::chrono::duration<double> elapsed_seconds = end_total-start_total;
 //    std::cout << "\n\n\n";
 //    std::cout << "Average Runtime: " << (elapsed_seconds.count())/num_trials << "\n";


	//------------------------------- Gradient checking for highway network --------------------------

	// int state_size = 5;
	// int minibatch_size = 3;
	// int longest_sent = 1;
	// highway_network_layer<precision> *hnl = new highway_network_layer<precision>();
	// hnl->init(state_size,minibatch_size,longest_sent);
	// hnl->forward(0);
	// hnl->backward(0);
	// cudaDeviceSynchronize();
	// CUDA_GET_LAST_ERROR("Finished forward and backprop");

	// std::cout << "Gradient checking d_W_h\n";
	// check_gradient_GPU(hnl->d_W_h,hnl->d_W_h_grad,state_size,state_size,hnl);

	// std::cout << "Gradient checking d_W_t\n";
	// check_gradient_GPU(hnl->d_W_t,hnl->d_W_t_grad,state_size,state_size,hnl);

	// std::cout << "Gradient checking d_b_h\n";
	// check_gradient_GPU(hnl->d_b_h,hnl->d_b_h_grad,state_size,1,hnl);

	// std::cout << "Gradient checking d_b_t\n";
	// check_gradient_GPU(hnl->d_b_t,hnl->d_b_t_grad,state_size,1,hnl);

	// std::cout << "Gradient checking y\n";
	// check_gradient_GPU(hnl->nodes[0]->d_y,hnl->d_Err_y,state_size,minibatch_size,hnl);



	//------------------------------- Gradient check for CharCNN ------------------------------

	conv_char_layer<precision> *ccl = new conv_char_layer<precision>();
	ccl->init();
	ccl->forward(0);
	ccl->backward(0);
	cudaDeviceSynchronize();
	CUDA_GET_LAST_ERROR("Finished forward and backprop");


	std::cout << "----------- Gradient checking filter ----------- \n";
	check_gradient_GPU(ccl->d_H,ccl->d_H_grad,(ccl->num_filters)*(ccl->filter_size)*(ccl->char_emb_size),1,ccl);

	std::cout << "----------- Gradient checking Q ----------- \n";
	check_gradient_GPU(ccl->d_Q,ccl->d_Q_grad,(ccl->num_unique_chars)*(ccl->char_emb_size),1,ccl);

	std::cout << "----------- Gradient checking b ----------- \n";
	check_gradient_GPU(ccl->d_b,ccl->d_b_grad,(ccl->num_filters),1,ccl);

	std::cout << "---------Gradient checking highway 1--------------\n";
	std::cout << "Gradient checking d_W_h\n";
	check_gradient_GPU(ccl->highway_layers[0]->d_W_h,ccl->highway_layers[0]->d_W_h_grad,ccl->highway_layers[0]->state_size,ccl->highway_layers[0]->state_size,ccl);

	std::cout << "Gradient checking d_W_t\n";
	check_gradient_GPU(ccl->highway_layers[0]->d_W_t,ccl->highway_layers[0]->d_W_t_grad,ccl->highway_layers[0]->state_size,ccl->highway_layers[0]->state_size,ccl);

	std::cout << "Gradient checking d_b_h\n";
	check_gradient_GPU(ccl->highway_layers[0]->d_b_h,ccl->highway_layers[0]->d_b_h_grad,ccl->highway_layers[0]->state_size,1,ccl);

	std::cout << "Gradient checking d_b_t\n";
	check_gradient_GPU(ccl->highway_layers[0]->d_b_t,ccl->highway_layers[0]->d_b_t_grad,ccl->highway_layers[0]->state_size,1,ccl);


	std::cout << "---------Gradient checking highway 4--------------\n";
	std::cout << "Gradient checking d_W_h\n";
	check_gradient_GPU(ccl->highway_layers[3]->d_W_h,ccl->highway_layers[3]->d_W_h_grad,ccl->highway_layers[3]->state_size,ccl->highway_layers[3]->state_size,ccl);

	std::cout << "Gradient checking d_W_t\n";
	check_gradient_GPU(ccl->highway_layers[3]->d_W_t,ccl->highway_layers[3]->d_W_t_grad,ccl->highway_layers[3]->state_size,ccl->highway_layers[3]->state_size,ccl);

	std::cout << "Gradient checking d_b_h\n";
	check_gradient_GPU(ccl->highway_layers[3]->d_b_h,ccl->highway_layers[3]->d_b_h_grad,ccl->highway_layers[3]->state_size,1,ccl);

	std::cout << "Gradient checking d_b_t\n";
	check_gradient_GPU(ccl->highway_layers[3]->d_b_t,ccl->highway_layers[3]->d_b_t_grad,ccl->highway_layers[3]->state_size,1,ccl);


}	






