#include <iostream>
#include <stdio.h>
#include "cublas_v2.h"

//This is used since all cuBLAS storage is column major
#define IDX2C(i,j,ld) (((j)*(ld))+(i))

int main() {
	int deviceCount; 
	cudaGetDeviceCount(&deviceCount);
	for (int device = 0; device < deviceCount; ++device) {
		cudaDeviceProp deviceProp; 
		cudaGetDeviceProperties(&deviceProp, device); 
		printf("Device %d has compute capability %d.%d.\n", device, deviceProp.major, deviceProp.minor); 
	}

}