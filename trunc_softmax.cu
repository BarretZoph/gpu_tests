#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>

#include "math_constants.h"

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

#define SOFTMAX_THREADS 256
#define NORM_THREADS 256


template<typename dType>
__global__
void load_in_embeddings_trunc(dType *d_temp_embeddings,dType *d_D,int *d_samples,int shortlist_size,int sampled_size,int LSTM_size) {

	int i_start = threadIdx.x; //start at the thread index
	int i_end = LSTM_size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step

	for(int k = blockIdx.x; k < shortlist_size + sampled_size; k+=gridDim.x) {
		int vocab_index;
		if(k < shortlist_size) {
			vocab_index = k;
		}
		else {
			vocab_index = d_samples[k];
		}
		for(int i= i_start; i < i_end; i += i_step) {
			d_temp_embeddings[IDX2C(i,k,LSTM_size)] = d_D[IDX2C(i,vocab_index,LSTM_size)];
		}
	}
}

//scales before normalization stage
//call in place of overflow kernel
template<typename dType>
__global__
void outputdist_truncated_kernel(dType *output, dType *input, int dim,dType sample_rate,int shortlist_size_plus) {
	__shared__ dType buffer[SOFTMAX_THREADS]; //shared memory for the block, this must be the number of threads per block in size
	int k = blockIdx.x; //get the block index
	dType *input_k = input + k*dim; //all threads in block start from same index
	dType *output_k = output + k*dim; //again all threads in block start from same index

	int i_start = threadIdx.x; //start at the thread index
	int i_end = dim; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step
	const int tid = threadIdx.x;

	//get the max element for each thread's assigned locations and put them in the buffer
	//dim elements are covered in this reduction
	//buffer[threadIdx.x] = -FLT_MAX;
	buffer[threadIdx.x] = -FLT_MAX;
	for(int i=i_start; i<i_end; i+=i_step) {
		dType z = input_k[i];
		if(buffer[threadIdx.x] < z) {
			buffer[threadIdx.x] = z;
		}
	}

	 __syncthreads();

	 // reduce
	 //first thread goes through and finds the max element in the buffer
	 //after this stage the max element for dim items is found
	for(int stride=SOFTMAX_THREADS/2; stride>0/*32*/; stride>>=1) {
		if(tid < stride) {
			buffer[tid] = (buffer[tid] > buffer[stride + tid]) ? buffer[tid] : buffer[stride + tid];
		}
		__syncthreads();
	}

	__syncthreads();

	// sum
	//Now go through all the dim elements and subtract the max from the element, keep a running sum for the normalization constant
	dType max_k = buffer[0];
	__syncthreads(); //THIS MUST BE HERE
	buffer[threadIdx.x] = 0;
	for (int i=i_start; i<i_end; i+=i_step) {
		//dType z = exp(input_k[i]-max_k); //subtract the max from the input, then exp it for the softmax
		dType z;
		if(i>=shortlist_size_plus) {
			z = sample_rate*cuda_exp_wrapper(input_k[i]-max_k);
		}
		else
		{	
			z = cuda_exp_wrapper(input_k[i]-max_k);
		}
		buffer[threadIdx.x] += z; //keep a running sum of these values for the normalization constant
		output_k[i] = z; //set the output as this value, then get ready to divide by the sum again
	}

 	__syncthreads();

 	// reduce
 	//Now sum all the elements in the buffer, for the normalization constant
 	for(int stride=SOFTMAX_THREADS/2; stride>0/*32*/; stride>>=1) {
		if(tid < stride) {
			buffer[tid] += buffer[stride + tid];
		}
		__syncthreads();
	}

	// if(tid<32) {
	// 	warpReduceSum(buffer,tid);
	// }

  	__syncthreads();

  	// normalize the softmax
	dType sum_k = buffer[0];
	for (int i=i_start; i<i_end; i+=i_step) {
		output_k[i] = output_k[i] / sum_k;
	}
}



template<typename dType>
__global__ 
void matrix_bias_kernel(int hiddenstate_size, dType *d_mat,dType *d_vec,dType *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
		d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] + d_vec[idx];
	}
}


template<typename dType>
__global__
void train_perplexity_kernel(int *d_output_vocab_indices_single,int *d_output_vocab_indices_01_single,dType *d_outputdist,
	double *train_perplexity,int minibatch_size,int output_vocab_size) 
{
	for(int i= 0; i<minibatch_size; i++) {
		if(d_output_vocab_indices_01_single[i]==1) {
			train_perplexity[0]+= log( (double) d_outputdist[IDX2C(d_output_vocab_indices_single[i],i,output_vocab_size)]);
		}
	}
}


//This kernel adds a matrices rows to a matrices columns, which ones depend on the index
//hiddenstate_size refers to the number of rows in d_mat_final and also d_mat_col
template<typename dType>
__global__
void matrix_column_to_matrix_column_kernel(dType *d_mat_final,dType *d_mat_col,int *d_indices,int hiddenstate_size) {
	int idx = threadIdx.x + blockIdx.y*blockDim.x;
	if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] += d_mat_col[IDX2C(idx,d_indices[blockIdx.x],hiddenstate_size)];
	}
}

template<typename dType>
__global__ 
void zero_columns_kernel_128(int hiddenstate_size, dType *d_mat,int *d_vec,dType *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
	d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_vec[blockIdx.x];
	}
}

template<typename dType>
__global__
void add_ones_b_d_grad(dType *d_b_d_grad,int *d_output_vocab_indices_01,int *d_output_vocab_indices,int minibatch_size) {
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
	if(idx < minibatch_size && d_output_vocab_indices_01[idx]==1) {
		atomicAdd(&d_b_d_grad[d_output_vocab_indices[idx]],1);
	}
}


#include "softmax_node.h"
#include "trunc_softmax.h"
#include "trunc_softmax.hpp"


template<typename dType>
void check_gradient_GPU(dType *d_mat,dType *d_grad,int rows,int cols,trunc_softmax<dType> *layer) {
	dType epsilon = 1e-5;
	cudaDeviceSynchronize();
	thrust::device_ptr<dType> d_thrust_mat = thrust::device_pointer_cast(d_mat);
	thrust::device_ptr<dType> d_thrust_grad = thrust::device_pointer_cast(d_grad);
	for(int i=0; i<rows; i++) {
		for(int j=0; j<cols; j++) {
			dType loss =0;
			d_thrust_mat[IDX2C(i,j,rows)]+= epsilon;
			loss = layer->forward_prop(0);
			cudaDeviceSynchronize();
			d_thrust_mat[IDX2C(i,j,rows)]+= -2*epsilon;
			loss -=layer->forward_prop(0);
			cudaDeviceSynchronize();
			d_thrust_mat[IDX2C(i,j,rows)]+= epsilon;
			std::cout << "Gradient difference: " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon)) << "     my gradient: " << d_thrust_grad[IDX2C(i,j,rows)] << "\n";
			if( (std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))) > 1/(dType)1000.0 ||  (std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))/(std::abs(d_thrust_grad[IDX2C(i,j,rows)]) + std::abs(loss/(2*epsilon)))) > 1/1000.0  ) {
				std::cout << "Row: " << i << "  Col: " << j << "\n";
				std::cout << "Gradient for gradient check: " << loss/(2*epsilon) << "\n";
				std::cout << "My gradient: " << d_thrust_grad[IDX2C(i,j,rows)] << "\n";
				std::cout << "Gradient difference: " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon)) << "\n";
				std::cout << "Gradient difference (Equation 2): " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))/(std::abs(d_thrust_grad[IDX2C(i,j,rows)]) + std::abs(loss/(2*epsilon)) ) << "\n\n";
			}
		}
	}
}

typedef double precision;

int main() {

	const int output_vocab_size = 25;
	const int LSTM_size = 4;
	const int minibatch_size = 2;
	const int shortlist_size = 3;
	const int sampled_size = 4;

	const int num_trials = 10;

	int *h_indicies = (int *)malloc(minibatch_size*sizeof(int));


	boost::uniform_real<> distribution(0,1);
	for(int i=0; i<minibatch_size; i++) {
		h_indicies[i] = output_vocab_size*distribution(gen);
	}

	trunc_softmax<precision> ts;
	//ts.prep_GPU_vocab_indices(h_indicies,1);

	/*
	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	cudaDeviceSynchronize();
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {

		ts.prep_GPU_vocab_indices(h_indicies,1);

	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;
	*/


    ts.init_loss_layer(output_vocab_size,LSTM_size,minibatch_size,shortlist_size,sampled_size,1);
    ts.forward_prop(0);
    ts.back_prop1(0);
    ts.back_prop2(0);

    //now we have all the gradients, so 
 //    std::cout << "\n\n--------------------GRADIENT CHECKING FOR D---------------------\n\n";
	// check_gradient_GPU(layer.d_D,layer.d_D_grad,LSTM_size,output_vocab_size,&layer);

	// std::cout << "\n\n--------------------------GRADIENT CHECKING FOR b_d---------------------\n\n";
	// check_gradient_GPU(layer.d_b_d,layer.d_b_d_grad,output_vocab_size,1,&layer);

	std::cout << "\n\n--------------------------GRADIENT CHECKING FOR h_t---------------------\n\n";
	check_gradient_GPU(ts.nodes[0].d_h_t,ts.nodes[0].d_d_ERRt_ht,LSTM_size,minibatch_size,&ts);


}




















