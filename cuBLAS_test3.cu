#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "cublas_v2.h"
#include <cuda_runtime.h>

//This is used since all cuBLAS storage is column major
#define IDX2C(i,j,ld) (((j)*(ld))+(i))

//NOTE for my convention d_ representes a device pointer and h_ represents a host pointer


void CUDA_ERROR_WRAPPER(cudaError_t cudaStat,std::string error_message) {
	if (cudaStat != cudaSuccess) {
		std::cout << error_message << std::endl;
		exit (EXIT_FAILURE);
	}
}

void CUBLAS_ERROR_WRAPPER(cublasStatus_t cudaStat,std::string error_message) {
	if (cudaStat != cudaSuccess) {
		std::cout << error_message << std::endl;
		exit (EXIT_FAILURE);
	}
}


//Initialize the vector values when calling CUDA_init_vector
void init_vector(float *vec,int dim) {
	for(int i=0; i<dim; i++) {
		vec[i] = 1.0f;
	}
}

//Initialize the matrix values when calling CUDA_init_matrix
void init_matrix(float *mat,int rows,int cols) {
	for (int j = 0; j < cols; j++) { 
		for (int i = 0; i < rows; i++) { 
			mat[IDX2C(i,j,rows)] = 1.0f; 
		} 
	} 
}

void print_vector(float *vec,int dim) {
	for(int i=0; i<dim; i++) {
		std::cout << vec[i] << "\n";
	}
	std::cout <<"\n";
}

void print_matrix(float *mat,int rows,int cols) {
	for (int i = 0; i < rows; i++) { 
		for (int j = 0; j < cols; j++) { 
			std::cout << mat[IDX2C(i,j,rows)] << " "; 
		} 
		std::cout << "\n";
	} 
	std::cout << "\n\n";
}

//Load the vector onto the GPU with this initialization
void CUDA_init_vector(float **h_vec,int dim) {
	*h_vec = (float *) malloc(dim*sizeof(**h_vec));
	init_vector(*h_vec,dim);
}

void CUDA_init_matrix(float **h_mat,int rows,int cols) {
	*h_mat = (float *) malloc(rows*cols*sizeof(**h_mat));
	init_matrix(*h_mat,rows,cols);
}

int main() {
	
	//The handle for cuBLAS, note multiple can be made to use across multiple GPU's
	cublasHandle_t handle;

	int rows_mat=4;
	int cols_mat=3;
	int rows_vec=3;

	//Init the CUBLAS creation
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS initialization failed\n");
 	
	//Host matrices
	float *h_mat=0;
	float *h_vec=0;
	float *h_final_vec;

	//Device matrices
	float *d_mat;
	float *d_vec;
	float *d_final_vec;

	//Initialize the matrices
	CUDA_init_vector(&h_vec,rows_vec);
	CUDA_init_vector(&h_final_vec,rows_vec);
	CUDA_init_matrix(&h_mat,rows_mat,cols_mat);

	//print the matrices before multiplication
	print_vector(h_vec,rows_vec);
	print_vector(h_final_vec,rows_vec);
	print_matrix(h_mat,rows_mat,cols_mat);

	//Now copy the matrices to the GPU
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_vec, rows_vec*sizeof(*h_vec)),"device memory allocation failed\n");
	CUBLAS_ERROR_WRAPPER(cublasSetVector(rows_vec, sizeof(*h_vec), h_vec, 1, d_vec, 1),"data download failed\n");

	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_final_vec, rows_vec*sizeof(*h_final_vec)),"device memory allocation failed\n");
	CUBLAS_ERROR_WRAPPER(cublasSetVector(rows_vec, sizeof(*h_final_vec), h_final_vec, 1, d_final_vec, 1),"data download failed\n");

	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_mat, rows_mat*cols_mat*sizeof(*h_mat)),"device memory allocation failed\n");
	CUBLAS_ERROR_WRAPPER(cublasSetMatrix(rows_mat, cols_mat, sizeof(*h_mat), h_mat, rows_mat, d_mat, rows_mat),"data download failed\n");

	//Do the multiplication
	float alpha = 1.0f;
	float beta = 0.0f;

	CUBLAS_ERROR_WRAPPER(cublasSgemv(handle, CUBLAS_OP_N, rows_mat, cols_mat, 
		&alpha, d_mat, rows_mat, d_vec, 1, &beta, 
		d_final_vec, 1),"matrix multiplication failed\n");

	CUBLAS_ERROR_WRAPPER(cublasGetVector (rows_vec, sizeof(*h_final_vec), d_final_vec, 1,h_final_vec, 1),"get vector failed\n");


	//print the matrices after multiplication
	std::cout << "Matrix multiplication just finished \n";
	print_vector(h_final_vec,rows_vec);

	cudaFree(d_mat);
	cudaFree(d_vec);
	cudaFree(d_final_vec); 
	cublasDestroy(handle); 
	
	free(h_mat);
	free(h_vec);
	return EXIT_SUCCESS;
}


