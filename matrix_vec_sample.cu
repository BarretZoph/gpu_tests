//Kernel for the sparse lookups

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "BZ_CUDA_UTIL.h"

//W is the embedding matrix
//d_lookup is the (hidden state size)x(minibatch size) matrix
//Note that W is stored in column major
//Row size and col size
__global__ 
void sparse_lookup(float *d_lookup, float *d_W,int *d_vocab_indices, int row_size,int col_size)
{
	a[threadIdx.x] += b[threadIdx.x];
}


int main() {
	// const int minibatch_size = 128;
	// const int hiddenstate_size = 1000;
	// const int vocab_size = 160000;

	float *h_mat;
	float *d_mat;
	int rows_mat = 3;
	int cols_mat = 2;

	float *h_vec;
	float *d_vec;
	int rows_vec = 2;

	float *d_vec_output;
	float *h_vec_output;
	int rows_vec_output =3;

	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");
	full_matrix_setup(&h_mat,&d_mat,rows_mat,cols_mat);
	full_vector_setup(&h_vec,&d_vec,rows_vec);
	full_vector_setup(&h_vec_output,&d_vec_output,rows_vec_output);

	print_matrix(h_mat,rows_mat,cols_mat);
	print_matrix(h_vec,rows_vec,1);

	matrix_vector_mult(handle,d_mat,d_vec, d_vec_output,rows_mat,cols_mat);

	get_vector_cuBLAS(h_vec_output,d_vec_output,rows_vec_output);

	print_matrix(h_vec_output,rows_vec_output,1);

	//sparse_lookup<<< >>>();
}