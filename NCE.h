#ifndef NCE_H
#define NCE_H

#include "multinomial.h"
#include <algorithm> 
#include <fstream>
#include <unordered_map>

template<typename dType>
class NCE_layer {
public:

	int LSTM_size;
	int minibatch_size;
	int output_vocab_size;
	int num_negative_samples;
	int longest_sent;
	dType learning_rate = 0.1;

	int curr_num_unique = 0; //for the current minibatch, how many unqiue samples are there

	cublasHandle_t handle;

	multinomial<long long int,double> unigram;

	dType *h_temp;
	dType *d_h_t;
	dType *d_D; //For NCE this is embedding size by output vocab size, while in softmax it is the other way around
	dType *d_b_d;
	dType *d_temp_embeddings;
	dType *d_dot_products; //stores dot product along with the embedding
	dType *d_p_true; // store the minus of p(TRUE), stored in format [minibatch] [minibatch] ...
	dType *d_d_ERRt_ht;
 	dType *d_D_grad;
 	dType *d_b_d_grad;
 	dType *d_ones; // 1xminibatch size
 	dType *d_temp_b_d_grad; //1 x ( num negative samples + minibatchsize )
 	thrust::device_ptr<dType> thrust_d_b_d_grad;

 	double *d_OBJ_val_temp;
 	double *d_final_NCE_OBJ;

 	dType *d_temp_D_grad;


	int *h_vocab_indicies;
	int *d_vocab_indicies; //stored as [negative samples][positive words] [negative samples][positive words] ... for the current larget length
	int *h_unique_indicies;
	int *d_unique_indicies;


	//these are for the inputs
	int *d_output_vocab_indices;

	dType *h_sampling_probs; //stores the log (k*Q(w)) 
	dType *d_sampling_probs; //same format as d_vocab_indicies

	std::vector<NCE_node<dType>> nodes;

	NCE_layer(int LSTM_size,int minibatch_size,int output_vocab_size,int num_negative_samples,int longest_sent,std::string file_name);

	//this will compute all of the negative samples for a minibatch
	void get_unigram_counts(std::vector<long long int> &unigram_counts,std::string file_name);

	//prep gpu indicies
	void prep_GPU_vocab_indices(int *h_input_vocab_indicies_target,int current_target_length,int minibatch_size);

	//this will merge the positive words with the negative samples for the correct format for d_vocab_indicies
	void merge_samples();

	double forward_prop();

	void back_prop();

	void clear_gradients();

	void update_weights();

	double 

};



#endif