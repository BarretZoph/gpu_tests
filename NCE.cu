#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>

#include "math_constants.h"

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

#include "multinomial.h"
#include <algorithm> 


//NCE


#define NUM_NCE_THREADS 128
template<typename dType>
__global__
void nce_dot_product(dType *d_dot_products,dType *d_D,dType *d_h_t,int *d_samples,int LSTM_size,int minibatch_size,int num_samples,int output_vocab_size) {

	__shared__ dType buffer[NUM_NCE_THREADS];

	//per block doing an embedding
	int i_start = threadIdx.x; //start at the thread index
	int i_end = LSTM_size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step
	const int tid = threadIdx.x;

	for(int k = blockIdx.x; k < num_samples*minibatch_size; k+=gridDim.x) {
		int minibatch_index = k/num_samples;
		int sample_index = k%num_samples;
		int vocab_index = d_samples[IDX2C(sample_index,minibatch_index,num_samples)];
		buffer[tid] = 0;

		for(int i=i_start; i<i_end; i+=i_step) {
			buffer[tid] += d_h_t[IDX2C(i,minibatch_index,LSTM_size)] * d_D[IDX2C(i,vocab_index,LSTM_size)];
		}

		 __syncthreads();

		 for(int stride=NUM_NCE_THREADS/2; stride>0; stride>>=1) {
			if(tid < stride) {
				buffer[tid] += buffer[stride + tid];
			}
			__syncthreads();
		}

	  	__syncthreads();

	  	
		dType sum_k = buffer[0];
		if(tid==0) {
			d_dot_products[IDX2C(sample_index,minibatch_index,num_samples)] = sum_k; 
		}
		__syncthreads();
	}
}



//copy into temp embeddings
//num samples is the size of the negative samples shared across a minibatch and the positive samples
template<typename dType>
__global__
void load_in_embeddings(dType *d_temp_embeddings,dType *d_D,int *d_samples,int num_samples,int LSTM_size) {

	int i_start = threadIdx.x; //start at the thread index
	int i_end = LSTM_size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step

	for(int k = blockIdx.x; k < num_samples; k+=gridDim.x) {
		int vocab_index = d_samples[k];
		for(int i= i_start; i < i_end; i += i_step) {
			d_temp_embeddings[IDX2C(i,k,LSTM_size)] = d_D[IDX2C(i,vocab_index,LSTM_size)];
		}
	}
}

template<typename dType>
__device__
inline dType log_add_exp(dType x,dType y) {

	dType min = cuda_min_wrapper(x,y);
	dType max = cuda_max_wrapper(x,y);
	return max + cuda_log1p_wrapper(cuda_exp_wrapper(min-max));
}

//compute -P(true) for all of the elements
template<typename dType>
__global__
void calc_p_true_kernel(dType *d_p_true,dType *d_dot_products,dType *d_sampling_probs,dType *d_b_d,int *d_samples,int num_samples,int minibatch_size) {

	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<num_samples*minibatch_size; i+=gridDim.x*blockDim.x) {
		int minibatch_index = i%minibatch_size;
		int sample_index = i/minibatch_size;
		//printf("%i\n",d_samples[sample_index]);
		d_p_true[IDX2C(minibatch_index,sample_index,minibatch_size)] = -1*cuda_exp_wrapper( d_dot_products[IDX2C(sample_index,minibatch_index,num_samples)] + d_b_d[d_samples[sample_index]] - \
			log_add_exp(d_dot_products[IDX2C(sample_index,minibatch_index,num_samples)] + d_b_d[d_samples[sample_index]],d_sampling_probs[sample_index]));   
	}		
}


//get the objective value for NCE
template<typename dType>
__global__
void objective_val_p1_NCE_kernel(dType *d_p_true,double *d_OBJ_val_temp,int num_negative_samples,int minibatch_size) {

	__shared__ dType buffer[NUM_NCE_THREADS];
	int i_start = threadIdx.x; //start at the thread index
	int i_end = minibatch_size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step
	int tid = threadIdx.x;
	buffer[threadIdx.x] = 0;

	for(int k = blockIdx.x; k < num_negative_samples; k+=gridDim.x) {
		for(int i=i_start; i<i_end; i+= i_step) {
			buffer[threadIdx.x] += cuda_log_wrapper(1+d_p_true[IDX2C(i,k,minibatch_size)]);
		}
	}

	__syncthreads();

	for(int stride=NUM_NCE_THREADS/2; stride>0; stride>>=1) {
		if(tid < stride) {
			buffer[tid] += buffer[stride + tid];
		}
		__syncthreads();
	}

	__syncthreads();

	if(tid==0) {
		d_OBJ_val_temp[blockIdx.x]=buffer[0];
	}
}


//get the objective value for NCE
template<typename dType>
__global__
void objective_val_p2_NCE_kernel(dType *d_p_true,double *d_final_NCE_OBJ,double *d_OBJ_val_temp,int num_negative_samples,int minibatch_size) {

	for(int i=0; i<NUM_NCE_THREADS; i++) {
		d_final_NCE_OBJ[0] +=d_OBJ_val_temp[i];
	}

	for(int i=0; i<minibatch_size; i++) {
		d_final_NCE_OBJ[0]+=cuda_log_wrapper(-d_p_true[IDX2C(i,i+num_negative_samples,minibatch_size)]);
	}
}




//compute d_err_ht with respect to positive embeddings
//temp embeddings pointer being passed in skips the beginning negative sample embeddings
template<typename dType>
__global__
void error_ht_positive_kernel(dType *d_d_ERRt_ht,dType *d_p_true,dType *d_temp_embeddings,int num_negative_samples,int LSTM_size,int minibatch_size) {

	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<LSTM_size*minibatch_size; i+=gridDim.x*blockDim.x) {
		int minibatch_index = i/LSTM_size;
		int LSTM_index = i%LSTM_size;
		d_d_ERRt_ht[IDX2C(LSTM_index,minibatch_index,LSTM_size)] += (1 + d_p_true[IDX2C(minibatch_index,num_negative_samples+minibatch_index,minibatch_size)]) * d_temp_embeddings[IDX2C(LSTM_index,minibatch_index,LSTM_size)];
	}		
}




//d_samples being passed in is pointing at the positive samples already
template<typename dType>
__global__
void positive_embedding_NCE(dType *d_h_t,dType *d_D_grad,dType *d_p_true,int *d_samples,int LSTM_size,int minibatch_size) {
	
	int i_start = threadIdx.x; //start at the thread index
	int i_end = LSTM_size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step

	for(int k = blockIdx.x; k < minibatch_size; k+=gridDim.x) {
		int vocab_index = d_samples[k];
		for(int i= i_start; i < i_end; i += i_step) {
			atomicAdd(&(d_D_grad[IDX2C(i,vocab_index,LSTM_size)]),d_h_t[IDX2C(i,k,LSTM_size)]*(1+d_p_true[IDX2C(k,k,minibatch_size)]));
		}
	}
}


template<typename dType>
__global__
void negative_embedding_NCE(dType *d_temp_D_grad,dType *d_D_grad,int *d_samples,int num_negative_samples,int LSTM_size) {

	int i_start = threadIdx.x; //start at the thread index
	int i_end = LSTM_size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step

	for(int k = blockIdx.x; k < num_negative_samples; k+=gridDim.x) {
		int vocab_index = d_samples[k];
		for(int i= i_start; i < i_end; i += i_step) {
			atomicAdd(&(d_D_grad[IDX2C(i,vocab_index,LSTM_size)]),d_temp_D_grad[IDX2C(i,k,LSTM_size)]);
		}
	}
}

template<typename dType>
__global__
void negative_bias_NCE(dType *d_temp_b_d_grad,dType *d_b_d_grad,int *d_samples,int num_negative_samples) {

	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<num_negative_samples; i+=gridDim.x*blockDim.x) {
		atomicAdd(&(d_b_d_grad[d_samples[i]]),d_temp_b_d_grad[i]);
	}
}

template<typename dType>
__global__
void positive_bias_NCE(dType *d_b_d_grad,dType *d_p_true,int *d_samples,int minibatch_size,int num_negative_samples) {

	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<minibatch_size; i+=gridDim.x*blockDim.x) {
		atomicAdd(&(d_b_d_grad[d_samples[i+num_negative_samples]]),1+d_p_true[IDX2C(i,i+num_negative_samples,minibatch_size)]);
	}
}

template<typename dType>
__global__ 
void zero_W_gradient(dType *d_W_gradient,int *d_vocab_indicies_m1,int hiddenstate_size,int total_length) {
	for(int j=blockIdx.y; j<total_length; j+=gridDim.y) {
		const int idx = threadIdx.x + blockIdx.x*blockDim.x;
		if(idx < hiddenstate_size) {
			d_W_gradient[IDX2C(idx,d_vocab_indicies_m1[j],hiddenstate_size)] = 0;
		}
	}
}

template<typename dType>
__global__
void ZERO_CHECK(dType *d_mat,int size) {

	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		if(d_mat[i]!=0) {
			printf("ERROR NOT ZERO\n");
		}
	}
}

struct scale_functor {
	const int minibatch_size;

	scale_functor(int _minibatch_size) : minibatch_size(_minibatch_size) {}

	__host__ __device__ void operator()(float &x) {
		x = (1.0f/minibatch_size)*x;
	}
	__host__ __device__ void operator()(double &x) {
		x = (1.0/minibatch_size)*x;
	}
};

//Kernel for getting scaling the gradient of W by 1/(minibatch size)
template<typename dType>
__global__
void scale_W_gradient(dType *d_W_gradient,int *d_vocab_indicies_m1,int hiddenstate_size,dType scale,int total_length) {
	for(int j=blockIdx.y; j<total_length; j+=gridDim.y) {
		const int idx = threadIdx.x + blockIdx.x*blockDim.x;
		if(idx < hiddenstate_size) {
			const int index = IDX2C(idx,d_vocab_indicies_m1[j],hiddenstate_size);
			d_W_gradient[index] = scale * d_W_gradient[index];
		}
	}
}

template<typename dType>
__global__
void gradient_update_mats(dType *d_mat,dType *d_mat_grad,dType learning_rate,int size) {
	for(int i=threadIdx.x + blockIdx.x*blockDim.x; i<size; i+=gridDim.x*blockDim.x) {
		d_mat[i]+= learning_rate * d_mat_grad[i];
	}
}

template<typename dType>
__global__ 
void update_W_gradient(dType *d_W, dType *d_W_gradient,int *d_vocab_indicies_m1,dType learning_rate,int hiddenstate_size,int total_length) {
	for(int j = blockIdx.y; j<total_length; j+=gridDim.y) {
		int idx = threadIdx.x + blockIdx.x*blockDim.x;
		if(idx < hiddenstate_size) {
			int index = IDX2C(idx,d_vocab_indicies_m1[j],hiddenstate_size);
			d_W[index] = learning_rate* d_W_gradient[index] + d_W[index];
		}
	}
}

#include "NCE_node.h"
#include "NCE.h"
#include "NCE.hpp"

template<typename dType>
void check_gradient_GPU(dType *d_mat,dType *d_grad,int rows,int cols,NCE_layer<dType> *layer) {
	dType epsilon = 1e-5;
	cudaDeviceSynchronize();
	thrust::device_ptr<dType> d_thrust_mat = thrust::device_pointer_cast(d_mat);
	thrust::device_ptr<dType> d_thrust_grad = thrust::device_pointer_cast(d_grad);
	for(int i=0; i<rows; i++) {
		for(int j=0; j<cols; j++) {
			dType loss =0;
			d_thrust_mat[IDX2C(i,j,rows)]+= epsilon;
			loss = layer->forward_prop();
			cudaDeviceSynchronize();
			d_thrust_mat[IDX2C(i,j,rows)]+= -2*epsilon;
			loss -=layer->forward_prop();
			cudaDeviceSynchronize();
			d_thrust_mat[IDX2C(i,j,rows)]+= epsilon;
			std::cout << "Gradient difference: " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon)) << "     my gradient: " << d_thrust_grad[IDX2C(i,j,rows)] << "\n";
			if( (std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))) > 1/(dType)1000.0 ||  (std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))/(std::abs(d_thrust_grad[IDX2C(i,j,rows)]) + std::abs(loss/(2*epsilon)))) > 1/1000.0  ) {
				std::cout << "Row: " << i << "  Col: " << j << "\n";
				std::cout << "Gradient for gradient check: " << loss/(2*epsilon) << "\n";
				std::cout << "My gradient: " << d_thrust_grad[IDX2C(i,j,rows)] << "\n";
				std::cout << "Gradient difference: " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon)) << "\n";
				std::cout << "Gradient difference (Equation 2): " << std::abs(d_thrust_grad[IDX2C(i,j,rows)] - loss/(2*epsilon))/(std::abs(d_thrust_grad[IDX2C(i,j,rows)]) + std::abs(loss/(2*epsilon)) ) << "\n\n";
			}
		}
	}
}

typedef float precision;

int main() {

	const int output_vocab_size = 12;
	const int LSTM_size = 4;
	const int minibatch_size = 3;
	const int num_negative_samples = 2;
	const int num_trials = 10;

	const int curr_sent_len = 1;
	std::string file_name = "test_NCE_file.txt";

	int *h_indicies = (int *)malloc(minibatch_size*curr_sent_len*sizeof(int));

	gen.seed(static_cast<unsigned int>(std::time(0)));

	boost::uniform_real<> distribution(0,1);
	for(int i=0; i<minibatch_size*curr_sent_len; i++) {
		h_indicies[i] = output_vocab_size*distribution(gen);
	}

	NCE_layer<precision> layer(LSTM_size,minibatch_size,output_vocab_size,num_negative_samples,curr_sent_len,file_name);
	layer.prep_GPU_vocab_indices(h_indicies,curr_sent_len,minibatch_size);

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	cudaDeviceSynchronize();
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		layer.forward_prop();
		layer.back_prop();
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of smart NCE: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;




 //    //gradient checking
 //    layer.clear_gradients();
 //    layer.prep_GPU_vocab_indices(h_indicies,curr_sent_len,minibatch_size);
	// layer.forward_prop();
	// layer.back_prop();

	// cudaDeviceSynchronize();
	// print_GPU_Matrix(layer.d_p_true,minibatch_size,num_negative_samples+minibatch_size);

	// std::cout << "\n\n--------------------GRADIENT CHECKING FOR D---------------------\n\n";
	// check_gradient_GPU(layer.d_D,layer.d_D_grad,LSTM_size,output_vocab_size,&layer);


	// std::cout << "\n\n--------------------------GRADIENT CHECKING FOR b_d---------------------\n\n";
	// check_gradient_GPU(layer.d_b_d,layer.d_b_d_grad,output_vocab_size,1,&layer);

	// std::cout << "\n\n--------------------------GRADIENT CHECKING FOR h_t---------------------\n\n";
	// check_gradient_GPU(layer.d_h_t,layer.d_d_ERRt_ht,LSTM_size,minibatch_size,&layer);

	// layer.update_weights();
	// layer.clear_gradients();


	// cudaDeviceSynchronize();
	// ZERO_CHECK<<<256,256>>>(layer.d_D_grad,LSTM_size*output_vocab_size);


}





