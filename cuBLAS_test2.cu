#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "cublas_v2.h"
#include <cuda_runtime.h>

//This is used since all cuBLAS storage is column major
#define IDX2C(i,j,ld) (((j)*(ld))+(i))

static __inline__ void modify (cublasHandle_t handle, float *m, int ldm, int n, int p, int q, float alpha, float beta) {
	cublasSscal (handle, n-p, &alpha, &m[IDX2C(p,q,ldm)], ldm); 
	cublasSscal (handle, ldm-p, &beta, &m[IDX2C(p,q,ldm)], 1); 
}

void CUDA_ERROR_WRAPPER(cudaError_t cudaStat,std::string error_message) {
	if (cudaStat != cudaSuccess) {
		std::cout << error_message << std::endl;
		exit (EXIT_FAILURE);
	}
}

void CUBLAS_ERROR_WRAPPER(cublasStatus_t cudaStat,std::string error_message) {
	if (cudaStat != cudaSuccess) {
		std::cout << error_message << std::endl;
		exit (EXIT_FAILURE);
	}
}


int main() {
	//Parameters
	const int M =6;
	const int N =5;

	//The handle for cuBLAS, note multiple can be made to use across multiple GPU's
	cublasHandle_t handle;

	int i, j; 
	float* devPtrA; 
	float* a = 0; 
	a = (float *) malloc(M * N * sizeof (*a));

	for (j = 0; j < N; j++) { 
		for (i = 0; i < M; i++) { 
			a[IDX2C(i,j,M)] = (float)(i * M + j + 1); 
		} 
	} 

	//Copy the memory from a on the host to devPtrA on the device
	//Not allocation is always done in column major format
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&devPtrA, M*N*sizeof(*a)),"device memory allocation failed");

	//Initialize the cuBLAS library with the handle created above
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS initialization failed\n");

	CUBLAS_ERROR_WRAPPER(cublasSetMatrix (M, N, sizeof(*a), a, M, devPtrA, M),"data download failed\n");

	//This is the cuBLAS operation
	modify (handle, devPtrA, M, N, 1, 2, 16.0f, 12.0f);

	CUBLAS_ERROR_WRAPPER(cublasGetMatrix (M, N, sizeof(*a), devPtrA, M, a, M),"data upload failed\n");


	cudaFree (devPtrA); 
	cublasDestroy(handle); 
	for (j = 0; j < N; j++) {
		for (i = 0; i < M; i++) {
	  		printf ("%7.0f", a[IDX2C(i,j,M)]); 
		} 
		printf ("\n"); 
	} 

	free(a); 
	return EXIT_SUCCESS;
}


