//Kernel for the sparse lookups

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "BZ_CUDA_UTIL.h"

#include <chrono>
#include <bulk/bulk.hpp>
#include <thrust/iterator/zip_iterator.h>

__global__
void t_stride_kernel(int n, float *y)
{
    for (int i = blockIdx.x * blockDim.x + threadIdx.x; 
         i < n; 
         i += blockDim.x * gridDim.x) 
      {
          y[i] = tanhf(y[i]);
      }
}




// struct tanh_functor {
//   float operator() (float &x) const { return std::tanh(x); }
// };

struct tanh_functor {
  __host__ __device__ void operator() (float &x) { x = std::tanh(x); }
};


template <typename Dtype>
__global__ void TanHForward(const int n, const Dtype* in, Dtype* out) {
  CUDA_KERNEL_LOOP(index, n) {
    out[index] = tanh(in[index]);
  }
}


//CAFFE
__global__ 
void tanh_kernel(float *d_result, float *d_matrix, int hiddenstate_size)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	int idx = threadIdx.x + blockIdx.y * blockDim.x;
	if( threadIdx.x + blockIdx.y * blockDim.x < hiddenstate_size ) {
		d_result[IDX2C(idx, blockIdx.x, hiddenstate_size)] = tanhf(d_matrix[IDX2C(idx, blockIdx.x, hiddenstate_size)]);
	}
}


__global__ 
void d_ERRnTOt_ot_kernel(float *d_d_ERRnTOt_ot,float *d_d_ERRnTOt_ht,float *d_o_t,float *d_c_t,int hiddenstate_size) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  	if(idx < hiddenstate_size) {
  		int index = IDX2C(idx,blockIdx.x,hiddenstate_size);
		d_d_ERRnTOt_ot[index] = d_d_ERRnTOt_ht[index] *  tanhf(d_c_t[index]) * d_o_t[index] * (1 - d_o_t[index]);
	}
}

struct thrust_test {
	__device__
	void operator() (bulk::agent<> &self,float *d_d_ERRnTOt_ot,float *d_d_ERRnTOt_ht,float *d_o_t,float *d_c_t,int hiddenstate_size) {
		int index = self.index();
		d_d_ERRnTOt_ot[index] = d_d_ERRnTOt_ht[index] *  tanhf(d_c_t[index]) * d_o_t[index] * (1 - d_o_t[index]);
	}
};


struct arbitrary_functor
{
    template <typename Tuple>
    __host__ __device__
    void operator()(Tuple t)
    {
        // D[i] = A[i] + B[i] * C[i];
        thrust::get<0>(t) = thrust::get<1>(t) * tanh(thrust::get<2>(t)) * thrust::get<3>(t) * (1 - thrust::get<3>(t));
    }
};



int main() {

	const int hiddenstate_size = 1000;
	const int minibatch_size = 128;
	const int num_trials = 100;


	thrust::host_vector<float> thrust_h_matrix(hiddenstate_size * minibatch_size);
	thrust::device_vector<float> thrust_d_matrix(hiddenstate_size * minibatch_size);

	thrust::host_vector<float> thrust_h_d_ERRnTOt_ot(hiddenstate_size * minibatch_size);
	thrust::device_vector<float> thrust_d_d_ERRnTOt_ot(hiddenstate_size * minibatch_size);

	thrust::host_vector<float> thrust_h_d_ERRnTOt_ht(hiddenstate_size * minibatch_size);
	thrust::device_vector<float> thrust_d_d_ERRnTOt_ht(hiddenstate_size * minibatch_size);

	thrust::host_vector<float> thrust_h_o_t(hiddenstate_size * minibatch_size);
	thrust::device_vector<float> thrust_d_o_t(hiddenstate_size * minibatch_size);

	thrust::host_vector<float> thrust_h_c_t(hiddenstate_size * minibatch_size);
	thrust::device_vector<float> thrust_d_c_t(hiddenstate_size * minibatch_size);

    initialize_thrust_vector(thrust_h_matrix,hiddenstate_size * minibatch_size);
    initialize_thrust_vector(thrust_h_d_ERRnTOt_ot,hiddenstate_size * minibatch_size);
    initialize_thrust_vector(thrust_h_d_ERRnTOt_ht,hiddenstate_size * minibatch_size);
    initialize_thrust_vector(thrust_h_o_t,hiddenstate_size * minibatch_size);
    initialize_thrust_vector(thrust_h_c_t,hiddenstate_size * minibatch_size);

    thrust_d_matrix = thrust_h_matrix;

    thrust_d_d_ERRnTOt_ot = thrust_h_d_ERRnTOt_ot;

    thrust_d_d_ERRnTOt_ht = thrust_h_d_ERRnTOt_ht;

	thrust_d_o_t = thrust_h_o_t;

	thrust_d_c_t = thrust_h_c_t;



	float *d_matrix;
	float *h_matrix;
	float *h_d_ERRnTOt_ot;
	float *h_d_ERRnTOt_ht;
	float *h_o_t;
	float *h_c_t;
	float *d_d_ERRnTOt_ot;
	float *d_d_ERRnTOt_ht;
	float *d_o_t;
	float *d_c_t;
	full_matrix_setup(&h_matrix, &d_matrix, hiddenstate_size, minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_ot, &d_d_ERRnTOt_ot, hiddenstate_size, minibatch_size);
	full_matrix_setup(&h_d_ERRnTOt_ht, &d_d_ERRnTOt_ht, hiddenstate_size, minibatch_size);
	full_matrix_setup(&h_o_t, &d_o_t, hiddenstate_size, minibatch_size);
	full_matrix_setup(&h_c_t, &d_c_t, hiddenstate_size, minibatch_size);


	//KERNEL TESTING
	int threads_per_block = 128;
	int num_blocks = (hiddenstate_size + threads_per_block - 1) / threads_per_block;
	dim3 kernel(minibatch_size, num_blocks, 1 );


	// std::cout << "Normal Kernel Before: \n";
 //    for(int i = 0; i < hiddenstate_size * minibatch_size; i++)
 //    {
 //    	std::cout << h_matrix[i] << std::endl;
 //    }

	//timing
	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		tanh_kernel<<<kernel, threads_per_block >>>(d_matrix, d_matrix, hiddenstate_size);
		CUDA_GET_LAST_ERROR();
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of my kernel: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

    // get_matrix_cuBLAS(h_matrix, d_matrix, hiddenstate_size, minibatch_size);

    // for(int i = 0; i < hiddenstate_size * minibatch_size; i++)
    // {
    // 	std::cout << h_matrix[i] << std::endl;
    // }


    int blockSize;      // The launch configurator returned block size 
    int minGridSize;    // The minimum grid size needed to achieve the maximum occupancy for a full device launch 
    cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, tanh_kernel, 0, hiddenstate_size * minibatch_size);

    std::cout << "MIN GRID SIZE: " << minGridSize << "\n";
    std::cout << "BLOCK SIZE: " << blockSize << "\n";


    //grid stride testing
	// start_total = std::chrono::system_clock::now();
	// threads_per_block = 128;
	// num_blocks = 16;
	// for(int i=0; i<num_trials; i++) {
	// 	t_stride_kernel<<<num_blocks,threads_per_block >>>(hiddenstate_size*minibatch_size,d_matrix);
	// 	CUDA_GET_LAST_ERROR();
	// }
	// cudaDeviceSynchronize();
	// end_total = std::chrono::system_clock::now();
	// elapsed_seconds = end_total-start_total;
 //    std::cout << "\n\n\n";
 //    std::cout << "Average Runtime of grdi stride kernel: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	// std::cout << "\n Thrust Kernel Before: \n";

 //    for(int i = 0; i < hiddenstate_size * minibatch_size; i++)
 //    {
 //    	std::cout << thrust_d_matrix[i] << std::endl;
 //    }
    //THRUST TESTING
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		thrust::for_each(thrust_d_matrix.begin(), thrust_d_matrix.end(), tanh_functor());
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of thrust: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


    // for(int i = 0; i < hiddenstate_size * minibatch_size; i++)
    // {
    // 	std::cout << thrust_d_matrix[i] << std::endl;
    // }








    //SAXPY type stuff
    std::cout << "-----------------------NOW STARTING THRUST OPERATOR STUFF----------------------\n";
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		d_ERRnTOt_ot_kernel<<<kernel,threads_per_block>>>(d_d_ERRnTOt_ot,d_d_ERRnTOt_ht,d_o_t,d_c_t,hiddenstate_size);
		CUDA_GET_LAST_ERROR();
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of MY ot kernel: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


    thrust_d_d_ERRnTOt_ot = thrust_h_d_ERRnTOt_ot;

    thrust_d_d_ERRnTOt_ht = thrust_h_d_ERRnTOt_ht;

	thrust_d_o_t = thrust_h_o_t;

	thrust_d_c_t = thrust_h_c_t;

 //    std::cout << "-----------------------NOW STARTING THRUST OPERATOR STUFF----------------------\n";
	// start_total = std::chrono::system_clock::now();
	// for(int i=0; i<num_trials; i++) {
	// 	bulk::async(bulk::par(hiddenstate_size*minibatch_size),thrust_test(),bulk::root.this_exec,thrust::raw_pointer_cast(thrust_d_d_ERRnTOt_ot.data()),
	// 		thrust::raw_pointer_cast(thrust_d_d_ERRnTOt_ht.data()),thrust::raw_pointer_cast(thrust_d_o_t.data()),
	// 		thrust::raw_pointer_cast(thrust_d_c_t.data()),hiddenstate_size*minibatch_size);
	// }
	// cudaDeviceSynchronize();
	// end_total = std::chrono::system_clock::now();
	// elapsed_seconds = end_total-start_total;
 //    std::cout << "\n\n\n";
 //    std::cout << "Average Runtime of thrust OLD ot kernel: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


    std::cout << "--------------------OTHER THRUST TEST-------------------------\n";
    start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(thrust_d_d_ERRnTOt_ot.begin(), thrust_d_d_ERRnTOt_ht.begin(), thrust_d_o_t.begin(), thrust_d_c_t.begin())),
                     thrust::make_zip_iterator(thrust::make_tuple(thrust_d_d_ERRnTOt_ot.end(),   thrust_d_d_ERRnTOt_ht.end(),   thrust_d_o_t.end(),   thrust_d_c_t.end())),
                     arbitrary_functor());
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of thrust NEW ot kernel: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


}


