
//for non shared samples for NCE



//num_samples in this is the number of noise samples + 1
#define NUM_NCE_THREADS 128
template<typename dType>
__global__
void nce_dot_product_SPARSE(dType *d_dot_products,dType *d_D,dType *d_h_t,int *d_samples,int LSTM_size,int minibatch_size,int num_samples,int output_vocab_size) {

	__shared__ dType buffer[NUM_NCE_THREADS];

	//per block doing an embedding
	int i_start = threadIdx.x; //start at the thread index
	int i_end = LSTM_size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step
	const int tid = threadIdx.x;

	for(int k = blockIdx.x; k < num_samples*minibatch_size; k+=gridDim.x) {
		int minibatch_index = k/num_samples;
		int sample_index = k%num_samples;
		int vocab_index = d_samples[IDX2C(sample_index,minibatch_index,num_samples)];
		buffer[tid] = 0;

		for(int i=i_start; i<i_end; i+=i_step) {
			buffer[tid] += d_h_t[IDX2C(i,minibatch_index,LSTM_size)] * d_D[IDX2C(i,vocab_index,LSTM_size)];
		}

		 __syncthreads();

		 for(int stride=NUM_NCE_THREADS/2; stride>0; stride>>=1) {
			if(tid < stride) {
				buffer[tid] += buffer[stride + tid];
			}
			__syncthreads();
		}

	  	__syncthreads();

	  	
		dType sum_k = buffer[0];
		if(tid==0) {
			d_dot_products[IDX2C(sample_index,minibatch_index,num_samples)] = sum_k; 
		}
		__syncthreads();
	}
}



