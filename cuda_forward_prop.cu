
#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"
#include <chrono>

// __global__ 
// void sigm_elemwise(float d_result, float d_matrix, int rows)
// {
// 	//Each block is responsible for copying one column of d_W to d_lookup
// 	if( threadIdx.x + blockIdx.y  blockDim.x < rows )
// 		d_result[IDX2C(threadIdx.x + blockIdx.y  blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + __expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y  blockDim.x, blockIdx.x, rows)] ) );
// }

//This is used since all cuBLAS storage is column major

typedef double prec;



__global__ 
void matrix_bias_kernel(int hiddenstate_size, prec *d_mat,  prec *d_vec, prec *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
	d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] + d_vec[idx];
	}
}


__global__ 
void tanh_kernel(prec *d_result, prec *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = tanhf( d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] );
}

// For doubles
void matrix_matrix_add(cublasHandle_t handle, prec *d_matrix1, prec * d_matrix2, prec *d_matrix_res,
	int mat_rows, int mat_cols) 
{
	double alpha = 1.0;
	double beta = 1.0;
	CUBLAS_ERROR_WRAPPER(cublasDgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, mat_rows, mat_cols,
		&alpha, d_matrix1, mat_rows, &beta, d_matrix2, mat_rows, d_matrix_res, mat_rows),"matrix matrix add failed\n");
}

__global__ 
void sparse_lookup(prec *d_lookup, prec *d_W,int *d_vocab_indices, int minibatch_size,int hiddenstate_size,int vocab_size)
{
	if(threadIdx.x < hiddenstate_size)
		d_lookup[IDX2C(threadIdx.x,blockIdx.x,hiddenstate_size)] = d_W[IDX2C(threadIdx.x,d_vocab_indices[blockIdx.x],hiddenstate_size)];
}

__global__ 
void sigmoid_kernel(prec *d_result, prec *d_matrix, int rows)
{
	//Each block is responsible for copying one column of d_W to d_lookup
	if( threadIdx.x + blockIdx.y * blockDim.x < rows )
		d_result[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] = 1.0f / ( 1 + expf(- d_matrix[IDX2C(threadIdx.x + blockIdx.y * blockDim.x, blockIdx.x, rows)] ) );
}

struct sigm_functor {
	__host__ __device__ void operator()(prec &x) {
		x = 1.0f / ( 1 + exp(-x));
	}
};

__global__ 
void elementwise_mult_kernel(int hiddenstate_size, prec *d_mat1,prec *d_mat2,prec *d_final) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_mat1[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_mat2[IDX2C(idx,blockIdx.x,hiddenstate_size)];

	}
}

__global__ 
void firstThree(int hiddenstate_size, prec *d_mat1, prec *d_mat2,prec *d_final) 
{
	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = d_mat1[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_mat2[IDX2C(idx,blockIdx.x,hiddenstate_size)];

	}
}


struct sigmoid_functor {
  double operator() (double x) const { return 1.0/(1.0+std::exp(-x)); }
};

struct tanh_functor {
  double operator() (double x) const { return std::tanh(x); }
};

struct tanh_sq_functor {
  double operator() (double x) const { return std::tanh(x)*std::tanh(x); }
};


struct exp_functor {
  double operator() (double x) const { return std::exp(x); }
};

//Does elementwise sigmoid operation on matrix
template <typename DerivedIn, typename DerivedOut>
void elemwise_sigmoid(const Eigen::MatrixBase<DerivedIn> &input_const, const Eigen::MatrixBase<DerivedOut> &output_const) {
	UNCONST(DerivedOut, output_const, output);
	output = input_const.array().unaryExpr(sigmoid_functor());
	output.matrix();
}

//Does elementsize tanh operation on matrix
template <typename DerivedIn, typename DerivedOut>
void elemwise_tanh(const Eigen::MatrixBase<DerivedIn> &input_const, const Eigen::MatrixBase<DerivedOut> &output_const) {
	UNCONST(DerivedOut, output_const, output);
	output = input_const.array().unaryExpr(tanh_functor());
	output.matrix();
}

template <typename Derived>
void hidden_calc(const Eigen::MatrixBase<Derived> &c_t,const Eigen::MatrixBase<Derived> &h_t_const,const Eigen::MatrixBase<Derived> &o_t) {
	UNCONST(Derived, h_t_const, h_t);
	h_t = o_t.array()*(c_t.array().unaryExpr(tanh_functor()));
}

template<typename Derived, typename Derived1, typename Derived2>
void compute_temp_mat(const Eigen::MatrixBase<Derived> &W_mat, const Eigen::MatrixBase<Derived1> &temp_mat_const, const Eigen::MatrixBase<Derived2> &vocab_indices_input) {
	UNCONST(Derived1, temp_mat_const, temp_mat);

	for(int i=0; i<vocab_indices_input.rows(); i++) {
		if(vocab_indices_input(i)!=-1) {
			temp_mat.col(i) = W_mat.col(vocab_indices_input(i));
		}
		else {
			//Just assign it to a vector, since it does not matter
			temp_mat.col(i) = W_mat.col(0);
		}
	}
}




#define THREADS_PER_BLOCK 128

int main(int argc, char * argv[] )
{

	int hiddenstate_size = 1000;
	int minibatch_size = 128;
	int input_vocab_size = 160000;

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> temp_mat;
	temp_mat.resize(hiddenstate_size, minibatch_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> W;
	W.resize(hiddenstate_size, input_vocab_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> W_hi;
	W_hi.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> W_hf;
	W_hf.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> M_i;
	M_i.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> M_f;
	M_f.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic, 1> x_t;
	x_t.resize(minibatch_size, 1);


	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> h_t_prev;
	h_t_prev.resize(hiddenstate_size, minibatch_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> c_t_prev;
	c_t_prev.resize(hiddenstate_size, minibatch_size);


	Eigen::Matrix<prec, Eigen::Dynamic,1> b_i;
	b_i.resize(hiddenstate_size, 1);

	Eigen::Matrix<prec, Eigen::Dynamic,1> b_f;
	b_f.resize(hiddenstate_size, 1);

	Eigen::Matrix<prec, Eigen::Dynamic,Eigen::Dynamic> i_t;
	i_t.resize(hiddenstate_size, minibatch_size);

	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> f_t;
	f_t.resize(hiddenstate_size, minibatch_size);

	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> c_t;
	c_t.resize(hiddenstate_size, minibatch_size);


	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> W_hc;
	W_hc.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> M_c;
	M_c.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic,1> b_c;
	b_c.resize(hiddenstate_size, 1);



	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> c_prime_t_tanh;
	c_prime_t_tanh.resize(hiddenstate_size, minibatch_size);



	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> o_t;
	o_t.resize(hiddenstate_size, minibatch_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> W_ho;
	W_ho.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> M_o;
	M_o.resize(hiddenstate_size, hiddenstate_size);

	Eigen::Matrix<prec, Eigen::Dynamic,1> b_o;
	b_o.resize(hiddenstate_size, 1);



	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> h_t;
	h_t.resize(hiddenstate_size, minibatch_size);



	cublasHandle_t handle;

	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle), "CUBLAS initialization failed\n");


	int prod_hidden_mini = hiddenstate_size * minibatch_size;
	int prod_input_hidden = input_vocab_size * hiddenstate_size;
	int prod_hidden_hidden = hiddenstate_size * hiddenstate_size;


	thrust::host_vector<int> h_vocab_indices(minibatch_size);
    thrust::device_vector<int> d_vocab_indices(minibatch_size);

    thrust::host_vector<prec> h_lookup(prod_hidden_mini);
    thrust::device_vector<prec> d_lookup(prod_hidden_mini);

	thrust::host_vector<prec> h_i_t(prod_hidden_mini);
    thrust::device_vector<prec> d_i_t(prod_hidden_mini);

    thrust::host_vector<prec> h_M_i(prod_hidden_hidden);
    thrust::device_vector<prec> d_M_i(prod_hidden_hidden);

	thrust::host_vector<prec> h_W(prod_input_hidden);
    thrust::device_vector<prec> d_W(prod_input_hidden);

	thrust::host_vector<prec> h_W_hi(prod_hidden_hidden);
    thrust::device_vector<prec> d_W_hi(prod_hidden_hidden);

    thrust::host_vector<prec> h_ht_1(prod_hidden_mini);
    thrust::device_vector<prec> d_ht_1(prod_hidden_mini);

    thrust::host_vector<prec> h_ct_1(prod_hidden_mini);
    thrust::device_vector<prec> d_ct_1(prod_hidden_mini);


	thrust::host_vector<prec> h_f_t(prod_hidden_mini);
    thrust::device_vector<prec> d_f_t(prod_hidden_mini);

    thrust::host_vector<prec> h_B_i(hiddenstate_size);
    thrust::device_vector<prec> d_B_i(hiddenstate_size);

    thrust::host_vector<prec> h_M_f(prod_hidden_hidden);
    thrust::device_vector<prec> d_M_f(prod_hidden_hidden);

	thrust::host_vector<prec> h_W_hf(prod_hidden_hidden);
    thrust::device_vector<prec> d_W_hf(prod_hidden_hidden);

    thrust::host_vector<prec> h_B_f(hiddenstate_size);
    thrust::device_vector<prec> d_B_f(hiddenstate_size);


	thrust::host_vector<prec> h_c_t(prod_hidden_mini);
    thrust::device_vector<prec> d_c_t(prod_hidden_mini);

    thrust::host_vector<prec> h_cp_t(prod_hidden_mini);
    thrust::device_vector<prec> d_cp_t(prod_hidden_mini);

    thrust::host_vector<prec> h_B_c(hiddenstate_size);
    thrust::device_vector<prec> d_B_c(hiddenstate_size);

    thrust::host_vector<prec> h_M_c(prod_hidden_hidden);
    thrust::device_vector<prec> d_M_c(prod_hidden_hidden);

	thrust::host_vector<prec> h_W_hc(prod_hidden_hidden);
    thrust::device_vector<prec> d_W_hc(prod_hidden_hidden);


    thrust::host_vector<prec> h_o_t(prod_hidden_mini);
    thrust::device_vector<prec> d_o_t(prod_hidden_mini);

    thrust::host_vector<prec> h_B_o(hiddenstate_size);
    thrust::device_vector<prec> d_B_o(hiddenstate_size);

    thrust::host_vector<prec> h_M_o(prod_hidden_hidden);
    thrust::device_vector<prec> d_M_o(prod_hidden_hidden);

	thrust::host_vector<prec> h_W_ho(prod_hidden_hidden);
    thrust::device_vector<prec> d_W_ho(prod_hidden_hidden);



	thrust::host_vector<prec> h_h_t(prod_hidden_mini);
    thrust::device_vector<prec> d_h_t(prod_hidden_mini);



    thrust::device_vector<prec> d_inter1(prod_hidden_mini);
    thrust::device_vector<prec> d_inter2(prod_hidden_mini);
    thrust::device_vector<prec> d_inter3(prod_hidden_mini);
    thrust::device_vector<prec> d_inter4(prod_hidden_mini);

    thrust::host_vector<prec> h_inter1(prod_hidden_mini);
    thrust::host_vector<prec> h_inter2(prod_hidden_mini);
    thrust::host_vector<prec> h_inter3(prod_hidden_mini);
    thrust::host_vector<prec> h_inter4(prod_hidden_mini);


    //allocate the host thrust vectors
    initialize_thrust_vector(h_i_t, prod_hidden_mini);
    initialize_thrust_vector(h_W, prod_input_hidden);
    initialize_thrust_vector(h_ht_1, prod_hidden_mini);


    initialize_thrust_vector(h_M_i, prod_hidden_hidden);
    initialize_thrust_vector(h_W_hi, prod_hidden_hidden);
    initialize_thrust_vector(h_B_i, hiddenstate_size);

    initialize_thrust_vector(h_M_f, prod_hidden_hidden);
    initialize_thrust_vector(h_W_hf, prod_hidden_hidden);
    initialize_thrust_vector(h_B_f, hiddenstate_size);

    initialize_thrust_vector(h_M_c, prod_hidden_hidden);
    initialize_thrust_vector(h_W_hc, prod_hidden_hidden);
    initialize_thrust_vector(h_B_c, hiddenstate_size);

    initialize_thrust_vector(h_ct_1, hiddenstate_size);

    initialize_thrust_vector(h_M_o, prod_hidden_hidden);
    initialize_thrust_vector(h_W_ho, prod_hidden_hidden);
    initialize_thrust_vector(h_B_o, hiddenstate_size);


	for(int i = 0; i < minibatch_size; i++)
		h_vocab_indices[i] = 0;

 //    Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> temp_mat;
	// Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> W;
	// Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> M_i;
	// Eigen::Matrix<prec, Eigen::Dynamic, 1> x_t;
	// Eigen::Matrix<prec, Eigen::Dynamic, Eigen::Dynamic> h_t_prev;
	// Eigen::Matrix<prec, Eigen::Dynamic,1> b_i;
	// Eigen::Matrix<prec, Eigen::Dynamic,Eigen::Dynamic> i_t;

    copy_to_eigen_thrust(W, h_W);



    copy_to_eigen_thrust(h_t_prev, h_ht_1);
    copy_to_eigen_thrust(x_t, h_vocab_indices);


    copy_to_eigen_thrust(M_i, h_M_i);
    copy_to_eigen_thrust(W_hi, h_W_hi);
    copy_to_eigen_thrust(i_t, h_i_t);
    copy_to_eigen_thrust(b_i, h_B_i);



    copy_to_eigen_thrust(M_f, h_M_f);
    copy_to_eigen_thrust(W_hf, h_W_hf);
    copy_to_eigen_thrust(f_t, h_f_t);
    copy_to_eigen_thrust(b_f, h_B_f);



    copy_to_eigen_thrust(M_c, h_M_c);
    copy_to_eigen_thrust(W_hc, h_W_hc);
    copy_to_eigen_thrust(c_t, h_c_t);
    copy_to_eigen_thrust(b_c, h_B_c);



    copy_to_eigen_thrust(c_t_prev, h_ct_1);



    copy_to_eigen_thrust(M_o, h_M_o);
    
    copy_to_eigen_thrust(W_ho, h_W_ho);

    copy_to_eigen_thrust(b_o, h_B_o);


	d_i_t = h_i_t;
	d_ht_1 = h_ht_1;
	d_W = h_W;


	d_W_hi = h_W_hi;
	d_B_i = h_B_i;
	d_M_i = h_M_i;


	d_W_hf = h_W_hf;
	d_B_f = h_B_f;
	d_M_f = h_M_f;


	d_W_hc = h_W_hc;
	d_B_c = h_B_c;
	d_M_c = h_M_c;

	d_ct_1 = h_ct_1;


	d_W_ho = h_W_ho;
	d_B_o = h_B_o;
	d_M_o = h_M_o;

	d_vocab_indices = h_vocab_indices;


	prec *ptr_d_W = thrust::raw_pointer_cast(&d_W[0]);
	prec *ptr_h_W = thrust::raw_pointer_cast(&h_W[0]);

	
	prec *ptr_d_ht_1 = thrust::raw_pointer_cast(&d_ht_1[0]);
	prec *ptr_h_ht_1 = thrust::raw_pointer_cast(&h_ht_1[0]);

	prec *ptr_d_ct_1 = thrust::raw_pointer_cast(&d_ct_1[0]);
	prec *ptr_h_ct_1 = thrust::raw_pointer_cast(&h_ct_1[0]);

	prec *ptr_d_W_hi = thrust::raw_pointer_cast(&d_W_hi[0]);
	prec *ptr_h_W_hi = thrust::raw_pointer_cast(&h_W_hi[0]);
	prec *ptr_d_B_i = thrust::raw_pointer_cast(&d_B_i[0]);
	prec *ptr_d_M_i = thrust::raw_pointer_cast(&d_M_i[0]);
	prec *ptr_h_M_i = thrust::raw_pointer_cast(&h_M_i[0]);
	prec *ptr_d_i_t = thrust::raw_pointer_cast(&d_i_t[0]);
	prec * ptr_h_i_t = thrust::raw_pointer_cast(&h_i_t[0]);

	prec *ptr_d_W_hf = thrust::raw_pointer_cast(&d_W_hf[0]);
	prec *ptr_h_W_hf = thrust::raw_pointer_cast(&h_W_hf[0]);
	prec *ptr_d_B_f = thrust::raw_pointer_cast(&d_B_f[0]);
	prec *ptr_d_M_f = thrust::raw_pointer_cast(&d_M_f[0]);
	prec *ptr_h_M_f = thrust::raw_pointer_cast(&h_M_f[0]);
	prec *ptr_d_f_t = thrust::raw_pointer_cast(&d_f_t[0]);
	prec * ptr_h_f_t = thrust::raw_pointer_cast(&h_f_t[0]);
	// prec *ptr_h_B_i = thrust::raw_pointer_cast(&h_B_i[0]);

	prec *ptr_d_W_hc = thrust::raw_pointer_cast(&d_W_hc[0]);
	prec *ptr_h_W_hc = thrust::raw_pointer_cast(&h_W_hc[0]);
	prec *ptr_d_B_c = thrust::raw_pointer_cast(&d_B_c[0]);
	prec *ptr_d_M_c = thrust::raw_pointer_cast(&d_M_c[0]);
	prec *ptr_h_M_c = thrust::raw_pointer_cast(&h_M_c[0]);
	prec *ptr_d_c_t = thrust::raw_pointer_cast(&d_c_t[0]);
	prec * ptr_h_c_t = thrust::raw_pointer_cast(&h_c_t[0]);
	prec *ptr_d_cp_t = thrust::raw_pointer_cast(&d_cp_t[0]);
	prec * ptr_h_cp_t = thrust::raw_pointer_cast(&h_cp_t[0]);

	prec *ptr_d_W_ho = thrust::raw_pointer_cast(&d_W_ho[0]);
	prec *ptr_h_W_ho = thrust::raw_pointer_cast(&h_W_ho[0]);
	prec *ptr_d_B_o = thrust::raw_pointer_cast(&d_B_o[0]);
	prec *ptr_d_M_o = thrust::raw_pointer_cast(&d_M_o[0]);
	prec *ptr_h_M_o = thrust::raw_pointer_cast(&h_M_o[0]);
	prec *ptr_d_o_t = thrust::raw_pointer_cast(&d_o_t[0]);
	prec * ptr_h_o_t = thrust::raw_pointer_cast(&h_o_t[0]);

	prec *ptr_d_h_t = thrust::raw_pointer_cast(&d_o_t[0]);
	prec * ptr_h_h_t = thrust::raw_pointer_cast(&h_o_t[0]);

	prec * ptr_d_lookup = thrust::raw_pointer_cast(&d_lookup[0]);
	prec * ptr_h_lookup = thrust::raw_pointer_cast(&h_lookup[0]);

	int * ptr_d_vocab_indices = thrust::raw_pointer_cast(&d_vocab_indices[0]);
	int * ptr_h_vocab_indices = thrust::raw_pointer_cast(&h_vocab_indices[0]);

	prec * ptr_d_inter1 = thrust::raw_pointer_cast(&d_inter1[0]);
	prec * ptr_d_inter2 = thrust::raw_pointer_cast(&d_inter2[0]);
	prec * ptr_d_inter3 = thrust::raw_pointer_cast(&d_inter3[0]);
	prec * ptr_d_inter4 = thrust::raw_pointer_cast(&d_inter4[0]);

	prec * ptr_h_inter1 = thrust::raw_pointer_cast(&h_inter1[0]);
	prec * ptr_h_inter2 = thrust::raw_pointer_cast(&h_inter2[0]);
	prec * ptr_h_inter3 = thrust::raw_pointer_cast(&h_inter3[0]);
	prec * ptr_h_inter4 = thrust::raw_pointer_cast(&h_inter4[0]);


	int threadsPerBlock = 128;
	int numBlocks = (hiddenstate_size + threadsPerBlock - 1) / threadsPerBlock;
	dim3 kernel(minibatch_size, numBlocks, 1 );

	std::chrono::time_point<std::chrono::system_clock> c_start_total,c_end_total;
	c_start_total = std::chrono::system_clock::now();

	int num_trials = 1;

	for(int i = 0; i < num_trials; i++){

		// cudaStream_t s1;
		// cudaStream_t s2;
		// cudaStream_t s3;
		// cudaStream_t s4;


		sparse_lookup<<<kernel,threadsPerBlock>>>(ptr_d_lookup, ptr_d_W, ptr_d_vocab_indices, minibatch_size, hiddenstate_size, input_vocab_size);

		/*** I_T COMPUTATION ***/
		matrix_matrix_mult(handle, ptr_d_M_i, ptr_d_lookup, ptr_d_inter1, hiddenstate_size, hiddenstate_size, minibatch_size );
		matrix_matrix_mult(handle, ptr_d_W_hi, ptr_d_ht_1, ptr_d_inter2, hiddenstate_size, hiddenstate_size, minibatch_size );
		matrix_matrix_add(handle, ptr_d_inter1, ptr_d_inter2, ptr_d_inter1, hiddenstate_size, minibatch_size );
		
		get_matrix_cuBLAS(ptr_h_inter1, ptr_d_inter1, hiddenstate_size, minibatch_size);



		matrix_bias_kernel<<<kernel,threadsPerBlock>>>(hiddenstate_size, ptr_d_inter1, ptr_d_B_i, ptr_d_i_t);
		sigmoid_kernel<<<kernel, threadsPerBlock>>>(ptr_d_i_t, ptr_d_i_t, hiddenstate_size);
		get_matrix_cuBLAS(ptr_h_i_t, ptr_d_i_t, hiddenstate_size, minibatch_size);



		// /*** F_T COMPUTATION ***/
		// matrix_matrix_mult(handle, ptr_d_M_f, ptr_d_lookup, ptr_d_inter1, hiddenstate_size, hiddenstate_size, minibatch_size );
		// matrix_matrix_mult(handle, ptr_d_W_hf, ptr_d_ht_1, ptr_d_inter2, hiddenstate_size, hiddenstate_size, minibatch_size );
		// matrix_matrix_add(handle, ptr_d_inter1, ptr_d_inter2, ptr_d_inter1, hiddenstate_size, minibatch_size );
		// matrix_bias_kernel<<<kernel,threadsPerBlock>>>(hiddenstate_size, ptr_d_inter1, ptr_d_B_f, ptr_d_f_t);
		// sigmoid_kernel<<<kernel, threadsPerBlock>>>(ptr_d_f_t, ptr_d_f_t, hiddenstate_size);
		// get_matrix_cuBLAS(ptr_h_f_t, ptr_d_f_t, hiddenstate_size, minibatch_size);


		// /*** O_T COMPUTATION ***/		
		// matrix_matrix_mult(handle, ptr_d_M_o, ptr_d_lookup, ptr_d_inter1, hiddenstate_size, hiddenstate_size, minibatch_size );
		// matrix_matrix_mult(handle, ptr_d_W_ho, ptr_d_ht_1, ptr_d_inter2, hiddenstate_size, hiddenstate_size, minibatch_size );
		// matrix_matrix_add(handle, ptr_d_inter1, ptr_d_inter2, ptr_d_inter1, hiddenstate_size, minibatch_size );
		// matrix_bias_kernel<<<kernel,threadsPerBlock>>>(hiddenstate_size, ptr_d_inter1, ptr_d_B_o, ptr_d_o_t);
		// sigmoid_kernel<<<kernel, threadsPerBlock>>>(ptr_d_o_t, ptr_d_o_t, hiddenstate_size);
		// get_matrix_cuBLAS(ptr_h_o_t, ptr_d_o_t, hiddenstate_size, minibatch_size);


		// /*** Cprime_T COMPUTATION ***/

		// matrix_matrix_mult(handle, ptr_d_M_c, ptr_d_lookup, ptr_d_inter1, hiddenstate_size, hiddenstate_size, minibatch_size );
		// matrix_matrix_mult(handle, ptr_d_W_hc, ptr_d_ht_1, ptr_d_inter2, hiddenstate_size, hiddenstate_size, minibatch_size );
		// matrix_matrix_add(handle, ptr_d_inter1, ptr_d_inter2, ptr_d_inter1, hiddenstate_size, minibatch_size );
		// matrix_bias_kernel<<<kernel,threadsPerBlock>>>(hiddenstate_size, ptr_d_inter1, ptr_d_B_c, ptr_d_cp_t);
		// tanh_kernel<<<kernel, threadsPerBlock>>>(ptr_d_cp_t, ptr_d_cp_t, hiddenstate_size);

		// /*** C_T COMPUTATION ***/
		// elementwise_mult_kernel<<<kernel,threadsPerBlock>>>(hiddenstate_size, ptr_d_f_t, ptr_d_ct_1, ptr_d_inter1);
		// elementwise_mult_kernel<<<kernel,threadsPerBlock>>>(hiddenstate_size, ptr_d_i_t, ptr_d_cp_t, ptr_d_inter2);
		// matrix_matrix_add(handle, ptr_d_inter1, ptr_d_inter2, ptr_d_c_t, hiddenstate_size, minibatch_size);
		// get_matrix_cuBLAS(ptr_h_c_t, ptr_d_c_t, hiddenstate_size, minibatch_size);
		// // print_matrix(ptr_h_c_t, hiddenstate_size, minibatch_size);

		// // print_matrix(ptr_h_o_t, hiddenstate_size, minibatch_size);


		// tanh_kernel<<<kernel, threadsPerBlock>>>(ptr_d_inter1, ptr_d_c_t, hiddenstate_size);
		// elementwise_mult_kernel<<<kernel,threadsPerBlock>>>(hiddenstate_size, ptr_d_o_t, ptr_d_inter1, ptr_d_h_t);
		// get_matrix_cuBLAS(ptr_h_h_t, ptr_d_h_t, hiddenstate_size, minibatch_size);
		// print_matrix(ptr_h_h_t, hiddenstate_size, minibatch_size);

	}

	// print_matrix(ptr_h_h_t, hiddenstate_size, minibatch_size);

	cudaDeviceSynchronize();

	c_end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> c_elapsed_seconds = c_end_total-c_start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of cuda: " << (c_elapsed_seconds.count())/10.0 << " seconds" << std::endl;


	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();

	for(int i = 0; i < num_trials; i++)
	{
		compute_temp_mat(W, temp_mat, x_t);


		bool matcha = eigen_check_thres((M_i*temp_mat + W_hi*h_t_prev), ptr_h_inter1, .001);
		std::cout << "Match A" << matcha << std::endl;

		//input gate
		i_t = ((M_i*temp_mat + W_hi*h_t_prev).colwise() + b_i).array().unaryExpr(sigmoid_functor());

		bool match1 = eigen_check_thres(i_t, ptr_h_i_t, .001);

		std::cout << "match 1: " << match1 << std::endl;

		// // //Forget gate
		// f_t = ((M_f*temp_mat + W_hf*h_t_prev).colwise() + b_f).array().unaryExpr(sigmoid_functor());

		// bool match2 = eigen_check_thres(f_t, ptr_h_f_t, .001);

		// std::cout << "match 2: " << match2 << std::endl;

		// // //Cell gate
		// c_prime_t_tanh = ((M_c*temp_mat + W_hc*h_t_prev).colwise() + b_c).array().unaryExpr(tanh_functor());

		// bool match3 = eigen_check_thres(c_prime_t_tanh, ptr_h_cp_t, .001);
		// std::cout << "match 3: " << match3 << std::endl;


		// c_t = ((f_t.array())*(c_t_prev.array())).matrix() + (i_t.array()*(c_prime_t_tanh.array())).matrix();


		// bool match4 = eigen_check_thres(c_t, ptr_h_c_t, .001);
		// std::cout << "match 4: " << match4 << std::endl;

		// // //Output gate
		// o_t = ((M_o*temp_mat + W_ho*h_t_prev).colwise() + b_o).unaryExpr(sigmoid_functor());

		// // //Output hidden state
		// h_t = o_t.array()*(c_t.array().unaryExpr(tanh_functor()));

	}

	// print_eigen_matrix(h_t);

    end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of Eigen: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


    // bool match = eigen_check_thres(h_t, ptr_h_h_t, 1.0);

    // if(!match)
    // 	std::cout << "NO MATCH TO EIGEN" << std::endl;
    // else
    // 	std::cout << "MATCHES EIGEN" << std::endl;


}