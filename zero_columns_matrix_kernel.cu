//Kernel for the sparse lookups

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"


//d_vec must be a binary matrix, 0 if no input at this position, 1 if there is input

__global__ 
void zero_columns_kernel_512(int hiddenstate_size, float *d_mat,int *d_vec,float *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
	d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_vec[blockIdx.x];
	}
}

__global__ 
void zero_columns_kernel_256(int hiddenstate_size, float *d_mat,int *d_vec,float *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
	d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_vec[blockIdx.x];
	}
}

__global__ 
void zero_columns_kernel_128(int hiddenstate_size, float *d_mat,int *d_vec,float *d_mat_final) 
{
 	int idx = threadIdx.x+blockIdx.y*blockDim.x;
  if(idx < hiddenstate_size) {
		d_mat_final[IDX2C(idx,blockIdx.x,hiddenstate_size)] = \
	d_mat[IDX2C(idx,blockIdx.x,hiddenstate_size)] * d_vec[blockIdx.x];
	}
}



template<typename Derived,typename Derived2>
void eigen_test(const Eigen::MatrixBase<Derived> &h_mat,const Eigen::MatrixBase<Derived2> &h_vec,
	const Eigen::MatrixBase<Derived> &h_mat_final_const)
{
	UNCONST(Derived,h_mat_final_const,h_mat_final);
	h_mat_final = h_mat;
	for(int i=0; i< h_vec.rows(); i++) {
		if(h_vec(i)==0) {
			h_mat_final.col(i).setZero();
		}
	}
}


int main() {
	const int minibatch_size = 128;
	const int hiddenstate_size = 1000;
	const int num_trials =100;

	float *h_mat;
	float *d_mat;
	int *h_vec;
	int *d_vec;
	float *h_mat_final;
	float *d_mat_final;

	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_mat(hiddenstate_size,minibatch_size);
	Eigen::Matrix<int, Eigen::Dynamic, 1> h_eigen_vec(minibatch_size,1);
	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> h_eigen_mat_final(hiddenstate_size,minibatch_size);

	//initialize cuBLAS
	std::cout << "\n";
	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");
	
	//Init matrices on CUDA
	full_matrix_setup(&h_mat,&d_mat,hiddenstate_size,minibatch_size);
	full_vector_setup_vocab_01(&h_vec,&d_vec,minibatch_size);
	full_matrix_setup(&h_mat_final,&d_mat_final,hiddenstate_size,minibatch_size);

	//Copy the matrices to eigen
	copy_to_eigen(h_eigen_mat,h_mat);
	copy_to_eigen(h_eigen_vec,h_vec);
	copy_to_eigen(h_eigen_mat_final,h_mat_final);

	// print_matrix(h_mat,hiddenstate_size,minibatch_size);
	// print_eigen_matrix(h_eigen_mat);

	// print_matrix(h_vec,minibatch_size,1);
	// print_eigen_matrix(h_eigen_vec);

	//Run the kernels
	for(int i=0; i<num_trials; i++) {
		int threads_per_block = 512;
		int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
		dim3 kernel1(minibatch_size,num_block,1);
		zero_columns_kernel_512<<<kernel1,threads_per_block>>>(hiddenstate_size,d_mat,d_vec,d_mat_final);
		CUDA_GET_LAST_ERROR();
	}

	for(int i=0; i<num_trials; i++) {
		int threads_per_block = 256;
		int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
		dim3 kernel2(minibatch_size,num_block,1);
		zero_columns_kernel_256<<< kernel2,threads_per_block >>>(hiddenstate_size,d_mat,d_vec,d_mat_final);
		CUDA_GET_LAST_ERROR();
	}

	for(int i=0; i<num_trials; i++) {
		int threads_per_block = 128;
		int num_block = (hiddenstate_size+threads_per_block-1)/threads_per_block;
		dim3 kernel3(minibatch_size,num_block,1);
		zero_columns_kernel_128<<< kernel3,threads_per_block >>>(hiddenstate_size,d_mat,d_vec,d_mat_final);
		CUDA_GET_LAST_ERROR();
	}



	cudaDeviceSynchronize();

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	//Eigen code timing
	for(int i=0; i<num_trials; i++) {
		eigen_test(h_eigen_mat,h_eigen_vec,h_eigen_mat_final);
	}
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of Eigen: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

	get_matrix_cuBLAS(h_mat_final,d_mat_final,hiddenstate_size,minibatch_size);

	if(eigen_check_thres(h_eigen_mat_final,h_mat_final,0.001f)) {
		std::cout << "EIGEN CHECK PASSED\n";
	}
	else {
		std::cout << "EIGEN CHECK FAILED\n";
	}
}


