#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/transform.h>
#include <thrust/iterator/constant_iterator.h>

struct tanh_functor {
  float operator() (float x) const { return std::tanh(x); }
};

template<typename precision>
void initialize_thrust_vector(thrust::host_vector<precision> &h_vec,int size) {
	boost::uniform_real<> distribution(LOWER,UPPER);
	for(int i=0; i<size; i++) {
		h_vec[i] = (precision)distribution(gen);
	}
}

//The size of the matrix
const int hiddenstate_size = 1000;
const int output_vocab_size = 20000;

//The thrust devices
thrust::host_vector<float> thrust_h_matrix(hiddenstate_size * minibatch_size);
thrust::device_vector<float> thrust_d_matrix(hiddenstate_size * minibatch_size);

//my function to initialize the thrust host vector with values
initialize_thrust_vector(thrust_h_matrix,hiddenstate_size * minibatch_size);

//copy the thrust host code to the device code
thrust_d_matrix = thrust_h_matrix;

//Run the thrust operation
for(int i=0; i<num_trials; i++) {
	thrust::for_each(thrust_d_matrix.begin(),thrust_d_matrix.end(),tanh_functor());
}

