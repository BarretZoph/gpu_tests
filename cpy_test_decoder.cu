#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

// #include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

int main(int argc, char * argv[])
{
	const int output_vocab_size = 20000;
	const int minibatch_size = 128;
	const int num_trials = 100;

	float *h_outputdist;
	float *d_outputdist;
	h_outputdist = (float*)malloc(output_vocab_size*minibatch_size*sizeof(float));
	CUDA_ERROR_WRAPPER( cudaMalloc((void**)&d_outputdist, output_vocab_size*minibatch_size*sizeof(float)) ,"device unpinned device malloc failed\n");
	//CUDA_ERROR_WRAPPER( cudaMemcpy(d_outputdist, h_outputdist, size, cudaMemcpyHostToDevice) ,"device unpinned device memcpy failed\n");


	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		CUDA_ERROR_WRAPPER( cudaMemcpy(d_outputdist, h_outputdist, output_vocab_size*minibatch_size*sizeof(float), cudaMemcpyHostToDevice), "could not memcpy d_unpinned_mat1_device1 to h_unpinned_mat1" );
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;

}