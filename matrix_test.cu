#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"


int main() {

	int outer_size = 1;
	int inner_size = 2;

	cublasHandle_t handle;
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle),"CUBLAS handler initialization failed\n");
	
	double *h_temp;
	double *d_mat1;
	double *d_mat2;
	double *d_mat3;

	full_matrix_setup(&h_temp,&d_mat1,outer_size,inner_size);
	full_matrix_setup(&h_temp,&d_mat2,outer_size,inner_size);
	full_matrix_setup(&h_temp,&d_mat3,outer_size,outer_size);
	cudaMemset(d_mat3,0,outer_size*outer_size*sizeof(double));

	double alpha = 1;
	double beta = 1;

	cudaDeviceSynchronize();
	std::cout << "PRINTING MAT1\n";
	print_GPU_Matrix(d_mat1,outer_size,inner_size);

	cudaDeviceSynchronize();
	std::cout << "PRINTING MAT2\n";
	print_GPU_Matrix(d_mat2,outer_size,inner_size);

	CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle,CUBLAS_OP_N,CUBLAS_OP_T,outer_size,outer_size,inner_size,&alpha,d_mat1,outer_size,
		d_mat2,outer_size,&beta,d_mat3,outer_size),"THIS SHOULD WORK\n");


	cudaDeviceSynchronize();
	std::cout << "PRINTING MAT3\n";
	print_GPU_Matrix(d_mat2,outer_size,inner_size);

	std::cout << "FINISHED\n";

}