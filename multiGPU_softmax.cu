#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

int main() {
	const int output_vocab_size=20000;
	const int hiddenstate_size=1000;
	const int num_trials=100;

	float *h_mat1;
	float *h_mat2;
	float *h_mat3;
	float *h_mat4;
	float *h_mat5;
	float *h_mat6;

	float *d_mat1;
	float *d_mat2;
	float *d_mat3;
	float *d_mat4;
	float *d_mat5;
	float *d_mat6;

	full_matrix_setup(&h_mat1,&d_mat1,output_vocab_size,hiddenstate_size);
	full_matrix_setup(&h_mat2,&d_mat2,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_mat3,&d_mat3,output_vocab_size,hiddenstate_size);
	full_matrix_setup(&h_mat4,&d_mat4,output_vocab_size,hiddenstate_size);
	full_matrix_setup(&h_mat5,&d_mat5,hiddenstate_size,hiddenstate_size);
	full_matrix_setup(&h_mat6,&d_mat6,output_vocab_size,hiddenstate_size);

	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	std::chrono::duration<double> elapsed_seconds;

	cublasHandle_t handle_0;
	cudaSetDevice(0);
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle_0),"CUBLAS handler initialization failed\n");

	cublasHandle_t handle_1;
	cudaSetDevice(1);
	CUBLAS_ERROR_WRAPPER(cublasCreate(&handle_1),"CUBLAS handler initialization failed\n");


	cudaSetDevice(0);
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		float alpha=1;
		float beta=0;
		CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle_0,CUBLAS_OP_N,CUBLAS_OP_N,output_vocab_size,hiddenstate_size,hiddenstate_size,&alpha,
		d_mat1,output_vocab_size,d_mat2,hiddenstate_size,&beta,d_mat3,output_vocab_size),"123 failed in test1\n");
		CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle_0,CUBLAS_OP_N,CUBLAS_OP_N,output_vocab_size,hiddenstate_size,hiddenstate_size,&alpha,
		d_mat4,output_vocab_size,d_mat5,hiddenstate_size,&beta,d_mat6,output_vocab_size),"456 failed in test1\n");
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test1: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;



    cudaSetDevice(0);
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		float alpha=1;
		float beta=0;
		cudaSetDevice(0);
		CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle_0,CUBLAS_OP_N,CUBLAS_OP_N,output_vocab_size,hiddenstate_size,hiddenstate_size,&alpha,
		d_mat1,output_vocab_size,d_mat2,hiddenstate_size,&beta,d_mat3,output_vocab_size),"123 failed in test2\n");
		cudaSetDevice(1);
		CUBLAS_ERROR_WRAPPER(cublas_gemm_wrapper(handle_1,CUBLAS_OP_N,CUBLAS_OP_N,output_vocab_size,hiddenstate_size,hiddenstate_size,&alpha,
		d_mat4,output_vocab_size,d_mat5,hiddenstate_size,&beta,d_mat6,output_vocab_size),"456 failed in test2\n");
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average test2: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;
	
}



