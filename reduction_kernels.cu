#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

__global__ void reduce0(int *g_idata,int *g_odata) {
	extern __shared__ int sdata[];
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
	sdata[tid] = g_idata[i]; //every thread loads one piece of memory into shared data
}


int main() {

}