#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <cfloat>

#include <Eigen/Dense>

#include "BZ_CUDA_UTIL.h"

//thrust streams
//#include <thrust/execution_policy.h>
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/extrema.h>


template<typename dType>
__device__ 
void warpReduceSum(volatile dType* sdata, int tid) {
	sdata[tid] += sdata[tid + 32];
	sdata[tid] += sdata[tid + 16];
	sdata[tid] += sdata[tid + 8];
	sdata[tid] += sdata[tid + 4];
	sdata[tid] += sdata[tid + 2];
	sdata[tid] += sdata[tid + 1];
}

#define NORM_THREADS 256
//launch these with 1 block
template<typename dType>
__global__
void basic_compute_norm(dType *d_gradient,int size,dType *result) {
	__shared__ dType buffer[NORM_THREADS];
	int i_start = threadIdx.x; //start at the thread index
	int i_end = size; //end at dim
	int i_step = blockDim.x; //the block dimension (aka the number of threads in the block) is the step
	int tid = threadIdx.x;


	buffer[tid] = 0;
	for(int i= i_start; i<i_end; i+=i_step) {
		buffer[tid]+=(d_gradient[i]*d_gradient[i]);
	}
	__syncthreads();

	for(int stride=NORM_THREADS/2; stride>32; stride>>=1) {
		if(tid < stride) {
			buffer[tid] += buffer[stride + tid];
		}
		__syncthreads();
	}

	if(tid<32) {
		warpReduceSum(buffer,tid);
	}
	__syncthreads();

	if(tid==0) {
		result[0]=buffer[0];
	}
}


template<typename dType>
__global__
void basic_compute_norm_p1(dType *d_gradient,int size,dType *result) {
	__shared__ dType buffer[NORM_THREADS];
	int i_start = threadIdx.x+blockIdx.x*blockDim.x; //start at the thread index
	int i_end = size; //end at dim
	int i_step = blockDim.x*gridDim.x; //the block dimension (aka the number of threads in the block) is the step
	int tid = threadIdx.x;


	buffer[tid] = 0;
	for(int i= i_start; i<i_end; i+=i_step) {
		buffer[tid]+=(d_gradient[i]*d_gradient[i]);
	}
	__syncthreads();

	for(int stride=NORM_THREADS/2; stride>32; stride>>=1) {
		if(tid < stride) {
			buffer[tid] += buffer[stride + tid];
		}
		__syncthreads();
	}

	if(tid<32) {
		warpReduceSum(buffer,tid);
	}
	__syncthreads();

	if(tid==0) {
		result[blockIdx.x]=buffer[0];
	}
}


template<typename dType>
__global__
void basic_compute_norm_p2(dType *temp_result,dType *final_result) {
	__shared__ dType buffer[NORM_THREADS];

	int tid = threadIdx.x;
	buffer[tid] = temp_result[tid];
	__syncthreads();

	for(int stride=NORM_THREADS/2; stride>32; stride>>=1) {
		if(tid < stride) {
			buffer[tid] += buffer[stride + tid];
		}
		__syncthreads();
	}

	if(tid<32) {
		warpReduceSum(buffer,tid);
	}
	__syncthreads();

	if(tid==0) {
		final_result[0]=buffer[0];
	}
}



struct square {
    __host__ __device__
    float operator()(const float& x) const { 
        return x * x;
    }

    __host__ __device__
    double operator()(const double& x) const { 
        return x * x;
    }
};


int main() {

	const int rows = 1000;
	const int cols = 1000;
	const int num_trials = 100;
	float *h_mat;
	float *d_mat;

	float *d_result;
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_result, 1*sizeof(float)),"GPU memory allocation failed\n");

	float *d_temp_result;
	CUDA_ERROR_WRAPPER(cudaMalloc((void**)&d_temp_result, NORM_THREADS*sizeof(float)),"GPU memory allocation failed\n");

	full_matrix_setup(&h_mat,&d_mat,rows,cols);

	thrust::device_ptr<float> thrust_d_mat = thrust::device_pointer_cast(d_mat);

	//timing
	std::chrono::time_point<std::chrono::system_clock> start_total,end_total;
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		
		float norm = std::sqrt( thrust::transform_reduce(thrust_d_mat,thrust_d_mat+rows*cols, square(), (float)0, thrust::plus<float>()) );
		//std::cout << "thrust norm: " << norm << "\n";
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of thrust: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;



    //timing
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		float norm;
		basic_compute_norm<<< 1,NORM_THREADS>>>(d_mat,rows*cols,d_result);
		cudaMemcpy(&norm,d_result,1*sizeof(float),cudaMemcpyDeviceToHost);
		norm = std::sqrt(norm);
		//std::cout << "my norm: " << norm << "\n";
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of mine: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;


     //timing
	start_total = std::chrono::system_clock::now();
	for(int i=0; i<num_trials; i++) {
		float norm;
		basic_compute_norm_p1<<<NORM_THREADS,NORM_THREADS>>>(d_mat,rows*cols,d_temp_result);
		basic_compute_norm_p2<<< 1,NORM_THREADS>>>(d_temp_result,d_result);
		cudaMemcpy(&norm,d_result,1*sizeof(float),cudaMemcpyDeviceToHost);
		norm = std::sqrt(norm);
		//std::cout << "my 2nd norm: " << norm << "\n";
	}
	cudaDeviceSynchronize();
	end_total = std::chrono::system_clock::now();
	elapsed_seconds = end_total-start_total;
    std::cout << "\n\n\n";
    std::cout << "Average Runtime of mine: " << (elapsed_seconds.count())/num_trials << " seconds" << std::endl;
}	


